﻿using FairlyRepairNotify.MeetModels;
using FairlyRepairNotify.Models;
using FairlyRepairNotify.Repositories;
using FairlyRepairNotify.Repositories.MeetRepositories;
using FairlyRepairNotify.Utility;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using AutoMapper;
using FairlyRepairNotify.ViewModels.Repair;
using System.IO;
using OfficeOpenXml;

namespace FairlyRepairNotify
{
    public partial class Form1 : Form
    {
        public string notifyUrl = "https://notify-api.line.me/api/notify";
        string ClientID = "63Zqpardt29Uzt5EiU5rpS";
        string ClientSecret = "LLZaWh3paSVgjfe104Su7CGWzlxnu5Ik6GSspsuiZOg";
        private Repositories.AdminRepository adminRepository = new Repositories.AdminRepository(new FairlyRepairDBEntities());
        private AuditRepository auditRepository = new AuditRepository(new FairlyRepairDBEntities());
        private RepairRepository repairRepository = new RepairRepository(new FairlyRepairDBEntities());
        private RepairRecordRepository repairRecordRepository = new RepairRecordRepository(new FairlyRepairDBEntities());
        private RepairRecordFeeRepository repairRecordFeeRepository = new RepairRecordFeeRepository(new FairlyRepairDBEntities());
        private EightDRepository eightDRepository = new EightDRepository(new FairlyRepairDBEntities());
        private Repositories.MeetRepositories.AdminRepository meetAdminRepository = new Repositories.MeetRepositories.AdminRepository(new MeetingSignInDBEntities());
        private AuditLevelRepository auditLevelRepository = new AuditLevelRepository(new MeetingSignInDBEntities());
        private Repositories.T8Repositories.PersonRepository t8personRepository = new Repositories.T8Repositories.PersonRepository(new T8ERPEntities());

        //測試模式
        public bool testMode = false;
        public string testMail = "seelen12@fairlybike.com";

        public Form1()
        {
            InitializeComponent();
            TrayMenuContext();
        }

        private void TrayMenuContext()
        {
            this.notifyIcon1.ContextMenuStrip = new System.Windows.Forms.ContextMenuStrip();
            this.notifyIcon1.ContextMenuStrip.Items.Add("開啟", null, this.button1_Click);
            this.notifyIcon1.ContextMenuStrip.Items.Add("停止", null, this.button2_Click);
            this.notifyIcon1.ContextMenuStrip.Items.Add("關閉", null, this.button3_Click);
        }

        /// <summary>
        /// App 發送訊息
        /// </summary>
        public async Task StartSendNotify()
        {
            try
            {
                await Task.Delay(3000);
                Console.WriteLine("test...");
                DateTime nowTime = DateTime.Now;

                bool canClose = true;

                #region 待審通知
                string link = "http://220.228.58.43:18151/Admin/AuditAdmin?State=";
                string msg = "<br>";
                msg += " <br> !! 8D報告通知 !!  <br>";

                var auditDepts = new List<string>();

                //主管未審資料
                //textBox1.Text += "\r\n 待主管簽核";
                var unAuditQuery = auditRepository.Query(false).Where(q => q.State == (int)AuditState.OfficerAudit);
                if (unAuditQuery.Count() > 0)
                {
                    //待審報告
                    var eightDIdList = unAuditQuery.Select(q => q.ReportID).ToList();
                    //承辦人
                    var eightDCreaterList = eightDRepository.GetAll().Where(q => !q.IsRevoke && eightDIdList.Contains(q.ID)).Select(q => q.CreaterId).ToList();
                    //主管
                    var reciverQuery = meetAdminRepository.Query(true, (int)MeetAdminType.Manager);

                    foreach (var item in reciverQuery)
                    {
                        var personInfo = t8personRepository.FindByJobID(item.Account);
                        if (personInfo != null && !string.IsNullOrEmpty(personInfo.Email))
                        {
                            //該主管審核部門
                            auditDepts = auditLevelRepository.Query(true, "", "", personInfo.JobID).Select(q => q.DeptId).ToList();
                            //該部門人員
                            List<string> sameDeptPersonIdList = t8personRepository.GetPersonQuery("", "").Where(q => auditDepts.Contains(q.DeptID)).Select(q => q.JobID).ToList();
                            //該人員於系統內編號
                            var havsUnAudited = adminRepository.GetAll().Where(q => sameDeptPersonIdList.Contains(q.Account) && eightDCreaterList.Contains(q.Account)).Count() > 0;
                            if (havsUnAudited)
                            {
                                string auditMsg = msg + " <br> 有部分資料需要審核，請點擊連結前往察看 <br> << " + link + "1 >> <br>";
                                //發送信件
                                if (testMode)
                                {
                                    MailHelper.POP3Mail(testMail, "8D報告 - 【待主管簽核】", auditMsg, "");
                                }
                                else
                                {
                                    MailHelper.POP3Mail(personInfo.Email, "8D報告 - 【待主管簽核】", auditMsg, "");
                                }
                            }
                        }
                    }
                }

                //副總未審資料
                //textBox1.Text += "\r\n 待副總簽核";
                unAuditQuery = auditRepository.Query(false).Where(q => q.State == (int)AuditState.ManagerAudit);
                if (unAuditQuery.Count() > 0)
                {
                    //待審報告
                    var eightDIdList = unAuditQuery.Select(q => q.ReportID).ToList();
                    //承辦人
                    var eightDCreaterList = eightDRepository.GetAll().Where(q => !q.IsRevoke && eightDIdList.Contains(q.ID)).Select(q => q.CreaterId).ToList();

                    var reciverQuery = adminRepository.Query(true, (int)MeetAdminType.Vice);
                    foreach (var item in reciverQuery)
                    {
                        var personInfo = t8personRepository.FindByJobID(item.Account);
                        if (personInfo != null && !string.IsNullOrEmpty(personInfo.Email))
                        {
                            //該主管審核部門
                            auditDepts = auditLevelRepository.Query(true, "", personInfo.JobID, "").Select(q => q.DeptId).ToList();
                            //該部門人員
                            List<string> sameDeptPersonIdList = t8personRepository.GetPersonQuery("", "").Where(q => auditDepts.Contains(q.DeptID)).Select(q => q.JobID).ToList();
                            //該人員於系統內編號
                            var havsUnAudited = adminRepository.GetAll().Where(q => sameDeptPersonIdList.Contains(q.Account) && eightDCreaterList.Contains(q.Account)).Count() > 0;
                            if (havsUnAudited)
                            {
                                string auditMsg = msg + " <br> 有部分資料需要審核，請點擊連結前往察看 <br> << " + link + "2 >> <br>";
                                //發送信件
                                if (testMode)
                                {
                                    MailHelper.POP3Mail(testMail, "8D報告 - 【待副總經理簽核】", auditMsg, "");
                                }
                                else
                                {
                                    MailHelper.POP3Mail(personInfo.Email, "8D報告 - 【待副總經理簽核】", auditMsg, "");
                                }
                            }
                        }
                    }
                }

                //總經理未審資料
                //textBox1.Text += "\r\n 待總經理簽核";
                unAuditQuery = auditRepository.Query(false).Where(q => q.State == (int)AuditState.ViceAudit);
                if (unAuditQuery.Count() > 0)
                {
                    //待審報告
                    var eightDIdList = unAuditQuery.Select(q => q.ReportID).ToList();
                    //承辦人
                    var eightDCreaterList = eightDRepository.GetAll().Where(q => !q.IsRevoke && eightDIdList.Contains(q.ID)).Select(q => q.CreaterId).ToList();

                    var reciverQuery = adminRepository.Query(true, (int)MeetAdminType.President);
                    foreach (var item in reciverQuery)
                    {
                        var personInfo = t8personRepository.FindByJobID(item.Account);
                        if (personInfo != null && !string.IsNullOrEmpty(personInfo.Email))
                        {
                            //該主管審核部門
                            auditDepts = auditLevelRepository.Query(true, personInfo.JobID, "", "").Select(q => q.DeptId).ToList();
                            //該部門人員
                            List<string> sameDeptPersonIdList = t8personRepository.GetPersonQuery("", "").Where(q => auditDepts.Contains(q.DeptID)).Select(q => q.JobID).ToList();
                            //該人員於系統內編號
                            var havsUnAudited = adminRepository.GetAll().Where(q => sameDeptPersonIdList.Contains(q.Account) && eightDCreaterList.Contains(q.Account)).Count() > 0;
                            if (havsUnAudited)
                            {
                                string auditMsg = msg + " <br> 有部分資料需要審核，請點擊連結前往察看 <br> << " + link + "3 >> <br>";
                                //發送信件
                                if (testMode)
                                {
                                    MailHelper.POP3Mail(testMail, "8D報告 - 待總經理簽核", auditMsg, "");
                                }
                                else
                                {
                                    MailHelper.POP3Mail(personInfo.Email, "8D報告 -【待總經理簽核】", auditMsg, "");
                                }
                            }
                        }
                    }
                }

                //var reciverList = t8personRepository.GetPersonQuery().ToList();
                //foreach (var item in reciverList)
                //{
                //    #region 寄送Email                    
                //    string to = "seelen12@fairlybike.com";
                //    //string to = item.EMail;
                //    string subject = "";
                //    string mailBody = "";
                //    //mailBody += $"<p> 申報人 : {repairInfo.CreaterName} </p>";                    
                //    //mailBody += $" 查看 : <a href=\"{Request.Url.Scheme}://{Request.Url.Authority}/Admin/RepairAdmin/Edit/{repairInfo.ID}\">後台管理連結</a> \r\n \r\n";

                //    subject = $"8D報告 - 簽核通知";
                //    MailHelper.POP3Mail(to, subject, mailBody);
                //    #endregion
                //}
                #endregion

                //每月初發送報修統計 Excel
                #region 建立Excel
                DateTime now = DateTime.Now;
                DateTime StartDate = now.AddMonths(-1);
                DateTime EndDate = now;
                var CurrentDirectory = Directory.GetCurrentDirectory();
                var lastMonth = DateTime.Now.AddMonths(-1);
                string fileName = $"{lastMonth.Year}年{lastMonth.Month}月報修紀錄";
                //string fullPath = CurrentDirectory.Replace(@"\bin\Debug", @"\ExportFiles\" + fileName + ".xlsx");
                string fullPath = @"C:\inetpub\wwwroot\FairlyRepair\ExportFiles\" + fileName + ".xlsx";
                if (testMode)
                {
                    fullPath = @"C:\Users\seelen12\Desktop\Fairly\ExcelFiles\" + fileName + ".xlsx";
                }

                //textBox1.Text += "\r\n 建立Excel";
                var result = ExportListExcel(StartDate, EndDate, fullPath);
                #endregion

                if (result && File.Exists(fullPath))
                {
                    #region 發送通知
                    //textBox1.Text += "\r\n 發送通知";
                    msg = " <br> " + fileName + " <br>";

                    //寄送給蔡正良
                    result = SendReportMail("", msg, "steven@fairlybike.com", fullPath, fileName);
                    //寄送給蕭鴻文
                    result = SendReportMail("", msg, "wayen@fairlybike.com", fullPath, fileName);
                    #endregion

                    if (result)
                    {
                        //執行完關閉
                        await Task.Delay(2000);
                        Environment.Exit(Environment.ExitCode);
                        this.Dispose();
                        this.Close();
                    }
                    else
                    {
                        //textBox1.Text += "\r\n" + "寄信失敗 !";
                    }
                }
                else
                {
                    Console.Write("無資料/資料處裡錯誤 !");
                    Console.ReadLine();
                }

                if (canClose)
                {
                    //執行完關閉
                    await Task.Delay(3000);
                    System.Environment.Exit(System.Environment.ExitCode);
                    this.Dispose();
                    this.Close();
                }
            }
            catch (Exception e)
            {
                textBox1.Text += "\r\n" + e.Message;
                throw;
            }
        }

        private bool SendReportMail(string link, string msg, string email, string fullPath = "", string fileName = "")
        {
            try
            {
                var lastMonth = DateTime.Now.AddMonths(-1);
                //發送信件
                if (testMode)
                {
                    MailHelper.POP3Mail(testMail, $"報修通知 - {lastMonth.Year}年{lastMonth.Month}月報修紀錄", msg, fullPath);
                }
                else
                {
                    MailHelper.POP3Mail(email, $"報修通知 - {lastMonth.Year}年{lastMonth.Month}月報修紀錄", msg, fullPath);
                }

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        /// <summary>
        /// 匯出Excel清單 (非同步)
        /// </summary>
        /// <returns></returns>
        public bool ExportListExcel(DateTime StartDate, DateTime EndDate, string fullPath)
        {
            // 建立Configuration設定class之間的映射關係
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<Repair, RepairView>();
            });

            // 建立Mapper
            var mapper = config.CreateMapper();
            //上個月總結

            DateTime dt = System.DateTime.Now;
            DateTime startDate = new System.DateTime(dt.Year, dt.Month, 1);
            DateTime endDate = startDate.AddMonths(1).AddDays(-1);

            try
            {
                #region 產生信件內容
                //textBox1.Text += "\r\n 匯出資料存在.....";
                ExcelPackage.LicenseContext = OfficeOpenXml.LicenseContext.NonCommercial; // 關閉新許可模式通知
                //建立Excel
                ExcelPackage excelPackage = new ExcelPackage();

                //建立第一個Sheet，後方為定義Sheet的名稱
                ExcelWorksheet sheet = excelPackage.Workbook.Worksheets.Add("每月報修紀錄");

                MemoryStream fileStream = new MemoryStream();
                try
                {

                    int col = 1;    //欄:直的，從第1欄開始

                    //第1列是標題列 
                    //標題列部分，是取得DataAnnotations中的DisplayName，這樣比較一致，
                    //這也可避免後期有修改欄位名稱需求，但匯出excel標題忘了改的問題發生。
                    //取得做法可參考最後的參考連結。

                    int row = 1;    //列:橫的

                    //取出要匯出Excel的資料(勾選需出勤紀錄的會議)
                    //取得一個月內的報修單
                    var repairsFromLastMonth = mapper.Map<List<RepairView>>(repairRepository.Query(null, null, null, null, 0, 0, 0, 0, 0, "", false).Where(q => q.CreateDate >= startDate && q.CreateDate <= endDate));

                    //總金額、總工時、總數量、達標分類占比
                    sheet.Cells[row, col++].Value = "總金額";
                    sheet.Cells[row, col++].Value = "總工時";
                    sheet.Cells[row, col++].Value = "數量";
                    sheet.Cells[row, col++].Value = "平均工時";
                    sheet.Cells[row, col++].Value = "超標";
                    sheet.Cells[row, col++].Value = "達標";
                    sheet.Cells[row, col++].Value = "未達標";

                    row = 2;

                    col = 1;
                    var repairIdList = repairsFromLastMonth.Select(q => q.ID).ToList();
                    var recordIdList = repairRecordRepository.GetAll().Where(q => repairIdList.Contains(q.RepairID)).Select(q => q.ID).ToList();

                    //總金額
                    var allRecordFee = repairRecordFeeRepository.Query().Where(q => q.Fee > 0 && recordIdList.Contains(q.RecordID));
                    var totalFee = allRecordFee.Count() > 0 ? allRecordFee.Sum(q => q.Fee) : 0;
                    sheet.Cells[row, col++].Value = $"{totalFee}";
                    sheet.Column(col).AutoFit();

                    //總工時
                    var minutes = repairsFromLastMonth.Sum(q => q.Minute);
                    sheet.Cells[row, col++].Value = $"{minutes / 60}";
                    sheet.Column(col).AutoFit();

                    //數量
                    sheet.Cells[row, col++].Value = $"{repairsFromLastMonth.Count()}";
                    sheet.Column(col).AutoFit();

                    //平均工時
                    sheet.Cells[row, col++].Value = $"{(minutes / repairsFromLastMonth.Count()) / 60}";
                    sheet.Column(col).AutoFit();

                    //超標 4H內
                    var exceedCount = repairsFromLastMonth.Where(q => q.Minute > 0 && q.Minute <= 240).Count();
                    sheet.Cells[row, col++].Value = $"{exceedCount}";
                    sheet.Column(col).AutoFit();

                    //達標 4~8H
                    var meetCount = repairsFromLastMonth.Where(q => q.Minute > 240 && q.Minute <= 960).Count();
                    sheet.Cells[row, col++].Value = $"{meetCount}";
                    sheet.Column(col).AutoFit();

                    //未達標 8H以上
                    var belowCount = repairsFromLastMonth.Where(q => q.Minute > 960).Count();
                    sheet.Cells[row, col++].Value = $"{belowCount}";
                    sheet.Column(col).AutoFit();

                    //迴圈讀取訓練資料
                    //資料從第2列開始
                    col = 2;

                    var existingFile = new FileInfo(fullPath);
                    //if (existingFile.Exists)
                    //{
                    //    existingFile.Delete();
                    //}

                    excelPackage.SaveAs(existingFile);
                    excelPackage.Dispose();
                    fileStream.Dispose();
                    //fileStream.Position = 0;//不重新將位置設為0，excel開啟後會出現錯誤
                }
                catch (Exception e)
                {
                    //textBox1.Text += "\r\n" + e.Message;
                    excelPackage.Dispose();
                    fileStream.Dispose();
                }
                #endregion

                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            button3.Visible = false;
            //Active = true;
            textBox1.Text = "開始發送訊息 。。。";
            Task.Run(StartSendNotify);
        }

        private void button1_Click(object sender, EventArgs e)
        {//開啟
            //Active = true;
            textBox1.Text += "\r\n開始發送訊息 。。。";
            Task.Run(StartSendNotify);
        }

        private void button2_Click(object sender, EventArgs e)
        {//停止
            //Active = false;
            textBox1.Text += "\r\n已停止執行";
        }

        private void button3_Click(object sender, EventArgs e)
        {
            System.Environment.Exit(System.Environment.ExitCode);
            this.Dispose();
            this.Close();
        }

        private void notifyIcon1_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            if (this.ShowInTaskbar == false)
                notifyIcon1.Visible = true;

            this.ShowInTaskbar = true;
            this.Show();
            this.Activate();
            this.WindowState = FormWindowState.Normal;
        }

        private void Form1_FormClosed(object sender, FormClosedEventArgs e)
        {
            System.Environment.Exit(System.Environment.ExitCode);
            this.Dispose();
            this.Close();
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            e.Cancel = true;
            this.Hide();
        }

        private void Form1_SizeChanged(object sender, EventArgs e)
        {
            if (this.WindowState == FormWindowState.Minimized)
            {
                this.WindowState = FormWindowState.Normal;
                notifyIcon1.Visible = true;
                this.Hide();
                this.ShowInTaskbar = false;
            }
        }
    }
}
