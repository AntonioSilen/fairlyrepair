﻿using FairlyRepairNotify.Models;
using FairlyRepairNotify.Repositories;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FairlyRepairNotify.ViewModels.Repair
{
    public class RepairView
    {
        AdminRepository adminRepository = new AdminRepository(new FairlyRepairDBEntities());
        private Repositories.T8Repositories.PersonRepository t8personRepository = new Repositories.T8Repositories.PersonRepository(new T8ERPEntities());
        private Repositories.T8Repositories.comDepartmentRepository comDepartmentRepository = new Repositories.T8Repositories.comDepartmentRepository(new T8ERPEntities());
        DeviceRepository deviceRepository = new DeviceRepository(new FairlyRepairDBEntities());
        EquipmentRepository equipmentRepository = new EquipmentRepository(new FairlyRepairDBEntities());

        public RepairView()
        {
            this.ProcessingStatus = 0;
        }

        //public List<RepairRecordView> RecordList { get; set; }

        [Required]
        [Display(Name = "編號")]
        public int ID { get; set; }

        [Required]
        [Display(Name = "課別")]
        public string DeptID { get; set; }
        public string DepartmentTitle
        {
            get
            {
                return comDepartmentRepository.FindByDeptId(this.DeptID).DeptName;
            }
        }

        [Required]
        [Display(Name = "廠區")]
        public int FactoryID { get; set; }
        public string FactoryTitle
        {
            get
            {
                var data = equipmentRepository.GetById(this.FactoryID);
                return data == null ? "" : data.Title;
                //return EnumHelper.GetDescription((Factory)this.FactoryID);
            }
        }

        [Required]
        [Display(Name = "樓層")]
        public int FloorID { get; set; }
        public string FloorTitle
        {
            get
            {
                var data = equipmentRepository.GetById(this.FloorID);
                return data == null ? "" : data.Title;
            }
        }

        [Required]
        [Display(Name = "區域")]
        public int AreaID { get; set; }
        public string AreaTitle
        {
            get
            {
                var data = equipmentRepository.GetById(this.AreaID);
                return data == null ? "" : data.Title;
            }
        }

        [Required]
        [Display(Name = "報修類型")]
        public int DeviceType { get; set; }
        public string DeviceTypeName
        {
            get
            {
                return deviceRepository.FindBy(this.DeviceType).Title;
            }
        }


        [Required]
        [Display(Name = "設備")]
        public int DeviceID { get; set; }
        public string DeviceName
        {
            get
            {
                var data = equipmentRepository.GetById(this.DeviceID);
                return data == null ? "" : data.Title;
                //return deviceRepository.FindBy(this.DeviceID).Title;
            }
        }

        [Display(Name = "設備項目")]
        public int DeviceItem { get; set; }
        public string DeviceItemName
        {
            get
            {
                //var ItemInfo = deviceRepository.FindBy(this.DeviceItem);
                //return ItemInfo == null ? "" : ItemInfo.Title;
                var data = equipmentRepository.GetById(this.AreaID);
                return data == null ? "" : data.Title;
            }
        }

        [Required]
        [Display(Name = "問題敘述")]
        public string Description { get; set; }

        [Display(Name = "事故照片")]
        public string Pics { get; set; }
        public List<RepairImage> PicList { get; set; }
        //public List<HttpPostedFileBase> PicsFiles { get; set; }

        [Required]
        [Display(Name = "故障現況")]
        public int FaultState { get; set; }

        [Required]
        [Display(Name = "調度員")]
        public int DispatcherID { get; set; }

        [Display(Name = "維修員")]
        public string MaintID { get; set; }

        [Display(Name = "送審原因")]
        public string SubmitReason { get; set; }

        [Required]
        [Display(Name = "總經理審核狀態")]
        public int PresAppStatus { get; set; }

        [Required]
        [Display(Name = "需總經理審核")]
        public bool IsPresApproval { get; set; }

        [Display(Name = "總經理審核日期")]
        public DateTime? PresAppDate { get; set; }

        [Display(Name = "總經理審核結果說明")]
        public string PresResult { get; set; }

        [Required]
        [Display(Name = "副總審核狀態")]
        public int ViceAppStatus { get; set; }

        [Required]
        [Display(Name = "需副總審核")]
        public bool IsViceApproval { get; set; }

        [Display(Name = "副總審核日期")]
        public DateTime? ViceAppDate { get; set; }

        [Display(Name = "副總審核結果說明")]
        public string ViceResult { get; set; }

        [Required]
        [Display(Name = "撤銷狀態")]
        public bool IsRevoke { get; set; }

        [Required]
        [Display(Name = "工時(分鐘)")]
        public DateTime? DispatchTime { get; set; }

        [Required]
        [Display(Name = "工時(分鐘)")]
        public DateTime? CompleteTime { get; set; }

        [Required]
        [Display(Name = "工時(分鐘)")]
        public decimal Minute { get; set; }

        [Required]
        [Display(Name = "新增採購")]
        public bool IsPurchase { get; set; }

        [Required]
        [Display(Name = "完工")]
        public bool IsFinish { get; set; }

        [Required]
        [Display(Name = "結案")]
        public bool IsClose { get; set; }

        [Required]
        [Display(Name = "更新時間")]
        public DateTime UpdateDate { get; set; }

        [Required]
        [Display(Name = "更新者")]
        public int Updater { get; set; }

        [Required]
        [Display(Name = "報修申請時間")]
        public DateTime CreateDate { get; set; }

        [Required]
        [Display(Name = "申請人")]
        public int Creater { get; set; }

        [Display(Name = "處理狀態")]
        public int ProcessingStatus { get; set; }
        public string ProcessingStatusStr { get; set; }

        [Display(Name = "費用(累計)")]
        public int TotalFee
        {
            get
            {
                if (this.ID == 0)
                {
                    return 0;
                }
                else
                {
                    RepairRecordRepository repairRecordRepository = new RepairRecordRepository(new FairlyRepairDBEntities());
                    RepairRecordFeeRepository repairRecordFeeRepository = new RepairRecordFeeRepository(new FairlyRepairDBEntities());
                    int total = 0;
                    var allRecord = repairRecordRepository.Query(null, this.ID);
                    if (allRecord.Count() > 0)
                    {
                        foreach (var item in allRecord)
                        {
                            total += item.Fee;
                        }
                    }

                    return total;
                }
            }
        }

        public string CreaterName
        {
            get
            {
                try
                {
                    var jobID = adminRepository.GetById(this.Creater).Account;
                    var nameInfo = t8personRepository.FindByJobID(jobID);
                    if (nameInfo == null)
                    {
                        return adminRepository.GetById(this.Creater).Name;
                    }

                    return nameInfo.PersonName;
                }
                catch (Exception e)
                {
                    return "";
                }
            }
        }
    }
}
