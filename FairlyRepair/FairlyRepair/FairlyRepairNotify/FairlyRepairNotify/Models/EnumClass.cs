﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace FairlyRepairNotify.Models
{
    /// <summary>
    /// 1.系統管理員  2.後台管理員
    /// </summary>
    public enum AdminType //管理員類型
    {
        /// <summary>
        /// 系統管理員
        /// </summary>
        [Description("系統管理員")]
        Sysadmin = 1,

        /// <summary>
        /// 總經理
        /// </summary>
        [Description("總經理")]
        President = 2,

        /// <summary>
        /// 副總
        /// </summary>
        [Description("副總")]
        VicePresident = 3,

        /// <summary>
        /// 調度員
        /// </summary>
        [Description("調度員")]
        Dispatcher = 4,

        /// <summary>
        /// 維修員
        /// </summary>
        [Description("維修員")]
        Mainter = 5,

        /// <summary>
        /// 課長
        /// </summary>
        [Description("課長")]
        Manager = 6,

        /// <summary>
        /// 申請人
        /// </summary>
        [Description("申請人")]
        Applicant = 7,
    }

    /// <summary>
    /// 1.系統管理員  2.後台管理員
    /// </summary>
    public enum MeetAdminType //帳號類型
    {
        /// <summary>
        /// 系統管理員
        /// </summary>
        [Description("系統管理員")]
        Sysadmin = 1,

        /// <summary>
        /// 總經理
        /// </summary>
        [Description("總經理")]
        President = 2,

        /// <summary>
        /// 副總
        /// </summary>
        [Description("副總")]
        Vice = 3,

        /// <summary>
        /// 主管
        /// </summary>
        [Description("主管")]
        Manager = 5,

        /// <summary>
        /// 一般員工
        /// </summary>
        [Description("一般員工")]
        Employee = 7,
    }

    /// <summary>
    /// 廠區
    /// </summary>
    public enum Factory
    {
        /// <summary>
        /// 一廠
        /// </summary>
        [Description("一廠")]
        Bar = 1,

        /// <summary>
        /// 二廠
        /// </summary>
        [Description("二廠")]
        Straight = 2,

        /// <summary>
        /// 三廠
        /// </summary>
        [Description("三廠")]
        Square = 3,
    }

    /// <summary>
    /// 結案狀態
    /// </summary>
    public enum CaseStatus
    {
        /// <summary>
        /// 未完成
        /// </summary>
        [Description("未完成")]
        Unfinished = 1,

        /// <summary>
        /// 已完成
        /// </summary>
        [Description("已完成")]
        Finished = 2,

        /// <summary>
        /// 已完成需再觀察
        /// </summary>
        [Description("已完成需再觀察")]
        Observe = 3,

        /// <summary>
        /// 其他
        /// </summary>
        [Description("其他")]
        Other = 4,
    }

    /// <summary>
    /// 維修狀況
    /// </summary>
    public enum MaintStatus
    {
        /// <summary>
        /// 正常(需維修)
        /// </summary>
        [Description("正常(需維修)")]
        Maint = 1,

        /// <summary>
        /// 正常(需立即處裡)
        /// </summary>
        [Description("正常(需立即處裡)")]
        Immediate = 2,

        /// <summary>
        /// 故障/損壞
        /// </summary>
        [Description("故障/損壞")]
        Broke = 3,

        /// <summary>
        /// 添加設備/工具(暫時)
        /// </summary>
        [Description("添加設備/工具(暫時)")]
        Add = 4,
    }
    /// <summary>
    /// 故障現況
    /// </summary>
    public enum FaultState
    {
        /// <summary>
        /// 未見損壞，但有使用上的異常
        /// </summary>
        [Description("未見損壞，但有使用上的異常")]
        State_I = 1,

        /// <summary>
        /// 輕微損壞，但仍可使用
        /// </summary>
        [Description("輕微損壞，但仍可使用")]
        State_II = 2,

        /// <summary>
        /// 中度損壞，可能於短期內即無法使用
        /// </summary>
        [Description("中度損壞，可能於短期內即無法使用")]
        State_III = 3,

        /// <summary>
        /// 嚴重損壞，已經無法使用或難以使用
        /// </summary>
        [Description("嚴重損壞，已經無法使用或難以使用")]
        State_IV = 4,

        /// <summary>
        /// 完全損壞，需立即尋找替代品替換
        /// </summary>
        [Description("完全損壞，需立即尋找替代品替換")]
        State_V = 5,

        /// <summary>
        /// 無損壞問題，但有新增設備需求
        /// </summary>
        [Description("無損壞問題，但有新增設備需求")]
        State_VI = 6,

        /// <summary>
        /// 無損壞問題，但有修改/調整/改造現有設備的需求
        /// </summary>
        [Description("無損壞問題，但有修改/調整/改造現有設備的需求")]
        State_VII = 7,

        /// <summary>
        /// 其他/無法判定/或需由安衛課、廠商評估
        /// </summary>
        [Description("其他/無法判定/或需由安衛課、廠商評估")]
        State_VIII = 8,
    }

    /// <summary>
    /// 設備
    /// </summary>
    public enum DeviceClass
    {
        /// <summary>
        /// 主層
        /// </summary>
        [Description("主層")]
        Main = 1,

        /// <summary>
        /// 二層
        /// </summary>
        [Description("二層")]
        Second = 2,

        /// <summary>
        /// 三層
        /// </summary>
        [Description("三層")]
        Third = 3,
    }
    /// <summary>
    /// 設備
    /// </summary>
    public enum DeviceType
    {
        /// <summary>
        /// 安全衛生課
        /// </summary>
        [Description("安全衛生課")]
        SafetyAndHealth = 1,

        /// <summary>
        /// 全廠
        /// </summary>
        [Description("全廠")]
        All = 2,

        /// <summary>
        /// 噴塗系統
        /// </summary>
        [Description("噴塗系統")]
        Spray = 3,
    }

    /// <summary>
    /// 簽到結果
    /// </summary>
    public enum EquipmentClass
    {
        /// <summary>
        /// 菲力工業
        /// </summary>
        [Description("菲力工業")]
        Fairlybike = 0,
        /// <summary>
        /// 廠區
        /// </summary>
        [Description("廠區")]
        Factory = 1,

        /// <summary>
        /// 樓層
        /// </summary>
        [Description("樓層")]
        Floor = 2,

        /// <summary>
        /// 區域
        /// </summary>
        [Description("區域")]
        Area = 3,

        /// <summary>
        /// 設備
        /// </summary>
        [Description("設備")]
        Device = 4,

    }
    /// <summary>
    /// 廠商類型
    /// </summary>
    public enum ManufactureType
    {
        /// <summary>
        /// 類型一
        /// </summary>
        [Description("類型一")]
        Failed = 1,

        /// <summary>
        /// 類型二
        /// </summary>
        [Description("類型二")]
        NotSignable = 2,

        /// <summary>
        /// 類型三
        /// </summary>
        [Description("類型三")]
        NotInList = 3,
    }

    /// <summary>
    /// 審核類型
    /// </summary>
    public enum AuditType
    {
        /// <summary>
        /// 8D
        /// </summary>
        [Description("8D")]
        EightD = 1,
    }

    /// <summary>
    /// 審核狀態
    /// </summary>
    public enum AuditState
    {
        /// <summary>
        /// 等待承辦人簽核中
        /// </summary>
        [Description("等待承辦人簽核中")]
        Processing = 0,

        /// <summary>
        /// 待主管簽核中
        /// </summary>
        [Description("待主管簽核中")]
        OfficerAudit = 1,

        /// <summary>
        /// 待副總經理簽核中
        /// </summary>
        [Description("待副總經理簽核中")]
        ManagerAudit = 2,

        /// <summary>
        /// 待總經理簽核中
        /// </summary>
        [Description("待總經理簽核中")]
        ViceAudit = 3,

        /// <summary>
        /// 簽核完成
        /// </summary>
        [Description("簽核完成")]
        PresidentAudit = 4,

        /// <summary>
        /// 駁回
        /// </summary>
        [Description("駁回")]
        Reject = 6,

        /// <summary>
        /// 核准
        /// </summary>
        [Description("核准")]
        Approval = 7,
    }
    /// <summary>
    /// 表單簽核
    /// </summary>
    public enum FormAudit
    {
        /// <summary>
        /// 待審
        /// </summary>
        [Description("待審")]
        Processing = 1,

        /// <summary>
        /// 核准
        /// </summary>
        [Description("核准")]
        Approval = 2,

        /// <summary>
        /// 駁回
        /// </summary>
        [Description("駁回")]
        Reject = 3,
    }
}