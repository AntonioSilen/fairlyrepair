﻿using FairlyRepairNotify.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FairlyRepairNotify.Repositories
{
    public class AdminRepository : GenericRepository<Admin>
    {
        //private FairlyRepairDBEntity db;

        //public AdminRepository() : this(null) { }

        //public AdminRepository(FairlyRepairDBEntity context)
        //{
        //    db = context ?? new FairlyRepairDBEntity();
        //}
        T8ERPEntities t8db = new T8ERPEntities();

        public AdminRepository(FairlyRepairDBEntities context) : base(context) { }

        public IQueryable<Admin> Query(bool? status, int type = 0, string account = "")
        {
            var query = GetAll();
            if (status.HasValue)
            {
                query = query.Where(p => p.Status == status);
            }
            if (type != 0)
            {
                query = query.Where(q => q.Type == type);
            }
            if (!string.IsNullOrEmpty(account))
            {
                query = query.Where(q => q.Account.Contains(account));
            }
            return query;
        }

        public Admin Login(string account, string password)
        {
            var all = GetAll();
            Admin admin = all.Where(p => p.Account == account && p.Password == password && p.Status).FirstOrDefault();
            if (admin == null)
            {
                var t8GroupPerson = t8db.comGroupPerson.Where(q => q.PersonId == account).FirstOrDefault();
                if (t8GroupPerson != null && GetAll().Where(q => q.Account == account).Count() <= 0)
                {
                    admin = new Admin
                    {
                        Name = t8GroupPerson.PersonName,
                        Account = t8GroupPerson.PersonId,
                        Password = t8GroupPerson.Birthday.ToString(),
                        Type = (int)AdminType.Applicant,
                        Department = 0,
                        Status = true,
                        UpdateDate = DateTime.Now,
                        Updater = 1,
                        CreateDate = DateTime.Now,
                        Creater = 1,
                    };
                    int Id = Insert(admin);
                }
            }
            return admin;
        }
    }
}