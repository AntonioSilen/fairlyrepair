﻿using FairlyRepairNotify.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FairlyRepairNotify.Repositories
{
    public class RepairRecordFeeRepository : GenericRepository<RepairRecordFee>
    {
        public RepairRecordFeeRepository(FairlyRepairDBEntities context) : base(context) { }
        private FairlyRepairDBEntities db = new FairlyRepairDBEntities();

        public IQueryable<RepairRecordFee> Query(int rid = 0)
        {
            var query = GetAll();
            if (rid != 0)
            {
                query = query.Where(q => q.RecordID == rid);
            }

            return query;
        }

        public RepairRecordFee FindBy(int id)
        {
            return db.RepairRecordFee.Find(id);
        }

        public void DeleteFile(int id)
        {
            RepairRecordFee data = FindBy(id);

            db.SaveChanges();
        }
    }
}
