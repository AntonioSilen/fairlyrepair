﻿using FairlyRepairNotify.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FairlyRepairNotify.Repositories
{
    public class EightDCategoryRepository : GenericRepository<EightDCategory>
    {
        public EightDCategoryRepository(FairlyRepairDBEntities context) : base(context) { }
        private readonly FairlyRepairDBEntities db = new FairlyRepairDBEntities();

        public IQueryable<EightDCategory> Query(bool? status, string title, int type = 0)
        {
            var query = GetAll();
            if (status.HasValue)
            {
                query = query.Where(q => q.IsOnline == status);
            }       
            if (!string.IsNullOrEmpty(title))
            {
                query = query.Where(q => q.Title.Contains(title));
            }
            if (type != 0)
            {
                query = query.Where(q => q.Type == type);
            }
          
            return query;
        }
    }
}