﻿using FairlyRepairNotify.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FairlyRepairNotify.Repositories
{
    public class DeviceRepository : GenericRepository<Device>
    {
        public DeviceRepository(FairlyRepairDBEntities context) : base(context) { }
        private FairlyRepairDBEntities db = new FairlyRepairDBEntities();

        public IQueryable<Device> Query(int dClass = 0, int parent = 0)
        {
            var query = GetAll();

            if (dClass != 0)
            {
                query = query.Where(q => q.Class == dClass);
            }
            if (parent != 0)
            {
                query = query.Where(q => q.Parent == parent);
            }
       
            return query;
        }

        public Device FindBy(int id)
        {
            return db.Device.Find(id);
        }

        public void DeleteFile(int id)
        {
            Device data = FindBy(id);

            db.SaveChanges();
        }
    }
}