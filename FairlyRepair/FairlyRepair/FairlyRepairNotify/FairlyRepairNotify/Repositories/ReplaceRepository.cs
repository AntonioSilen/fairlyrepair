﻿using FairlyRepairNotify.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FairlyRepairNotify.Repositories
{
    public class ReplaceRepository : GenericRepository<Replace>
    {
        public ReplaceRepository(FairlyRepairDBEntities context) : base(context) { }
        private FairlyRepairDBEntities db = new FairlyRepairDBEntities();

        public IQueryable<Replace> Query(bool? isPres = null, bool? isVice = null,
            int? presStatus = null, int? viceStatus = null,
            int deviceId = 0, int factory = 0)
        {
            var query = GetAll();
            if (isPres.HasValue)
            {
                query = query.Where(q => q.IsPresApproval == isPres);
            }
            if (isVice.HasValue)
            {
                query = query.Where(q => q.IsViceApproval == isVice);
            }
            if (presStatus.HasValue)
            {
                query = query.Where(q => q.PresAppStatus == presStatus);
            }
            if (viceStatus.HasValue)
            {
                query = query.Where(q => q.ViceAppStatus == viceStatus);
            }
            if (deviceId != 0)
            {
                query = query.Where(q => q.DeviceID == deviceId);
            }
            if (factory != 0)
            {
                query = query.Where(q => q.FactoryID == factory);
            }

            return query;
        }

        public Replace FindBy(int id)
        {
            return db.Replace.Find(id);
        }

        public void DeleteFile(int id)
        {
            Replace data = FindBy(id);

            db.SaveChanges();
        }

    }
}