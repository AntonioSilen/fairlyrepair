﻿using FairlyRepairNotify.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FairlyRepairNotify.Repositories
{
    public class RepairRecordImageRepository : GenericRepository<RepairRecordImage>
    {
        public RepairRecordImageRepository(FairlyRepairDBEntities context) : base(context) { }
        private FairlyRepairDBEntities db = new FairlyRepairDBEntities();

        public IQueryable<RepairRecordImage> Query(int rid = 0)
        {
            var query = GetAll();
            if (rid != 0)
            {
                query = query.Where(q => q.RecordID == rid);
            }
       
            return query;
        }

        public RepairRecordImage FindBy(int id)
        {
            return db.RepairRecordImage.Find(id);
        }

        public void DeleteFile(int id)
        {
            RepairRecordImage data = FindBy(id);

            db.SaveChanges();
        }
    }
}