﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace FairlyRepairNotify.Repositories.OracleRepositories
{
    public class RepositorySetting
    {
        public String g_OracleQCConnectionString = @"Data Source=
                                                           (DESCRIPTION =
                                                             (ADDRESS_LIST =
                                                               (ADDRESS = (PROTOCOL = TCP)(HOST = nts8)(PORT = 1521))
                                                             )
                                                             (CONNECT_DATA =
                                                               (SERVICE_NAME = orcl)
                                                             )
                                                           )
                                                         ;Persist Security Info=True;User ID=fairlyvin;Password=fairlyvin";

        public static class CommonMethod
        {
            public static List<T> ConvertToList<T>(DataTable dt)
            {
                if (dt == null)
                {
                    return null;
                }
                var columnNames = dt.Columns.Cast<DataColumn>().Select(c => c.ColumnName.ToLower()).ToList();

                //Type t = dt.GetType();
                //var properties = t.GetProperties();
                var properties = typeof(T).GetProperties();

                return dt.AsEnumerable().Select(row =>
                {
                    var objT = Activator.CreateInstance<T>();
                    foreach (var pro in properties)
                    {
                        if (columnNames.Contains(pro.Name.ToLower()))
                        {
                            try
                            {
                                var val = row[pro.Name];
                                var type = val.GetType();
                                switch (type.FullName)
                                {
                                    case "System.Int64":
                                        pro.SetValue(objT, Convert.ToInt32(val));
                                        break;
                                    case "System.Double":
                                        pro.SetValue(objT, Convert.ToDecimal(val));
                                        break;
                                    case "System.Int16":
                                        pro.SetValue(objT, Convert.ToBoolean(val));
                                        break;
                                    default:
                                        if (val == null)
                                        {
                                            pro.SetValue(objT, "");
                                        }
                                        else
                                        {
                                            pro.SetValue(objT, val);
                                        }
                                        break;
                                }
                                //if (type.FullName == "System.Int64")
                                //{
                                //    pro.SetValue(objT, Convert.ToInt32(val));
                                //}
                                //else if (type.FullName == "System.Double")
                                //{
                                //    pro.SetValue(objT, Convert.ToDecimal(val));
                                //}
                                //else if (type.FullName == "System.Int16")
                                //{
                                //    pro.SetValue(objT, Convert.ToBoolean(val));
                                //}
                                //else if (val == null)
                                //{
                                //    pro.SetValue(objT, "");
                                //}
                                //else
                                //{
                                //    pro.SetValue(objT, val);
                                //}                                
                            }
                            catch (Exception ex)
                            {
                                string msg = ex.Message;
                            }
                        }
                    }
                    return objT;
                }).ToList();
            }
        }
    }
}