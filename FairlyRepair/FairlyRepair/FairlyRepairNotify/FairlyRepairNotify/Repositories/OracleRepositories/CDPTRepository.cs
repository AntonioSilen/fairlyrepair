﻿using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace FairlyRepairNotify.Repositories.OracleRepositories
{
    public class CDPTRepository : RepositorySetting
    {
        public IQueryable<CDPTModel> GetAll()
        {
            OracleConnection oralceConnection = new OracleConnection(g_OracleQCConnectionString);
            oralceConnection.Open();

            // 查詢資料
            string sql = @"select distinct COD_CUST from FILY_CDPT order by COD_CUST";
           
            OracleCommand cmd = new OracleCommand(sql, oralceConnection);
            cmd.CommandType = CommandType.Text;

            OracleDataAdapter DataAdapter = new OracleDataAdapter();
            DataAdapter.SelectCommand = cmd;
            DataSet ds = new DataSet();
            DataAdapter.Fill(ds, "Table");
            DataTable dt = ds.Tables["Table"];
            var iQuery = new List<CDPTModel>().AsQueryable();
            if (dt.Rows.Count < 1)
            {
                oralceConnection.Close();
                oralceConnection.Dispose();
                return iQuery;
            }

            if (dt != null)
            {
                iQuery = CommonMethod.ConvertToList<CDPTModel>(dt).AsQueryable();
            }

            oralceConnection.Close();
            oralceConnection.Dispose();
            return iQuery;
        }
    }

    public class CDPTModel
    {
        public string COD_CUST { get; set; }
    }
}