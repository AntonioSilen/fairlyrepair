﻿using FairlyRepairNotify.Models;
using FairlyRepairNotify.Utility.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FairlyRepairNotify.Repositories
{
    public class AuditRepository : GenericRepository<Audit>
    {
        public AuditRepository(FairlyRepairDBEntities context) : base(context) { }
        private FairlyRepairDBEntities db = new FairlyRepairDBEntities();

        public IQueryable<Audit> Query(bool? isInvalid = null, int reportId = 0, int type = 0, int? state = null, string trainingTitle = "", string personId = "", string officer = "")
        {
            var query = GetAll();
            if (isInvalid.HasValue)
            {
                query = query.Where(q => q.IsInvalid == isInvalid);
            }
            if (reportId != 0)
            {
                query = query.Where(q => q.TargetID == reportId);
            }
            if (type != 0)
            {
                query = query.Where(q => q.Type == type);
            }
            if (state.HasValue)
            {//審核狀態
                if (!string.IsNullOrEmpty(officer))
                {//兼承辦人
                    query = query.Where(q => q.Officer == officer || q.State == state);                
                }
                else
                {
                    query = query.Where(q => q.State == state);
                }
            }
            else
            {//未審
                if (!string.IsNullOrEmpty(officer))
                {//兼承辦人
                    query = query.Where(q => q.Officer == officer);
                }
            }
            if (!string.IsNullOrEmpty(personId))
            {
                query = query.Where(q => q.PersonID == personId);
            }           
    
            return query;
        }

        public Audit FindBy(int id)
        {
            return db.Audit.Find(id);
        }

        public Audit FindByTarget(int targetId = 0, int type = 0, int reportId = 0, string personId = "")
        {
            var query = GetAll();
            if (targetId != 0)
            {
                query = query.Where(q => q.TargetID == targetId);
            }
            if (type != 0)
            {
                query = query.Where(q => q.Type == type);
            }
            if (reportId != 0)
            {
                query = query.Where(q => q.ReportID == reportId);
            }
            if (!string.IsNullOrEmpty(personId))
            {
                query = query.Where(q => q.PersonID == personId);
            }
            return query.FirstOrDefault();
        }
    }
}