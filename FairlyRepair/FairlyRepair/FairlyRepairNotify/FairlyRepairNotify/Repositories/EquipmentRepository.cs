﻿using FairlyRepairNotify.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FairlyRepairNotify.Repositories
{
    public class EquipmentRepository : GenericRepository<Equipment>
    {
        public EquipmentRepository(FairlyRepairDBEntities context) : base(context) { }
        private FairlyRepairDBEntities db = new FairlyRepairDBEntities();

        public IQueryable<Equipment> Query(bool? status, int parent = 0, int cls = 0)
        {
            var query = GetAll();

            if (status.HasValue)
            {
                query = query.Where(q => q.IsOnline == status);
            }
            if (parent != 0)
            {
                query = query.Where(q => q.Parent == parent);
            }
            if (cls != 0)
            {
                query = query.Where(q => q.Class == cls);
            }
       
            return query;
        }

        public Equipment FindBy(int id)
        {
            return db.Equipment.Find(id);
        }

        public void DeleteFile(int id)
        {
            Equipment data = FindBy(id);

            db.SaveChanges();
        }

        public Equipment GetInfoById(int id)
        {
            return db.Equipment.Find(id);
        }

        public string GetEquipmentTitle(int id)
        {
            string title = "";

            var info = FindBy(id);
            if (info != null)
            {
                title = info.Title + " " + title;

                while (info.Parent != 0)
                {
                    info = FindBy(info.Parent);
                    if (info == null)
                    {
                        return title;
                    }
                    title = info.Title + " " + title;
                }
            }
            return "菲力工業 " + title;
        }

        //public List<BreadModel> GetBreadList(int id)
        //{
        //    List<BreadModel> resultList = new List<BreadModel>();

        //    var info = FindBy(id);            
        //    if (info == null)
        //    {
        //        resultList.Add(new BreadModel
        //        {
        //            Controller = "EquipmentAdmin",
        //            Action = "Index",
        //            Query = "",
        //            Title = $"【設備管理】",
        //            Sort = 1
        //        });
        //        return resultList;
        //    }
    
        //    int i = 1;
        //    while (1==1)
        //    {
        //        resultList.Add(new BreadModel
        //        {
        //            Controller = "EquipmentAdmin",
        //            Action = "Index",
        //            Query = "Parent=" + info.ID,
        //            Title = $"【{info.Title}】附屬項目",
        //            Sort = i + 1
        //        });
        //        i++;
        //        resultList.Add(new BreadModel
        //        {
        //            Controller = "EquipmentAdmin",
        //            Action = "Edit",
        //            Query = "id=" + info.ID,
        //            Title = $"【{info.Title}】",
        //            Sort = i + 1
        //        });
        //        i++;
        //        info = FindBy(info.Parent);
        //        if (info == null)
        //        {
        //            resultList.Add(new BreadModel
        //            {
        //                Controller = "EquipmentAdmin",
        //                Action = "Index",
        //                Query = "",
        //                Title = $"【設備管理】",
        //                Sort = i + 1
        //            });
        //            return resultList;
        //        }
        //    }            
        //}

    }
}