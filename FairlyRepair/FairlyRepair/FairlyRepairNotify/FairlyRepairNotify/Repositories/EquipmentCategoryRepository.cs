﻿using FairlyRepairNotify.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FairlyRepairNotify.Repositories
{
    public class EquipmentCategoryRepository : GenericRepository<EquipmentCategory>
    {
        public EquipmentCategoryRepository(FairlyRepairDBEntities context) : base(context) { }
        private FairlyRepairDBEntities db = new FairlyRepairDBEntities();

        public IQueryable<EquipmentCategory> Query(bool? status)
        {
            var query = GetAll();

            if (status.HasValue)
            {
                query = query.Where(q => q.IsOnline == status);
            }

            return query;
        }

        public EquipmentCategory FindBy(int id)
        {
            return db.EquipmentCategory.Find(id);
        }
    }
}