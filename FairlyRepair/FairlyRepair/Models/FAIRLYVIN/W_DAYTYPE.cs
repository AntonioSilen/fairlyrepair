﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FairlyRepair.Models.FAIRLYVIN
{
    public class W_DAYTYPE
    {
        public string TYPE_NUM { get; set; }
        public string TYPE_NAME { get; set; }
        public string TYPE_CATE { get; set; }
    }
}