﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FairlyRepair.Models.FAIRLYVIN
{
    public class W_DAYN_NOTE
    {
        public string DIARYDATE { get; set; }
        public string TYPE_NUM { get; set; }
        public string DATE_NOTE { get; set; }
        public string DATE_PIC1 { get; set; }
        public string ENTER_USER { get; set; }
        public string COD_CUST { get; set; }
        public string NAM_ITEM { get; set; }
        public string LIN_PSCT { get; set; }
    }
}