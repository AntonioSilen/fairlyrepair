﻿using AutoMapper;
using FairlyRepair.Areas.Admin.ViewModels.Approval;
using FairlyRepair.Areas.Admin.ViewModels.EightD;
using FairlyRepair.Areas.Admin.ViewModels.EightDCategory;
using FairlyRepair.Areas.Admin.ViewModels.Emissions;
using FairlyRepair.Areas.Admin.ViewModels.Equipment;
using FairlyRepair.Areas.Admin.ViewModels.Manager;
using FairlyRepair.Areas.Admin.ViewModels.Manufacture;
using FairlyRepair.Areas.Admin.ViewModels.ProductionDiary;
using FairlyRepair.Areas.Admin.ViewModels.ProductionType;
using FairlyRepair.Areas.Admin.ViewModels.Repair;
using FairlyRepair.Areas.Admin.ViewModels.RepairRecord;
using FairlyRepair.Areas.Admin.ViewModels.Replace;
using FairlyRepair.Models;
using FairlyRepair.Models.FAIRLYVIN;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FairlyRepair.App_Start
{
    public class AutoMapperConfig
    {
        public static void Initialize()
        {
            Mapper.Initialize(cfg =>
            {
                #region 後台            
                cfg.CreateMap<Admin, ManagerView>();
                cfg.CreateMap<ManagerView, Admin>();

                cfg.CreateMap<Repair, RepairView>();
                cfg.CreateMap<RepairView, Repair>();
                cfg.CreateMap<Repair, ApprovalView>();
                cfg.CreateMap<ApprovalView, Repair>();

                cfg.CreateMap<RepairRecord, RepairRecordView>();
                cfg.CreateMap<RepairRecordView, RepairRecord>();
                cfg.CreateMap<RepairRecordFee, RepairRecordFeeView>();
                cfg.CreateMap<RepairRecordFeeView, RepairRecordFee>();

                cfg.CreateMap<Replace, ReplaceView>();
                cfg.CreateMap<ReplaceView, Replace>();
                cfg.CreateMap<Replace, ApprovalView>();
                cfg.CreateMap<ApprovalView, Replace>();

                cfg.CreateMap<Manufacture, ManufactureView>();
                cfg.CreateMap<ManufactureView, Manufacture>();

                cfg.CreateMap<Equipment, EquipmentView>();
                cfg.CreateMap<EquipmentView, Equipment>();

                cfg.CreateMap<EightD, EightDView>();
                cfg.CreateMap<EightDView, EightD>();

                cfg.CreateMap<EightDCategory, EightDCategoryView>();
                cfg.CreateMap<EightDCategoryView, EightDCategory>();

                cfg.CreateMap<W_DAYN_NOTE, ProductionDiaryView>();
                cfg.CreateMap<ProductionDiaryView, W_DAYN_NOTE>();

                cfg.CreateMap<W_DAYTYPE, ProductionTypeView>();
                cfg.CreateMap<ProductionTypeView, W_DAYTYPE>();

                cfg.CreateMap<Emissions, EmissionsView>();
                cfg.CreateMap<EmissionsView, Emissions>();

                cfg.CreateMap<EmissionsT2, EmissionsT2View>();
                cfg.CreateMap<EmissionsT2View, EmissionsT2>();

                cfg.CreateMap<EmissionsT3C1, EmissionsT3C1View>();
                cfg.CreateMap<EmissionsT3C1View, EmissionsT3C1>();

                cfg.CreateMap<EmissionsT4C1, EmissionsT4C1View>();
                cfg.CreateMap<EmissionsT4C1View, EmissionsT4C1>();

                cfg.CreateMap<EmissionsT6, EmissionsT6View>();
                cfg.CreateMap<EmissionsT6View, EmissionsT6>();

                //cfg.CreateMap<Audit, AuditView>();
                //cfg.CreateMap<AuditView, Audit>();

                #endregion

                #region 前台
                cfg.CreateMap<Repair, ViewModels.Repair.RepairView>();
                cfg.CreateMap<ViewModels.Repair.RepairView, Repair>();


                #endregion
            });
        }
    }
}