
//廠區變更
function ChangeFactory() {
    var factoryId = $('#FactoryID').val();
    $('#FloorID').children().remove();
    $('#AreaID').children().remove();
    $('#DeviceID').children().remove();
    if (factoryId == '0') {
        return false;
    }
    $.ajax({
        url: "/Repair/GetFloor",
        type: 'POST',
        anysc: false,
        dataType: "json",
        data: {
            factoryId: factoryId
        },
        success: function (response) {
            console.log(response);
            $.map(response, function (data) {
                var selected = data.ID == factoryId ? 'selected' : '';
                $('#FloorID').append('<option value="' + data.ID + '" ' + selected + '>' + data.Title + '</option>');
                if (selected == 'selected') {
                    ChangeFloor();
                }
            });
        }
    });
}
//樓層變更
function ChangeFloor() {
    var floorId = $('#FloorID').val();
    $('#AreaID').children().remove();
    $('#DeviceID').children().remove();
    if (floorId == '0') {
        return false;
    }
    $.ajax({
        url: "/Repair/GetArea",
        type: 'POST',
        anysc: false,
        dataType: "json",
        data: {
            floorId: floorId
        },
        success: function (response) {
            console.log(response);
            $.map(response, function (data) {
                var selected = data.ID == factoryId ? 'selected' : '';
                $('#AreaID').append('<option value="' + data.ID + '" ' + selected + '>' + data.Title + '</option>');
                if (selected == 'selected') {
                    ChangeArea();
                }
            });
        }
    });
}
//區域變更
function ChangeArea() {
    var areaId = $('#AreaID').val();
    $('#DeviceID').children().remove();
    if (areaId == '0') {
        return false;
    }
    $.ajax({
        url: "/Repair/GetDevice",
        type: 'POST',
        anysc: false,
        dataType: "json",
        data: {
            areaId: areaId
        },
        success: function (response) {
            console.log(response);
            $.map(response, function (data) {
                var selected = data.ID == factoryId ? 'selected' : '';
                $('#DeviceID').append('<option value="' + data.ID + '" ' + selected + '>' + data.Title + '</option>');
            });
        }
    });
}