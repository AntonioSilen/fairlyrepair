﻿using FairlyRepair.ActionFilters;
using FairlyRepair.Areas.Admin.ViewModels.Auth;
using FairlyRepair.Models;
using FairlyRepair.Models.Others;
using FairlyRepair.Repositories;
using FairlyRepair.Utility.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace FairlyRepair.Areas.Admin.Controllers
{
    [ErrorHandleAdminActionFilter]
    public class AuthAdminController : BaseAdminController
    {
        private AdminRepository adminRepository;
        private Repositories.T8Repositories.PersonRepository personRepository;

        public AuthAdminController() : this(null, null) { }

        public AuthAdminController(AdminRepository repo, Repositories.T8Repositories.PersonRepository repo2)
        {
            adminRepository = repo ?? new AdminRepository(new FairlyRepairDBEntities());
            personRepository = repo2 ?? new Repositories.T8Repositories.PersonRepository(new T8ERPEntities());
        }

        // GET: Admin/AccountAdmin
        public ActionResult Login(string ReturnUrl = "")
        {
            if (AdminInfoHelper.GetAdminInfo() != null)
            {
                return RedirectToAction("Index", "HomeAdmin");
            }
            //#if DEBUG
            //            if (System.Diagnostics.Debugger.IsAttached)
            //            {
            //                var controller = DependencyResolver.Current.GetService<AuthAdminController>();
            //                controller.ControllerContext = new ControllerContext(this.Request.RequestContext, controller);
            //                return controller.Login(new LoginView() { Account = "fairlyadmin", Password = "123456" });
            //            }
            //#endif
            //var adminInfo = AdminInfoHelper.GetAdminInfo();
            //if (adminInfo != null)
            //{
            //    var controller = DependencyResolver.Current.GetService<AuthAdminController>();
            //    controller.ControllerContext = new ControllerContext(this.Request.RequestContext, controller);

            //    var loginInfo = adminRepository.GetById(adminInfo.ID);
            //    if (loginInfo != null && loginInfo.Type <= (int)MeetAdminType.Manager)
            //    {
            //        return controller.Login(new LoginView() { Account = loginInfo.Account, Password = loginInfo.Password });
            //    }
            //}

            var login = new LoginView();
            login.ReturnUrl = ReturnUrl;
            return View(login);
        }

        /// <summary>
        /// 登入
        /// </summary>
        /// <param name="login"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Login(LoginView login)
        {
            if (ModelState.IsValid)
            {
                var admin = adminRepository.Login(login.Account, login.Password);
                if (admin != null)
                {
                    SetLogin(login, admin);

                    //var ReturnUrl = Request.QueryString["ReturnUrl"];
                    if (!string.IsNullOrEmpty(login.ReturnUrl) && login.ReturnUrl.IndexOf("AuthAdmin") < 0)
                    {
                        return Redirect(login.ReturnUrl);
                    }
                    else
                    {
                        //var ReturnUrl = FormsAuthentication.GetRedirectUrl(login.Account, false);
                        return RedirectToAction("Index", "HomeAdmin");
                    }

                    //var mainAdmin = GetMainAccountInfo(admin.Account);
                    //AdminInfo adminInfo = new AdminInfo();
                    //adminInfo.ID = admin.ID;
                    //adminInfo.Account = admin.Account;
                    //adminInfo.Type = admin.Type;
                    //adminInfo.AccountType = mainAdmin.Type;
                    //adminInfo.Name = mainAdmin.Name;
                    //adminInfo.DeptId = mainAdmin.DeptId;
                    //adminInfo.Department = mainAdmin.Department;
                    //AdminInfoHelper.Login(adminInfo, login.RememberMe);

                    //if (FormsAuthentication.GetRedirectUrl(login.Account,false).IndexOf("AuthAdmin") < 0)
                    //{
                    //    return Redirect(FormsAuthentication.GetRedirectUrl(login.Account, false));
                    //}
                    //return RedirectToAction("Index", "Home");
                }
                else
                {
                    var t8Person = personRepository.GetPersonQuery("", login.Account).FirstOrDefault();
                    if (t8Person != null)
                    {
                        if (adminRepository.Query(null, 0, login.Account).FirstOrDefault() == null)
                        {
                            DateTime now = DateTime.Now;
                            var newPerson = new Models.Admin
                            {
                                Account = login.Account,
                                Password = t8Person.Birthday.ToString(),
                                Type = (int)AdminType.Applicant,
                                Name = t8Person.Name,
                                DeptId = t8Person.DeptId,
                                Status = true,
                                UpdateDate = now,
                                Updater = 1,
                                CreateDate = now,
                                Creater = 1
                            };
                            newPerson.ID = adminRepository.Insert(newPerson);

                            if (login.Password == newPerson.Password)
                            {
                                SetLogin(login, newPerson);

                                if (!string.IsNullOrEmpty(login.ReturnUrl) && login.ReturnUrl.IndexOf("AuthAdmin") < 0)
                                {
                                    return Redirect(login.ReturnUrl);
                                }
                                else
                                {
                                    return RedirectToAction("Index", "HomeAdmin");
                                }
                            }
                        }
                    }
                }
            }
            return View(login);
        }

        private static void SetLogin(LoginView login, Models.Admin admin)
        {
            Repositories.MeetRepositories.AdminRepository meetAdminRepository = new Repositories.MeetRepositories.AdminRepository(new MeetModels.MeetingSignInDBEntities());
            var mainAdmin = meetAdminRepository.GetByAccount(login.Account);
            AdminInfo adminInfo = new AdminInfo();
            adminInfo.ID = admin.ID;
            adminInfo.Account = admin.Account;
            adminInfo.Type = admin.Type;
            adminInfo.AccountType = mainAdmin == null ? (int)MeetAdminType.Employee : mainAdmin.Type;
            adminInfo.Name = admin.Name;
            AdminInfoHelper.Login(adminInfo, login.RememberMe);
        }


        [Authorize]
        public ActionResult Logout()
        {
            AdminInfoHelper.Logout();
            return RedirectToAction("Login");
        }
    }
}