﻿using AutoMapper;
using FairlyRepair.Models;
using FairlyRepair.Models.Others;
using FairlyRepair.Repositories.T8Repositories;
using FairlyRepair.Repositories;
using FairlyRepair.Utility;
using FairlyRepair.Utility.Helpers;
using Ical.Net.DataTypes;
using Ical.Net.Serialization;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using FairlyRepair.ActionFilters;
using System.Net;
using System.Configuration;
using FairlyRepair.Areas.Admin.ViewModels.Emissions;
using OfficeOpenXml;

namespace FairlyRepair.Areas.Admin.Controllers
{
    [Authorize]
    //[ErrorHandleAdminActionFilter]
    public class EmissionsAdminController : BaseAdminController
    {
        private AdminRepository adminRepository = new AdminRepository(new FairlyRepairDBEntities());
        private EmissionsRepository emissionsRepository = new EmissionsRepository(new FairlyRepairDBEntities());
        private EmissionsT2Repository emissionsT2Repository = new EmissionsT2Repository(new FairlyRepairDBEntities());
        private EmissionsT3C1Repository emissionsT3C1Repository = new EmissionsT3C1Repository(new FairlyRepairDBEntities());
        private EmissionsT4C1Repository emissionsT4C1Repository = new EmissionsT4C1Repository(new FairlyRepairDBEntities());
        private EmissionsT6Repository emissionsT6Repository = new EmissionsT6Repository(new FairlyRepairDBEntities());
        private comGroupPersonRepository comGroupPersonRepository = new comGroupPersonRepository(new T8ERPEntities());

        // GET: Admin/EmissionsAdmin
        public ActionResult Index(EmissionsIndexView model, int dt = 0)
        {           
            var query = emissionsRepository.Query();

            var pageResult = query.ToPageResult<Emissions>(model);
            model.PageResult = Mapper.Map<PageResult<EmissionsView>>(pageResult);

            return View(model);
        }

        public ActionResult Edit(int id = 0)
        {
            var model = new EmissionsView();
            model.EmissionsT2 = new EmissionsT2View();
            model.EmissionsT3C1 = new EmissionsT3C1View();
            model.EmissionsT4C1 = new EmissionsT4C1View();
            model.EmissionsT6 = new EmissionsT6View();
            var adminInfo = AdminInfoHelper.GetAdminInfo();
            if (id == 0)
            {
                
            }
            else
            {
                var query = emissionsRepository.FindBy(id);
                model = Mapper.Map<EmissionsView>(query);
                var t2Query = emissionsT2Repository.GetByEmissionsId(id);
                model.EmissionsT2 = t2Query == null ? new EmissionsT2View() : Mapper.Map<EmissionsT2View>(t2Query);
                var t3C1Query = emissionsT3C1Repository.GetByEmissionsId(id);
                model.EmissionsT3C1 = t3C1Query == null ? new EmissionsT3C1View() : Mapper.Map<EmissionsT3C1View>(t3C1Query);
                var t4C1Query = emissionsT4C1Repository.GetByEmissionsId(id);
                model.EmissionsT4C1 = t4C1Query == null ? new EmissionsT4C1View() : Mapper.Map<EmissionsT4C1View>(t4C1Query);
                var t6Query = emissionsT6Repository.GetByEmissionsId(id);
                model.EmissionsT6 = t6Query == null ? new EmissionsT6View() : Mapper.Map<EmissionsT6View>(t6Query);
            }
            if (model.EmissionDate < Convert.ToDateTime("2000/01/01"))
            {
                model.EmissionDate = DateTime.Now;
            }

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public ActionResult Edit(EmissionsView model)
        {
            if (ModelState.IsValid)
            {
                Emissions data = Mapper.Map<Emissions>(model);
                var adminInfo = AdminInfoHelper.GetAdminInfo();
                int adminId = adminInfo.ID;

                if (model.ID == 0)
                {
                    model.ID = emissionsRepository.Insert(data);
                    ShowMessage(true, "新增成功");
                }
                else
                {
                    emissionsRepository.Update(data);
                    ShowMessage(true, "修改成功");
                }

                return RedirectToAction("Edit", new { id = model.ID });
            }

            return View(model);
        }

        public string SaveT2(int id, string emissionsdate, string remark, EmissionsT2View data)
        {
            try
            {
                DateTime EmissionsDate = DateTime.Now;
                if (!string.IsNullOrEmpty(emissionsdate) && Convert.ToDateTime(emissionsdate) > Convert.ToDateTime("2000/01/01"))
                {
                    EmissionsDate = Convert.ToDateTime(emissionsdate);
                }
                var dataObj = Mapper.Map<EmissionsT2>(data);
                if (id == 0)
                {
                    int Id = emissionsRepository.Insert(new Emissions()
                    {
                        EmissionDate = EmissionsDate,
                        Remark = remark,
                        CreateDate = DateTime.Now
                    });
                    dataObj.EmissionID = Id;
                    emissionsT2Repository.Insert(dataObj);                    
                }
                else
                {
                    var emissionsData = emissionsRepository.GetById(id);
                    emissionsData.EmissionDate = Convert.ToDateTime(emissionsdate);
                    emissionsData.Remark = remark;
                    emissionsRepository.Update(emissionsData);

                    var query = emissionsT2Repository.GetByEmissionsId(id);
                    if (query == null)
                    {
                        dataObj.EmissionID = id;
                        emissionsT2Repository.Insert(dataObj);
                    }
                    else
                    {
                        emissionsT2Repository.Update(dataObj);
                    }                    
                }
                return id.ToString();
            }
            catch (Exception e)
            {
                return "0";
            }
        }

        public string SaveT3C1(int id, string emissionsdate, string remark, EmissionsT3C1View data)
        {
            try
            {
                DateTime EmissionsDate = DateTime.Now;
                if (!string.IsNullOrEmpty(emissionsdate) && Convert.ToDateTime(emissionsdate) > Convert.ToDateTime("2000/01/01"))
                {
                    EmissionsDate = Convert.ToDateTime(emissionsdate);
                }
                var dataObj = Mapper.Map<EmissionsT3C1>(data);
                if (id == 0)
                {
                    int Id = emissionsRepository.Insert(new Emissions()
                    {
                        EmissionDate = EmissionsDate,
                        Remark = remark,
                        CreateDate = DateTime.Now
                    });
                    dataObj.EmissionID = Id;
                    emissionsT3C1Repository.Insert(dataObj);
                }
                else
                {
                    var emissionsData = emissionsRepository.GetById(id);
                    emissionsData.EmissionDate = Convert.ToDateTime(emissionsdate);
                    emissionsData.Remark = remark;
                    emissionsRepository.Update(emissionsData);

                    var query = emissionsT3C1Repository.GetByEmissionsId(id);
                    if (query == null)
                    {
                        dataObj.EmissionID = id;
                        emissionsT3C1Repository.Insert(dataObj);
                    }
                    else
                    {
                        emissionsT3C1Repository.Update(dataObj);
                    }
                }
                return id.ToString();
            }
            catch (Exception e)
            {
                return "0";
            }
        }

        public string SaveT4C1(int id, string emissionsdate, string remark, EmissionsT4C1View data)
        {
            try
            {
                DateTime EmissionsDate = DateTime.Now;
                if (!string.IsNullOrEmpty(emissionsdate) && Convert.ToDateTime(emissionsdate) > Convert.ToDateTime("2000/01/01"))
                {
                    EmissionsDate = Convert.ToDateTime(emissionsdate);
                }
                var dataObj = Mapper.Map<EmissionsT4C1>(data);
                if (id == 0)
                {
                    int Id = emissionsRepository.Insert(new Emissions()
                    {
                        EmissionDate = EmissionsDate,
                        Remark = remark,
                        CreateDate = DateTime.Now
                    });
                    dataObj.EmissionID = Id;
                    emissionsT4C1Repository.Insert(dataObj);
                }
                else
                {
                    var emissionsData = emissionsRepository.GetById(id);
                    emissionsData.EmissionDate = Convert.ToDateTime(emissionsdate);
                    emissionsData.Remark = remark;
                    emissionsRepository.Update(emissionsData);

                    var query = emissionsT4C1Repository.GetByEmissionsId(id);
                    if (query == null)
                    {
                        dataObj.EmissionID = id;
                        emissionsT4C1Repository.Insert(dataObj);
                    }
                    else
                    {
                        emissionsT4C1Repository.Update(dataObj);
                    }
                }
                return id.ToString();
            }
            catch (Exception e)
            {
                return "0";
            }
        }

        public string SaveT6(int id, string emissionsdate, string remark, EmissionsT6View data)
        {
            try
            {
                DateTime EmissionsDate = DateTime.Now;
                if (!string.IsNullOrEmpty(emissionsdate) && Convert.ToDateTime(emissionsdate) > Convert.ToDateTime("2000/01/01"))
                {
                    EmissionsDate = Convert.ToDateTime(emissionsdate);
                }
                var dataObj = Mapper.Map<EmissionsT6>(data);
                if (id == 0)
                {
                    int Id = emissionsRepository.Insert(new Emissions()
                    {
                        EmissionDate = EmissionsDate,
                        Remark = remark,
                        CreateDate = DateTime.Now
                    });
                    dataObj.EmissionID = Id;
                    emissionsT6Repository.Insert(dataObj);
                }
                else
                {
                    var emissionsData = emissionsRepository.GetById(id);
                    emissionsData.EmissionDate = Convert.ToDateTime(emissionsdate);
                    emissionsData.Remark = remark;
                    emissionsRepository.Update(emissionsData);

                    var query = emissionsT6Repository.GetByEmissionsId(id);
                    if (query == null)
                    {
                        dataObj.EmissionID = id;
                        emissionsT6Repository.Insert(dataObj);
                    }
                    else
                    {
                        emissionsT6Repository.Update(dataObj);
                    }
                }
                return id.ToString();
            }
            catch (Exception e)
            {
                return "0";
            }
        }

        /// <summary>
        /// 匯出Excel (非同步)
        /// </summary>
        /// <returns></returns>
        public ActionResult ExportEmissions(int id)
        {
            //取出要匯出Excel的資料
            var query = emissionsRepository.GetById(id);
            EmissionsView emissionsInfo = Mapper.Map<EmissionsView>(query);
            var t2Query = emissionsT2Repository.GetByEmissionsId(id);
            var emissionsT2 = t2Query == null ? new EmissionsT2View() : Mapper.Map<EmissionsT2View>(t2Query);
            var t3C1Query = emissionsT3C1Repository.GetByEmissionsId(id);
            var emissionsT3C1 = t3C1Query == null ? new EmissionsT3C1View() : Mapper.Map<EmissionsT3C1View>(t3C1Query);
            var t4C1Query = emissionsT4C1Repository.GetByEmissionsId(id);
            var emissionsT4C1 = t4C1Query == null ? new EmissionsT4C1View() : Mapper.Map<EmissionsT4C1View>(t4C1Query);
            var t6Query = emissionsT6Repository.GetByEmissionsId(id);
            var emissionsT6 = t6Query == null ? new EmissionsT6View() : Mapper.Map<EmissionsT6View>(t6Query);

            try
            {
                ExcelPackage.LicenseContext = LicenseContext.NonCommercial; // 關閉新許可模式通知
                //建立Excel
                ExcelPackage ep = new ExcelPackage();

                #region Emissions
                ExcelWorksheet sheet = ep.Workbook.Worksheets.Add("碳排放紀錄");
                int col = 1;
                int row = 1;
                sheet.Cells[row++, 1].Value = "紀錄時間";
                sheet.Cells[row++, 1].Value = "填寫人";
                sheet.Cells[row++, 1].Value = "備註";

                col = 2;
                row = 1;
                sheet.Cells[row++, col].Value = emissionsInfo.EmissionDate.ToString("yyyy/HH/mm HH:mm");
                var personInfo = comGroupPersonRepository.FindByPersonID(emissionsInfo.PersonId);
                sheet.Cells[row++, col].Value = personInfo.PersonName;
                sheet.Cells[row++, col].Value = emissionsInfo.Remark;
                for (int i = 1; i <= 2; i++)
                {
                    sheet.Column(i).AutoFit();
                }
                #endregion

                #region T2
                //建立第一個Sheet，後方為定義Sheet的名稱
                ExcelWorksheet sheetT2 = ep.Workbook.Worksheets.Add("彙整表二、全廠七大溫室氣體排放量統計表");

                col = 1;
                row = 1;
                sheetT2.Cells[row++, 1].Value = "CO2";
                sheetT2.Cells[row++, 1].Value = "CH4";
                sheetT2.Cells[row++, 1].Value = "N2O";
                sheetT2.Cells[row++, 1].Value = "HFCs";
                sheetT2.Cells[row++, 1].Value = "PFCs";
                sheetT2.Cells[row++, 1].Value = "SF6";
                sheetT2.Cells[row++, 1].Value = "NF3";
                sheetT2.Cells[row++, 1].Value = "七種溫室氣體年總排放當量";
                sheetT2.Cells[row++, 1].Value = "生質排放當量";

                col = 2;
                row = 1;
                sheetT2.Cells[row++, col].Value = emissionsT2.CO2;
                sheetT2.Cells[row++, col].Value = emissionsT2.CH4;
                sheetT2.Cells[row++, col].Value = emissionsT2.N2O;
                sheetT2.Cells[row++, col].Value = emissionsT2.HFCS;
                sheetT2.Cells[row++, col].Value = emissionsT2.PFCS;
                sheetT2.Cells[row++, col].Value = emissionsT2.SF6;
                sheetT2.Cells[row++, col].Value = emissionsT2.NF3;
                sheetT2.Cells[row++, col].Value = emissionsT2.GreenHouseGases;
                sheetT2.Cells[row++, col].Value = emissionsT2.Biomass;

                col = 3;
                row = 1;
                sheetT2.Cells[row++, col].Value = emissionsT2.CO2Percent + " %";
                sheetT2.Cells[row++, col].Value = emissionsT2.CH4Percent + " %";
                sheetT2.Cells[row++, col].Value = emissionsT2.N2OPercent + " %";
                sheetT2.Cells[row++, col].Value = emissionsT2.HFCSPercent + " %";
                sheetT2.Cells[row++, col].Value = emissionsT2.PFCSPercent + " %";
                sheetT2.Cells[row++, col].Value = emissionsT2.SF6Percent + " %";
                sheetT2.Cells[row++, col].Value = emissionsT2.NF3Percent + " %";
                sheetT2.Cells[row++, col].Value = emissionsT2.GreenHouseGasesPercent + " %";
                sheetT2.Cells[row++, col].Value = "-";

                for (int i = 1; i <= 3; i++)
                {
                    sheetT2.Column(i).AutoFit();
                }
                #endregion

                #region T3C1
                //建立第一個Sheet，後方為定義Sheet的名稱
                ExcelWorksheet sheetT3C1 = ep.Workbook.Worksheets.Add("彙整表三、範疇一七大溫室氣體排放量統計表");

                col = 1;
                row = 1;
                sheetT3C1.Cells[row++, 1].Value = "CO2";
                sheetT3C1.Cells[row++, 1].Value = "CH4";
                sheetT3C1.Cells[row++, 1].Value = "N2O";
                sheetT3C1.Cells[row++, 1].Value = "HFCs";
                sheetT3C1.Cells[row++, 1].Value = "PFCs";
                sheetT3C1.Cells[row++, 1].Value = "SF6";
                sheetT3C1.Cells[row++, 1].Value = "NF3";
                sheetT3C1.Cells[row++, 1].Value = "七種溫室氣體年總排放當量";

                col = 2;
                row = 1;
                sheetT3C1.Cells[row++, col].Value = emissionsT3C1.CO2;
                sheetT3C1.Cells[row++, col].Value = emissionsT3C1.CH4;
                sheetT3C1.Cells[row++, col].Value = emissionsT3C1.N2O;
                sheetT3C1.Cells[row++, col].Value = emissionsT3C1.HFCS;
                sheetT3C1.Cells[row++, col].Value = emissionsT3C1.PFCS;
                sheetT3C1.Cells[row++, col].Value = emissionsT3C1.SF6;
                sheetT3C1.Cells[row++, col].Value = emissionsT3C1.NF3;
                sheetT3C1.Cells[row++, col].Value = emissionsT3C1.GreenHouseGases;
              

                col = 3;
                row = 1;
                sheetT3C1.Cells[row++, col].Value = emissionsT3C1.CO2Percent + " %";
                sheetT3C1.Cells[row++, col].Value = emissionsT3C1.CH4Percent + " %";
                sheetT3C1.Cells[row++, col].Value = emissionsT3C1.N2OPercent + " %";
                sheetT3C1.Cells[row++, col].Value = emissionsT3C1.HFCSPercent + " %";
                sheetT3C1.Cells[row++, col].Value = emissionsT3C1.PFCSPercent + " %";
                sheetT3C1.Cells[row++, col].Value = emissionsT3C1.SF6Percent + " %";
                sheetT3C1.Cells[row++, col].Value = emissionsT3C1.NF3Percent + " %";
                sheetT3C1.Cells[row++, col].Value = emissionsT3C1.GreenHouseGasesPercent + " %";

                for (int i = 1; i <= 3; i++)
                {
                    sheetT3C1.Column(i).AutoFit();
                }
                #endregion

                #region T4C1
                //建立第一個Sheet，後方為定義Sheet的名稱
                ExcelWorksheet sheetT4C1 = ep.Workbook.Worksheets.Add("彙整表四、全廠溫室氣體範疇別及範疇一排放型式排放量統計表");

                col = 1;
                row = 1;
                sheetT4C1.Cells[row++, 1].Value = "範疇1";
                sheetT4C1.Cells[row++, 1].Value = "固定排放";
                sheetT4C1.Cells[row++, 1].Value = "製程排放";
                sheetT4C1.Cells[row++, 1].Value = "移動排放";
                sheetT4C1.Cells[row++, 1].Value = "逸散排放";
                sheetT4C1.Cells[row++, 1].Value = "範疇2 能源間接排放";
                sheetT4C1.Cells[row++, 1].Value = "範疇3 其他間接排放";
                sheetT4C1.Cells[row++, 1].Value = "總排放當量";

                col = 2;
                row = 1;
                sheetT4C1.Cells[row++, col].Value = emissionsT4C1.Category1;
                sheetT4C1.Cells[row++, col].Value = emissionsT4C1.Fixed;
                sheetT4C1.Cells[row++, col].Value = emissionsT4C1.Process;
                sheetT4C1.Cells[row++, col].Value = emissionsT4C1.Mobile;
                sheetT4C1.Cells[row++, col].Value = emissionsT4C1.Fugitive;
                sheetT4C1.Cells[row++, col].Value = emissionsT4C1.Category2Energy;
                sheetT4C1.Cells[row++, col].Value = emissionsT4C1.Category3Other;
                sheetT4C1.Cells[row++, col].Value = emissionsT4C1.Total;

                col = 3;
                row = 1;
                sheetT4C1.Cells[row++, col].Value = emissionsT4C1.Category1Percent + " %";
                sheetT4C1.Cells[row++, col].Value = emissionsT4C1.FixedPercent + " %";
                sheetT4C1.Cells[row++, col].Value = emissionsT4C1.ProcessPercent + " %";
                sheetT4C1.Cells[row++, col].Value = emissionsT4C1.MobilePercent + " %";
                sheetT4C1.Cells[row++, col].Value = emissionsT4C1.FugitivePercent + " %";
                sheetT4C1.Cells[row++, col].Value = emissionsT4C1.Category2EnergyPercent + " %";
                sheetT4C1.Cells[row++, col].Value = "-";
                sheetT4C1.Cells[row++, col].Value = emissionsT4C1.TotalPercent + " %";

                for (int i = 1; i <= 3; i++)
                {
                    sheetT4C1.Column(i).AutoFit();
                }
                #endregion

                #region T6
                //建立第一個Sheet，後方為定義Sheet的名稱
                ExcelWorksheet sheetT6 = ep.Workbook.Worksheets.Add("彙整表六、溫室氣體不確定性量化評估結果");

                col = 1;
                row = 1;
                sheetT6.Cells[row++, 1].Value = "進行不確定性評估之排放量絕對值加總";
                sheetT6.Cells[row++, 1].Value = "排放總量絕對值加總";
                sheetT6.Cells[row++, 1].Value = "進行不確定性評估之排放量佔總排放量之比例";
                sheetT6.Cells[row++, 1].Value = "本清冊之總不確定性 95%信賴區間下限";
                sheetT6.Cells[row++, 1].Value = "本清冊之總不確定性 95%信賴區間上限";

                col = 2;
                row = 1;
                sheetT6.Cells[row++, col].Value = emissionsT6.Uncertainty;
                sheetT6.Cells[row++, col].Value = emissionsT6.AbsoluteEmissions;
                sheetT6.Cells[row++, col].Value = emissionsT6.UncertaintyPercent + " %";
                sheetT6.Cells[row++, col].Value = emissionsT6.NintyfiveMin;
                sheetT6.Cells[row++, col].Value = emissionsT6.NintyfiveMax;
                for (int i = 1; i <= 2; i++)
                {
                    sheetT6.Column(i).AutoFit();
                }
                #endregion

                //因為ep.Stream是加密過的串流，故要透過SaveAs將資料寫到MemoryStream，在將MemoryStream使用FileStreamResult回傳到前端
                MemoryStream fileStream = new MemoryStream();
                ep.SaveAs(fileStream);
                ep.Dispose();//如果這邊不下Dispose，建議此ep要用using包起來，但是要記得先將資料寫進MemoryStream在Dispose。
                fileStream.Position = 0;//不重新將位置設為0，excel開啟後會出現錯誤

                return File(fileStream, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", emissionsInfo.EmissionDate.ToString("yyyy-MM-dd") + "_碳排放紀錄.xlsx");
            }
            catch (Exception e)
            {

                throw;
            }
        }
        public ActionResult ExportEmissionsList(DateTime start, DateTime end)
        {
            try
            {
                ExcelPackage.LicenseContext = LicenseContext.NonCommercial; // 關閉新許可模式通知
                                                                            //建立Excel
                ExcelPackage ep = new ExcelPackage();

                #region EmissionsList
                int colT2 = 1;
                int rowT2 = 1;
                ExcelWorksheet sheetT2 = ep.Workbook.Worksheets.Add("彙整表二、全廠七大溫室氣體排放量統計表");
                int colT3C1 = 1;
                int rowT3C1 = 1;
                ExcelWorksheet sheetT3C1 = ep.Workbook.Worksheets.Add("彙整表三、範疇一七大溫室氣體排放量統計表");
                int colT4C1 = 1;
                int rowT4C1 = 1;
                ExcelWorksheet sheetT4C1 = ep.Workbook.Worksheets.Add("彙整表四、全廠溫室氣體範疇別及範疇一排放型式排放量統計表");
                int colT6 = 1;
                int rowT6 = 1;
                ExcelWorksheet sheetT6 = ep.Workbook.Worksheets.Add("彙整表六、溫室氣體不確定性量化評估結果");

                //取出要匯出Excel的資料
                var query = emissionsRepository.Query(start, end);
                foreach (var item in query)
                {
                    var emissionsData = emissionsRepository.GetById(item.ID);

                    if (emissionsData != null)
                    {
                        var t2Query = emissionsT2Repository.GetByEmissionsId(item.ID);
                        if (t2Query != null)
                        {
                            sheetT2.Cells[rowT2, colT2++].Value = "紀錄時間";
                            sheetT2.Cells[rowT2, colT2++].Value = "CO2";
                            sheetT2.Cells[rowT2, colT2++].Value = "CH4";
                            sheetT2.Cells[rowT2, colT2++].Value = "N2O";
                            sheetT2.Cells[rowT2, colT2++].Value = "HFCs";
                            sheetT2.Cells[rowT2, colT2++].Value = "PFCs";
                            sheetT2.Cells[rowT2, colT2++].Value = "SF6";
                            sheetT2.Cells[rowT2, colT2++].Value = "NF3";
                            sheetT2.Cells[rowT2, colT2++].Value = "七種溫室氣體年總排放當量";
                            sheetT2.Cells[rowT2, colT2++].Value = "生質排放當量";

                            colT2 = 2;
                            sheetT2.Cells[rowT2, colT2++].Value = emissionsData.EmissionDate.ToString("yyyy/HH/mm HH:mm");
                            sheetT2.Cells[rowT2, colT2++].Value = t2Query.CO2Percent + "%";
                            sheetT2.Cells[rowT2, colT2++].Value = t2Query.CH4Percent + "%";
                            sheetT2.Cells[rowT2, colT2++].Value = t2Query.N2OPercent + "%";
                            sheetT2.Cells[rowT2, colT2++].Value = t2Query.HFCSPercent + "%";
                            sheetT2.Cells[rowT2, colT2++].Value = t2Query.PFCSPercent + "%";
                            sheetT2.Cells[rowT2, colT2++].Value = t2Query.SF6Percent + "%";
                            sheetT2.Cells[rowT2, colT2++].Value = t2Query.NF3Percent + "%";
                            sheetT2.Cells[rowT2, colT2++].Value = t2Query.GreenHouseGasesPercent + "%";
                            sheetT2.Cells[rowT2, colT2++].Value = "-";

                            rowT2++;
                            colT2 = 1;
                        }

                        var t3C1Query = emissionsT3C1Repository.GetByEmissionsId(item.ID);
                        if (t3C1Query != null)
                        {
                            sheetT3C1.Cells[rowT3C1, colT3C1++].Value = "紀錄時間";
                            sheetT3C1.Cells[rowT3C1, colT3C1++].Value = "CO2";
                            sheetT3C1.Cells[rowT3C1, colT3C1++].Value = "CH4";
                            sheetT3C1.Cells[rowT3C1, colT3C1++].Value = "N2O";
                            sheetT3C1.Cells[rowT3C1, colT3C1++].Value = "HFCs";
                            sheetT3C1.Cells[rowT3C1, colT3C1++].Value = "PFCs";
                            sheetT3C1.Cells[rowT3C1, colT3C1++].Value = "SF6";
                            sheetT3C1.Cells[rowT3C1, colT3C1++].Value = "NF3";
                            sheetT3C1.Cells[rowT3C1, colT3C1++].Value = "七種溫室氣體年總排放當量";

                            colT3C1 = 2;
                            sheetT3C1.Cells[rowT3C1, colT3C1++].Value = emissionsData.EmissionDate.ToString("yyyy/HH/mm HH:mm");
                            sheetT3C1.Cells[rowT3C1, colT3C1++].Value = t2Query.CO2Percent + "%";
                            sheetT3C1.Cells[rowT3C1, colT3C1++].Value = t2Query.CH4Percent + "%";
                            sheetT3C1.Cells[rowT3C1, colT3C1++].Value = t2Query.N2OPercent + "%";
                            sheetT3C1.Cells[rowT3C1, colT3C1++].Value = t2Query.HFCSPercent + "%";
                            sheetT3C1.Cells[rowT3C1, colT3C1++].Value = t2Query.PFCSPercent + "%";
                            sheetT3C1.Cells[rowT3C1, colT3C1++].Value = t2Query.SF6Percent + "%";
                            sheetT3C1.Cells[rowT3C1, colT3C1++].Value = t2Query.NF3Percent + "%";
                            sheetT3C1.Cells[rowT3C1, colT3C1++].Value = t2Query.GreenHouseGasesPercent + "%";

                            rowT3C1++;
                            colT3C1 = 1;
                        }

                        var t4C1Query = emissionsT4C1Repository.GetByEmissionsId(item.ID);
                        if (t4C1Query != null)
                        {
                            sheetT4C1.Cells[rowT4C1, colT4C1++].Value = "紀錄時間";
                            sheetT4C1.Cells[rowT4C1, colT4C1++].Value = "範疇1";
                            sheetT4C1.Cells[rowT4C1, colT4C1++].Value = "固定排放";
                            sheetT4C1.Cells[rowT4C1, colT4C1++].Value = "製程排放";
                            sheetT4C1.Cells[rowT4C1, colT4C1++].Value = "移動排放";
                            sheetT4C1.Cells[rowT4C1, colT4C1++].Value = "逸散排放";
                            sheetT4C1.Cells[rowT4C1, colT4C1++].Value = "範疇2 能源間接排放";
                            sheetT4C1.Cells[rowT4C1, colT4C1++].Value = "範疇3 其他間接排放";
                            sheetT4C1.Cells[rowT4C1, colT4C1++].Value = "總排放當量";

                            colT4C1 = 2;
                            sheetT4C1.Cells[rowT4C1, colT4C1++].Value = emissionsData.EmissionDate.ToString("yyyy/HH/mm HH:mm");
                            sheetT4C1.Cells[rowT4C1, colT4C1++].Value = t4C1Query.Category1Percent + "%";
                            sheetT4C1.Cells[rowT4C1, colT4C1++].Value = t4C1Query.FixedPercent + "%";
                            sheetT4C1.Cells[rowT4C1, colT4C1++].Value = t4C1Query.ProcessPercent + "%";
                            sheetT4C1.Cells[rowT4C1, colT4C1++].Value = t4C1Query.MobilePercent + "%";
                            sheetT4C1.Cells[rowT4C1, colT4C1++].Value = t4C1Query.FugitivePercent + "%";
                            sheetT4C1.Cells[rowT4C1, colT4C1++].Value = t4C1Query.Category2EnergyPercent + "%";
                            sheetT4C1.Cells[rowT4C1, colT4C1++].Value = "-";
                            sheetT4C1.Cells[rowT4C1, colT4C1++].Value = t4C1Query.Total + "%";

                            rowT4C1++;
                            colT4C1 = 1;
                        }

                        var t6Query = emissionsT6Repository.GetByEmissionsId(item.ID);
                        if (t6Query != null)
                        {
                            sheetT6.Cells[rowT6, colT6++].Value = "紀錄時間";
                            sheetT6.Cells[rowT6, colT6++].Value = "進行不確定性評估之排放量絕對值加總";
                            sheetT6.Cells[rowT6, colT6++].Value = "排放總量絕對值加總";
                            sheetT6.Cells[rowT6, colT6++].Value = "進行不確定性評估之排放量佔總排放量之比例";
                            sheetT6.Cells[rowT6, colT6++].Value = "本清冊之總不確定性 95%信賴區間下限";
                            sheetT6.Cells[rowT6, colT6++].Value = "本清冊之總不確定性 95%信賴區間上限";

                            colT6 = 2;
                            sheetT6.Cells[rowT6, colT6++].Value = emissionsData.EmissionDate.ToString("yyyy/HH/mm HH:mm");
                            sheetT6.Cells[rowT6, colT6++].Value = t6Query.Uncertainty;
                            sheetT6.Cells[rowT6, colT6++].Value = t6Query.AbsoluteEmissions;
                            sheetT6.Cells[rowT6, colT6++].Value = t6Query.UncertaintyPercent + "%";
                            sheetT6.Cells[rowT6, colT6++].Value = t6Query.NintyfiveMin;
                            sheetT6.Cells[rowT6, colT6++].Value = t6Query.NintyfiveMax;

                            rowT6++;
                            colT6 = 1;
                        }
                    }
                }

                //自動調整寬度
                for (int i = 1; i <= 3; i++)
                {
                    sheetT2.Column(i).AutoFit();
                }
                for (int i = 1; i <= 3; i++)
                {
                    sheetT3C1.Column(i).AutoFit();
                }
                for (int i = 1; i <= 3; i++)
                {
                    sheetT4C1.Column(i).AutoFit();
                }
                for (int i = 1; i <= 2; i++)
                {
                    sheetT6.Column(i).AutoFit();
                }
                #endregion

                //因為ep.Stream是加密過的串流，故要透過SaveAs將資料寫到MemoryStream，在將MemoryStream使用FileStreamResult回傳到前端
                MemoryStream fileStream = new MemoryStream();
                ep.SaveAs(fileStream);
                ep.Dispose();//如果這邊不下Dispose，建議此ep要用using包起來，但是要記得先將資料寫進MemoryStream在Dispose。
                fileStream.Position = 0;//不重新將位置設為0，excel開啟後會出現錯誤

                return File(fileStream, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", start.ToString("yyyy-MM-dd") + "~" + end.ToString("yyyy-MM-dd") + "_碳排放紀錄.xlsx");
            }
            catch (Exception e)
            {

                throw;
            }
        }
    }
}