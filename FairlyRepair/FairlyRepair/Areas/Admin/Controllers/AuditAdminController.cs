﻿using AutoMapper;
using FairlyRepair.ActionFilters;
using FairlyRepair.Areas.Admin.ViewModels.Audit;
using FairlyRepair.Models;
using FairlyRepair.Models.Others;
using FairlyRepair.Repositories;
using FairlyRepair.Utility.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FairlyRepair.Areas.Admin.Controllers
{
    [Authorize]
    //[ErrorHandleAdminActionFilter]
    public class AuditAdminController : BaseAdminController
    {
        // GET: Admin/AuditAdmin
        private AdminRepository adminRepository = new AdminRepository(new FairlyRepairDBEntities());
        private AuditRepository auditRepository = new AuditRepository(new FairlyRepairDBEntities());
        private Repositories.MeetRepositories.AuditLevelRepository auditLevelRepository = new Repositories.MeetRepositories.AuditLevelRepository(new MeetModels.MeetingSignInDBEntities());
        private EightDRepository eightDRepository = new EightDRepository(new FairlyRepairDBEntities());
        private Repositories.T8Repositories.PersonRepository t8personRepository = new Repositories.T8Repositories.PersonRepository(new T8ERPEntities());

        // GET: Admin/AuditAdmin
        public ActionResult Index(AuditIndexView model)
        {
            var adminInfo = AdminInfoHelper.GetAdminInfo();
            switch ((MeetAdminType)adminInfo.AccountType)
            {
                case MeetAdminType.Employee:
                    return RedirectToAction("Index", "EightDAdmin");              
            }
            
            //var personInfo = t8personRepository.FindByJobID(adminRepository.GetById(adminInfo.ID).Account);
            //var personDept = personInfo == null ? "" : personInfo.DeptID;
            string officerId = model.State == null ? "" : adminInfo.Account;
            
            var query = auditRepository.Query(model.IsInvalid, model.ReportID, model.Type, model.State, model.Title, model.PersonID, officerId);
            if (!model.IsOfficer)
            {
                query = query.Where(q => q.Officer != adminInfo.Account);
            }

            List<string> auditDepts = new List<string>();
            if (adminInfo.AccountType == (int)MeetAdminType.Manager)
            {//主管審核
                auditDepts = auditLevelRepository.Query(true, "", "", adminInfo.Account).Select(q => q.DeptId).ToList();
            }
            else if (adminInfo.AccountType == (int)MeetAdminType.Vice)
            {//副總審核
                auditDepts = auditLevelRepository.Query(true, "", adminInfo.Account, "").Select(q => q.DeptId).ToList();
            }
            else if (adminInfo.AccountType == (int)MeetAdminType.President)
            {//總經理審核
                auditDepts = auditLevelRepository.Query(true, adminInfo.Account, "", "").Select(q => q.DeptId).ToList();
            }
            var sameDeptPersonIdList = t8personRepository.GetPersonQuery("", "").Where(q => auditDepts.Contains(q.DeptID)).Select(q => q.JobID).ToList();
            var adminAccList = adminRepository.GetAll().Where(q => sameDeptPersonIdList.Contains(q.Account)).Select(q => q.Account).ToList();
            query = query.Where(q => sameDeptPersonIdList.Contains(q.PersonID) || adminAccList.Contains(q.Officer));

            var pageResult = query.ToPageResult<Audit>(model);
            model.PageResult = Mapper.Map<PageResult<AuditItemView>>(pageResult);

            Repositories.MeetRepositories.AdminRepository meetAdminRepository = new Repositories.MeetRepositories.AdminRepository(new MeetModels.MeetingSignInDBEntities());
            foreach (var item in model.PageResult.Data)
            {
                item.Title = "";
                try
                {
                    var officer = meetAdminRepository.GetByAccount(item.Officer);
                    string officerStr = officer == null || string.IsNullOrEmpty(officer.Name) ? officer.Account : officer.Name;
                    item.OfficerName = " 【填表人 : " + officerStr + "】";

                    var reportData = eightDRepository.GetById(item.ReportID);
                    if (reportData != null)
                    {
                        item.Title = reportData.Title;
                    }

                }
                catch (Exception ex)
                {
                    item.Title = "找不到標題";
                    item.OfficerName = "";
                }
                //if (item.Type > (int)AuditType.Internal)
                //{
                //    var personInfo = t8personRepository.FindByJobID(item.PersonID);
                //    if (personInfo != null)
                //    {
                //        item.PersonName = "【" + personInfo.DepartmentStr + "】";
                //        item.PersonName += personInfo.Name;
                //    }
                //}
                //else
                //{
                //    var officerAdminInfo = adminRepository.GetById(item.Officer);
                //    var officerPersonInfo = t8personRepository.FindByJobID(officerAdminInfo.Account);
                //    item.OfficerName = officerPersonInfo == null ? officerAdminInfo.Name : officerPersonInfo.Name;
                //}
            }

            return View(model);
        }
        
        //public ActionResult Add(int id, int type)
        //{
        //    if (id == 0 || type == 0)
        //    {
        //        return RedirectToAction("Index", "AuditIndex");
        //    }
        //    //新增待審資料
        //    var data = new Audit() {
        //        TargetID = id,
        //        Type = type,
        //        State = (int)AuditState.OfficerAudit,
        //        IsInvalid = false,
        //        Officer = "",
        //        CreateDate= DateTime.Now
        //    };
        //    auditRepository.Insert(data);

        //    return RedirectToAction("Index", "AuditIndex");
        //}
    }
}