﻿using AutoMapper;
using FairlyRepair.ActionFilters;
using FairlyRepair.Areas.Admin.ViewModels.EightD;
using FairlyRepair.Models;
using FairlyRepair.MeetModels;
using FairlyRepair.Models.Others;
using FairlyRepair.Repositories;
using FairlyRepair.Utility.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.IO;
using FairlyRepair.Utility;
using System.Text;

namespace FairlyRepair.Areas.Admin.Controllers
{
    [Authorize]
    //[ErrorHandleAdminActionFilter]
    public class EightDAdminController : BaseAdminController
    {
        private EightDRepository eightDRepository = new EightDRepository(new FairlyRepairDBEntities());
        private EightDFileRepository eightDFileRepository = new EightDFileRepository(new FairlyRepairDBEntities());
        private EightDCategoryRepository eightDCategoryRepository = new EightDCategoryRepository(new FairlyRepairDBEntities());
        private AuditRepository auditRepository = new AuditRepository(new FairlyRepairDBEntities());      
        private AdminRepository adminRepository = new AdminRepository(new FairlyRepairDBEntities());      
        private Repositories.MeetRepositories.AuditLevelRepository auditLevelRepository = new Repositories.MeetRepositories.AuditLevelRepository(new MeetingSignInDBEntities());
        private Repositories.T8Repositories.PersonRepository t8personRepository = new Repositories.T8Repositories.PersonRepository(new T8ERPEntities());

        public int AuditProcessing = (int)FormAudit.Processing;
        public int AuditApproval = (int)FormAudit.Approval;
        public int AuditReject = (int)FormAudit.Reject;

        // GET: Admin/EightDAdmin

        public ActionResult OverView()
        {
            Repositories.T8Repositories.DepartmentRepository t8departmentRepository = new Repositories.T8Repositories.DepartmentRepository(new T8ERPEntities());
            OverViewView model = new OverViewView();
            model.StartDate = Convert.ToDateTime("2020/01/01");
            model.EndDate = DateTime.Now;

            return View(model);
        }

        public ActionResult Index(EightDIndexView model)
        {
            var adminInfo = AdminInfoHelper.GetAdminInfo();
            model.Category = 0;
            if (!string.IsNullOrEmpty(model.CategoryStr))
            {
                var cateData = eightDCategoryRepository.FindByTitle(model.CategoryStr);
                model.Category = cateData == null ? 0 : cateData.ID;
            }
            var query = eightDRepository.Query(model.IsFinished, model.Title, model.Category, model.Customer, model.Depts, false);
            var pageResult = query.ToPageResult<Models.EightD>(model);
            model.PageResult = Mapper.Map<PageResult<EightDView>>(pageResult);

            foreach (var item in model.PageResult.Data)
            {
                #region 審核狀態
                var auditInfo = auditRepository.Query(null, item.ID, 0, null, "", "").Where(q => q.Type == (int)AuditType.EightD).OrderByDescending(q => q.ID).FirstOrDefault();
                 if (auditInfo == null)
                {
                    item.AuditStatus = "已填寫未送審";
                    item.IsInvalid = false;
                }
                else
                {
                    item.AuditState = auditInfo == null ? 0 : auditInfo.State;
                    item.AuditStatus = EnumHelper.GetDescription((AuditState)item.AuditState);
                    item.IsInvalid = auditInfo == null ? false : auditInfo.IsInvalid;
                }
                #endregion
            }

            return View(model);
        }

        public ActionResult Edit(int id = 0, bool isNew = false, bool FromAudit = false)
        {
            var adminInfo = AdminInfoHelper.GetAdminInfo();

            EightDView model = new EightDView();
            if (id == 0)
            {
                model = new EightDView();
                model.CreaterId = adminInfo.Account;
            }
            else
            {
                var query = eightDRepository.GetById(id);
                model = Mapper.Map<EightDView>(query);
                model.FilesList = eightDFileRepository.Query(id).ToList();

                if (model == null)
                {
                    ShowMessage(false, "找不到資料");
                    return RedirectToAction("Index");
                }

                if (isNew && model.IsAudit)
                {//取消送審/重新編輯
                    var newData = Mapper.Map<EightDView>(query);
                    query.IsRevoke = true;
                    eightDRepository.Update(query);
                    var oldAudit = auditRepository.Query(null, query.ID, (int)AuditType.EightD, null, "", "").FirstOrDefault();
                    if (oldAudit != null)//複製並更新舊的審核紀錄
                    {
                        //撤銷舊資料
                        oldAudit.IsInvalid = true;
                        auditRepository.Update(oldAudit);

                        //重置審核資料
                        newData.AuditInfo = model.AuditInfo = auditRepository.FindByTarget(query.ID, (int)AuditType.EightD);
                    }
                    EightD data = Mapper.Map<EightD>(newData);
                    data.IsAudit = false;
                    data.OfficerAudit = AuditProcessing;
                    data.OfficerRemark = "";
                    data.OfficerSignature = "";
                    data.OfficerAuditDate = null;
                    data.ManagerAudit = AuditProcessing;
                    data.ManagerRemark = "";
                    data.ManagerSignature = "";
                    data.ManagerAuditDate = null;
                    data.ViceAudit = AuditProcessing;
                    data.ViceRemark = "";
                    data.ViceSignature = "";
                    data.ViceAuditDate = null;
                    data.PresidentAudit = AuditProcessing;
                    data.PresidentRemark = "";
                    data.PresidentSignature = "";
                    data.PresidentAuditDate = null;
                    newData.ID = eightDRepository.Insert(data);
                    newData = Mapper.Map<EightDView>(eightDRepository.GetById(newData.ID));

                    SetEightDViewData(FromAudit, newData);

                    return RedirectToAction("Edit", new { id = newData.ID, FromAudit = FromAudit});
                }
            }
            SetEightDViewData(FromAudit, model);

            return View(model);
        }

        private void SetEightDViewData(bool FromAudit, EightDView model)
        {
            model.FromAudit = FromAudit;

            var officerPersonInfo = t8personRepository.FindByJobID(model.CreaterId);
            if (officerPersonInfo != null)
            {
                model.OfficerDeptId = officerPersonInfo.DeptID;
                model.OfficerDeptName = officerPersonInfo.DepartmentStr;
                model.OfficerName = officerPersonInfo.Name;

                var auditLevelData = auditLevelRepository.FindByDept(officerPersonInfo.DeptID);
                model.AuditLevelInfo = auditLevelData;
                //主管
                if (model.ManagerAudit > 1)
                {
                    model.ManagerSignature = auditLevelData.ManagerReview + ".jpg";
                    var personInfo = t8personRepository.FindByJobID(auditLevelData.ManagerReview);
                    model.ManagerDeptId = personInfo.DeptId;
                    model.ManagerDeptName = personInfo.DepartmentStr;
                    model.ManagerName = personInfo.Name;
                }
                //副總
                if (model.ViceAudit > 1)
                {
                    model.ViceSignature = auditLevelData.ViceReview + ".jpg";
                    var personInfo = t8personRepository.FindByJobID(auditLevelData.ViceReview);
                    model.ViceDeptId = personInfo.DeptId;
                    model.ViceDeptName = personInfo.DepartmentStr;
                    model.ViceName = personInfo.Name;
                }
                //總經理
                if (model.PresidentAudit > 1)
                {
                    model.PresidentSignature = auditLevelData.PresidentReview + ".jpg";
                    var personInfo = t8personRepository.FindByJobID(auditLevelData.PresidentReview);
                    model.PresidentDeptId = personInfo.DeptId;
                    model.PresidentDeptName = personInfo.DepartmentStr;
                    model.PresidentName = personInfo.Name;
                }
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public ActionResult Edit(EightDView model)
        {
            if (model.FinishDate < Convert.ToDateTime("2000/01/01"));
            {
                model.FinishDate = null;
            }
            if (ModelState.IsValid)
            {
                try
                {
                    var adminInfo = AdminInfoHelper.GetAdminInfo();

                    //#region 檔案處理
                    //bool hasFile = ImageHelper.CheckFileExists(model.ReportFilePost);
                    //if (hasFile)
                    //{//檔案
                    //    ImageHelper.DeleteFile(PhotoFolder, model.ReportFile);
                    //    model.ReportFile = ImageHelper.SaveFile(model.ReportFilePost, PhotoFolder, model.ReportFilePost.FileName.Split('.')[0] + "_" + DateTime.Now.ToString("yyyyMMddHHmm"));
                    //}
                    //#endregion

                    EightD data = Mapper.Map<EightD>(model);

                    if (model.IsAudit)
                    {
                        data.OfficerAudit = (int)FormAudit.Approval;
                    }

                    #region 日期處理
                    data.OfficerAuditDate = SetAuditDate(data.OfficerAudit, data.OfficerAuditDate);
                    data.ManagerAuditDate = SetAuditDate(data.ManagerAudit, data.ManagerAuditDate);
                    data.ViceAuditDate = SetAuditDate(data.ViceAudit, data.ViceAuditDate);
                    data.PresidentAuditDate = SetAuditDate(data.PresidentAudit, data.PresidentAuditDate);
                    #endregion
                    if (data.FinishDate < Convert.ToDateTime("2000/01/01"))
                    {
                        data.FinishDate = null;
                    }

                    if (model.ID == 0)
                    {
                        //F8D{客戶編號}{類別}{單號+1}
                        string serialNumber = $"F8D{model.Customer}{model.Category}{DateTime.Now.ToString("yyyyMMddHHmmss")}";
                        data.SerialNumber = serialNumber;

                        data.IsAudit = false;
                        data.OfficerAudit = AuditProcessing;
                        data.OfficerRemark = "";
                        data.OfficerSignature = "";
                        data.OfficerAuditDate = null;
                        data.ManagerAudit = AuditProcessing;
                        data.ManagerRemark = "";
                        data.ManagerSignature = "";
                        data.ManagerAuditDate = null;
                        data.ViceAudit = AuditProcessing;
                        data.ViceRemark = "";
                        data.ViceSignature = "";
                        data.ViceAuditDate = null;
                        data.PresidentAudit = AuditProcessing;
                        data.PresidentRemark = "";
                        data.PresidentSignature = "";
                        data.PresidentAuditDate = null;

                        data.CreateDate = DateTime.Now;
                        data.CreaterId = adminInfo.Account;
                        model.ID = eightDRepository.Insert(data);
                    }
                    else
                    {
                        if (adminInfo.Account != model.CreaterId && adminInfo.AccountType > (int)MeetAdminType.Manager)
                        {
                            ShowMessage(false, "儲存失敗，無修改權限");
                            return RedirectToAction("External", new { id = model.ID, FromAudit = model.FromAudit });
                        }
                        eightDRepository.Update(data);
                    }

                    #region 檔案處理
                    if (model.FilesFiles != null)
                    {
                        foreach (HttpPostedFileBase pic in model.FilesFiles)
                        {
                            bool hasFile = ImageHelper.CheckFileExists(pic);
                            if (hasFile)
                            {
                                EightDFile file = new EightDFile();
                                //ImageHelper.DeleteFile(PhotoFolder, model.FileOne);
                                file.EightDID = model.ID;
                                file.Files = ImageHelper.SaveFile(pic, PhotoFolder);
                                eightDFileRepository.Insert(file);
                            }
                        }
                    }
                    #endregion

                    #region 送審
                    if (model.IsAudit)
                    {
                        #region 簽核人員
                        AuditPerson(adminInfo, data, model.ID);
                        eightDRepository.Update(data);
                        #endregion

                        var auditInfo = auditRepository.Query(null, model.ID, 1).FirstOrDefault();

                        #region 簽核狀態
                        int state = HandleState(model.CreaterDeptId, data.OfficerAudit, data.ManagerAudit, data.ViceAudit, data.PresidentAudit);
                        #endregion

                        if (data.OfficerAudit == AuditReject || data.ManagerAudit == AuditReject || data.ViceAudit == AuditReject || data.PresidentAudit == AuditReject)
                        {
                            auditInfo.IsInvalid = true;
                        }
                        if (auditInfo == null)
                        {
                            var auditId = auditRepository.Insert(new Audit()
                            {
                                ReportID = data.ID,
                                PersonID = "",
                                TargetID = data.ID,
                                Type = 1,
                                State = state,
                                IsInvalid = false,
                                Officer = model.CreaterId,
                                CreateDate = DateTime.Now
                            });
                        }
                        else
                        {
                            auditInfo.State = state;
                            auditRepository.Update(auditInfo);
                        }
                    }
                    #endregion
                }
                catch (Exception e)
                {
                    ShowMessage(false, e.Message);
                }
                ShowMessage(true, "儲存成功");
                return RedirectToAction(nameof(EightDAdminController.Edit), new { id = model.ID, FromAudit = model.FromAudit });
            }
            else
            {
                var msg = ModelState.Values.Where(q => q.Errors.Count() > 0).FirstOrDefault().Errors[0].ErrorMessage;
                ShowMessage(false, msg);
            }

            ShowMessage(false, "輸入資料有誤");
            return View(model);
        }

        private void AuditPerson(AdminInfo adminInfo, EightD data, int id)
        {
            int accountType = GetMainAccountInfo(adminInfo.Account).Type;
            if (accountType < (int)MeetAdminType.Employee)
            {
                var targetInfo = eightDRepository.GetById(id);
                if (targetInfo.CreaterId == adminInfo.Account)
                {
                    switch (adminInfo.Type)
                    {
                        case (int)MeetAdminType.Sysadmin:
                            data.ManagerAudit = data.OfficerAudit;
                            data.ManagerAuditDate = data.OfficerAuditDate;
                            data.ManagerRemark = data.OfficerRemark;
                            data.ManagerSignature = data.OfficerSignature;
                            break;
                        case (int)MeetAdminType.Manager:
                            data.ManagerAudit = data.OfficerAudit;
                            data.ManagerAuditDate = data.OfficerAuditDate;
                            data.ManagerRemark = data.OfficerRemark;
                            data.ManagerSignature = data.OfficerSignature;
                            break;
                        case (int)MeetAdminType.Vice:
                            data.ViceAudit = data.OfficerAudit;
                            data.ViceAuditDate = data.OfficerAuditDate;
                            data.ViceRemark = data.OfficerRemark;
                            data.ViceSignature = data.OfficerSignature;
                            break;
                        case (int)MeetAdminType.President:
                            data.PresidentAudit = data.OfficerAudit;
                            data.PresidentAuditDate = data.OfficerAuditDate;
                            data.PresidentRemark = data.OfficerRemark;
                            data.PresidentSignature = data.OfficerSignature;
                            break;
                    }
                }
            }
        }

        public ActionResult Delete(int id)
        {
            //EightD data = eightDRepository.GetById(id);
            var fileList = eightDFileRepository.Query(id);
            foreach (var item in fileList)
            {
                eightDFileRepository.Delete(item.ID);
            }
            eightDRepository.Delete(id);
            ShowMessage(true, "Data deleted.");
            return RedirectToAction(nameof(EightDAdminController.Index));
        }

        public ActionResult DeleteFile(int id)
        {
            eightDFileRepository.Delete(id);
            ShowMessage(true, "刪除成功");
            return RedirectToAction("Index");
        }
    }
}