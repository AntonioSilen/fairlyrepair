﻿using AutoMapper;
using FairlyRepair.ActionFilters;
using FairlyRepair.Areas.Admin.ViewModels.EightDCategory;
using FairlyRepair.Areas.Admin.ViewModels.Tags;
using FairlyRepair.Models;
using FairlyRepair.Models.Others;
using FairlyRepair.Repositories;
using FairlyRepair.Utility.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FairlyRepair.Areas.Admin.Controllers
{
    [Authorize]
    [ErrorHandleAdminActionFilter]
    public class EightDCategoryAdminController : BaseAdminController
    {
        private EightDCategoryRepository eightDCategoryRepository;

        public EightDCategoryAdminController() : this(null) { }

        public EightDCategoryAdminController(EightDCategoryRepository repo)
        {
            eightDCategoryRepository = repo ?? new EightDCategoryRepository(new FairlyRepairDBEntities());
        }

        // GET: Admin/EightDCategoryAdmin
        public ActionResult Index(EightDCategoryIndexView model)
        {
            var adminInfo = AdminInfoHelper.GetAdminInfo();

            var query = eightDCategoryRepository.Query(model.IsOnline, model.Title, model.Type);
            var pageResult = query.ToPageResult<Models.EightDCategory>(model);
            model.PageResult = Mapper.Map<PageResult<EightDCategoryView>>(pageResult);
            return View(model);
        }

        public ActionResult Edit(int id = 0)
        {
            var adminInfo = AdminInfoHelper.GetAdminInfo();

            EightDCategoryView model = new EightDCategoryView();
            if (id == 0)
            {
                model = new EightDCategoryView();
            }
            else
            {
                var query = eightDCategoryRepository.GetById(id);
                model = Mapper.Map<EightDCategoryView>(query);
            }
            model.Type = 1;
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public ActionResult Edit(EightDCategoryView model)
        {
            if (ModelState.IsValid)
            {
                var adminInfo = AdminInfoHelper.GetAdminInfo();
                EightDCategory data = Mapper.Map<EightDCategory>(model);
                if (model.ID == 0)
                {
                    model.ID = eightDCategoryRepository.Insert(data);                    
                }
                else
                {
                    eightDCategoryRepository.Update(data);
                }
                ShowMessage(true, "");
                return RedirectToAction(nameof(EightDCategoryAdminController.Edit), new { id = model.ID });
            }

            ShowMessage(false, "輸入資料有誤");
            return View(model);
        }

        public ActionResult Delete(int id)
        {
            EightDCategory data = eightDCategoryRepository.GetById(id);
            eightDCategoryRepository.Delete(data.ID);
            ShowMessage(true, "Data deleted.");
            return RedirectToAction(nameof(EightDCategoryAdminController.Index));
        }

        public JsonResult GetCategory(string key)
        {
            var result = eightDCategoryRepository.GetAll().Where(t => t.Title.Contains(key));
            var tagResult = new List<AutoCompleteView>();
            foreach (var item in result)
            {
                var tag = new AutoCompleteView();
                tag.label = item.Title;
                tag.value = item.ID.ToString();
                tagResult.Add(tag);
            }

            //result格式未處理，須為 {"XXX":[{"label":"A", "value":1},{"label":"B","value":2},{"label":"C","value":3}]} 

            return Json(tagResult);
        }
    }
}