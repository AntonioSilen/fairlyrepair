﻿using AutoMapper;
using FairlyRepair.Models.Others;
using FairlyRepair.Repositories.T8Repositories;
using FairlyRepair.Models;
using FairlyRepair.Repositories;
using FairlyRepair.Repositories.T8Repositories;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FairlyRepair.Utility.Helpers;
using FairlyRepair.Areas.Admin.ViewModels.RepairRecord;
using FairlyRepair.ActionFilters;
using System.IO;

namespace FairlyRepair.Areas.Admin.Controllers
{
    [Authorize]
    [ErrorHandleAdminActionFilter]
    public class RepairRecordAdminController : BaseAdminController
    {
        private AdminRepository adminRepository = new AdminRepository(new FairlyRepairDBEntities());
        private PersonRepository personRepository = new PersonRepository(new T8ERPEntities());
        private RepairRecordRepository repairRecordRepository = new RepairRecordRepository(new FairlyRepairDBEntities());
        private RepairRepository repairRepository = new RepairRepository(new FairlyRepairDBEntities());
        private RepairImageRepository repairImageRepository = new RepairImageRepository(new FairlyRepairDBEntities());
        private RepairRecordImageRepository repairRecordImageRepository = new RepairRecordImageRepository(new FairlyRepairDBEntities());
        private RepairRecordReceiptRepository repairRecordReceiptRepository = new RepairRecordReceiptRepository(new FairlyRepairDBEntities());
        private RepairRecordFeeRepository repairRecordFeeRepository = new RepairRecordFeeRepository(new FairlyRepairDBEntities());
        private TagsRepository tagsRepository = new TagsRepository(new FairlyRepairDBEntities());
        private TagsRelationRepository tagsRelationRepository = new TagsRelationRepository(new FairlyRepairDBEntities());
        public GroupPersonRepository groupPersonRepository = new GroupPersonRepository(new T8ERPEntities());
        private InterlockRepository interlockRepository = new InterlockRepository(new FairlyRepairDBEntities());

        public ActionResult Index(RepairRecordIndexView model)
        {
            var repairQuery = repairRepository.FindBy(model.RepairID);
            if (repairQuery == null)
            {
                return RedirectToAction("Index", "HomeAdmin");
            }
            var query = repairRecordRepository.Query(model.IsSolved, model.RepairID);
            var pageResult = query.ToPageResult<RepairRecord>(model);
            model.PageResult = Mapper.Map<PageResult<RepairRecordView>>(pageResult);            

            model.RepairInfo = Mapper.Map<ViewModels.Repair.RepairView>(repairQuery);
            model.RepairInfo.PicList = repairImageRepository.Query(model.RepairID).ToList();

            return View(model);
        }

        public ActionResult Edit(int id = 0, int RepairID = 0)
        {
            if (id == 0 && RepairID == 0)
            {
                return RedirectToAction("Index", "HomeAdmin");
            }           
            var model = new RepairRecordView();
            if (id != 0)
            {
                var query = repairRecordRepository.FindBy(id);
                model = Mapper.Map<RepairRecordView>(query);
                model.PicList = repairRecordImageRepository.Query(id).ToList();
                model.FeeList = Mapper.Map<List<RepairRecordFeeView>>(repairRecordFeeRepository.Query(id));
                foreach (var item in model.FeeList)
                {
                    item.Tags = TagsRelationHelper.GetTagRelationStr(item.ID, 1);
                }
                var repairData = repairRepository.GetById(model.RepairID);
                model.IsFinish = repairData.IsFinish;
            }
            else
            {
                model.RepairID = RepairID;
                model.CreateDate = DateTime.Now;
                model.IsSolved = false;
            }

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public ActionResult Edit(RepairRecordView model, List<HttpPostedFileBase> pics)
        {
            if (ModelState.IsValid)
            {
                Repair repairData = repairRepository.GetById(model.RepairID);
                if (repairData == null)
                {
                    return RedirectToAction("Index", "RepairAdmin");
                }
                RepairRecord data = Mapper.Map<RepairRecord>(model);
                var adminInfo = AdminInfoHelper.GetAdminInfo();

                if (model.ID == 0)
                {
                    data.Creater = adminInfo.ID;
                    model.ID = repairRecordRepository.Insert(data);
                    SendLineNotify(model.ID, (int)AdminType.Mainter);//維修員
                    SendLineNotify(model.ID, (int)AdminType.Dispatcher);//調度員

                    try
                    {
                        string personId = adminRepository.GetById(model.Creater).Account;
                        //SendLineNotify(model.ID, (int)AdminType.Applicant, $"{Request.Url.Scheme}://{Request.Url.Authority}/Repair/Record?RepairID={model.ID}", personId);//申請人
                        SendLineNotify(model.ID, 0, $"{Request.Url.Scheme}://{Request.Url.Authority}/Repair/Record?RepairID={model.ID}", personId);//申請人
                        ShowMessage(true, "新增成功");
                    }
                    catch (Exception e)
                    {
                        ShowMessage(false, "新增失敗");
                    }                    
                }
                else
                {
                    try
                    {
                        if (adminInfo.ID != repairRecordRepository.FindBy(model.ID).Creater && adminInfo.Type > (int)AdminType.Dispatcher)
                        {
                            ShowMessage(false, "儲存失敗，無修改權限");
                            return View(model);
                        }
                        repairRecordRepository.Update(data);
                        ShowMessage(true, "修改成功");
                    }
                    catch (Exception e)
                    {
                        ShowMessage(false, "修改失敗");
                    }                  
                }

                if (!repairData.IsFinish && model.IsSolved && adminInfo.Type != (int)AdminType.Dispatcher)
                {
                    //通知驗收人員
                    SendFinishCheckLineNotify(model.ID, (int)AdminType.Dispatcher);//調度員
                }

                #region 檔案處理
                if (model.ReceiptPicsFiles != null)
                {
                    foreach (HttpPostedFileBase pic in model.ReceiptPicsFiles)
                    {
                        bool hasFile = ImageHelper.CheckFileExists(pic);
                        if (hasFile)
                        {
                            RepairRecordReceipt receiptData = new RepairRecordReceipt();
                            string FileUploads = System.Web.Configuration.WebConfigurationManager.AppSettings["FileUploads"];
                            string fileUploads = Server.MapPath(FileUploads);
                            //ImageHelper.DeleteFile(PhotoFolder, model.FileOne);
                            receiptData.RecordID = model.ID;
                            receiptData.Pics = ImageHelper.SaveFile(pic, Path.Combine(fileUploads, "RecordFeePhoto"));
                            repairRecordReceiptRepository.Insert(receiptData);
                        }
                    }
                }
                if (model.PicsFiles != null)
                {
                    foreach (HttpPostedFileBase pic in model.PicsFiles)
                    {
                        bool hasFile = ImageHelper.CheckFileExists(pic);
                        if (hasFile)
                        {
                            RepairRecordImage imageData = new RepairRecordImage();
                            //ImageHelper.DeleteFile(PhotoFolder, model.FileOne);
                            imageData.RecordID = model.ID;
                            imageData.Pics = ImageHelper.SaveFile(pic, PhotoFolder);
                            repairRecordImageRepository.Insert(imageData);
                        }
                    }
                }
                #endregion

                #region 費用
                int totalFee = 0;
                if (!string.IsNullOrEmpty(model.JsonFee) && model.JsonFee != "[]")
                {
                    var jsonFee = Newtonsoft.Json.JsonConvert.DeserializeObject<List<RepairRecordFeeView>>(model.JsonFee);
                    if (model.RepairID != 0 && model.ID != 0)
                    {
                        var existList = repairRecordFeeRepository.Query(model.ID).ToList();
                        foreach (var item in existList)
                        {
                            repairRecordFeeRepository.Delete(item.ID);
                            var feeTagList = tagsRelationRepository.Query(item.ID, 1).ToList();
                            foreach (var tag in feeTagList)
                            {
                                tagsRelationRepository.Delete(tag.ID);
                            }
                        }

                        foreach (var item in jsonFee)
                        {
                            totalFee += item.Fee;
                            int Id = repairRecordFeeRepository.Insert(new RepairRecordFee()
                            {
                                RecordID = model.ID,
                                Title = item.Title,
                                Description = item.Description,
                                Fee = item.Fee,
                            });
                            if (!string.IsNullOrEmpty(item.Tags) && Id != 0)
                            {
                                TagsRelationHelper.AddTagRelation(Id, item.Tags, 1);
                            }                            
                        }
                    }
                }
                //var query = repairRecordRepository.FindBy(model.ID);
                data.Fee = totalFee;
                repairRecordRepository.Update(data);
                #endregion

                return RedirectToAction("Edit", new { id = model.ID });
            }

            return View(model);
        }

        public ActionResult Delete(int id)
        {
            var fileList = repairRecordImageRepository.Query(id);
            foreach (var item in fileList)
            {
                repairRecordImageRepository.Delete(item.ID);
            }
            repairRecordRepository.Delete(id);
            ShowMessage(true, "刪除成功");
            return RedirectToAction("Index");
        }
        public ActionResult DeleteReceiptPic(int id)
        {
            repairRecordReceiptRepository.Delete(id);
            ShowMessage(true, "刪除成功");
            return RedirectToAction("Index");
        }
        public ActionResult DeletePic(int id)
        {
            repairRecordImageRepository.Delete(id);
            ShowMessage(true, "刪除成功");
            return RedirectToAction("Index");
        }

        public string SendLineNotify(int repairId, int adminType, string urlMain = "", string personId = "")
        {
            try
            {
                var repairInfo = Mapper.Map<Areas.Admin.ViewModels.Repair.RepairView>(repairRepository.GetById(repairId));              
                var query = adminRepository.Query(true, adminType);
                if (!string.IsNullOrEmpty(personId))
                {
                    query = adminRepository.Query(true, 0, personId);
                }

                var jidList = query.Select(q => q.Account).ToList();
                //jidList.Add("2416");
                //jidList.Add("2006");
                //jidList.Add("2192");
                var notifyList = interlockRepository.Query().Where(q => jidList.Contains(q.JobID));

                string Message = "";
                string notifyTitle = "【菲力工業報修更新】";
                urlMain = string.IsNullOrEmpty(urlMain) ? $"{Request.Url.Scheme}://{Request.Url.Authority}/Admin/RepairAdmin/Edit/{repairInfo.ID}" : urlMain;

                RepairAdminController repairController = new RepairAdminController();
                string repairInfoStr = repairController.ComposeRepairInfoStr(repairInfo);
                Message = $" \r\n{notifyTitle}處理紀錄更新 \r\n \r\n";
                Message += repairInfoStr;
                //Message += $" \r\n 課別 : {repairInfo.DepartmentTitle} \r\n \r\n";
                //Message += $" \r\n 廠區 : {repairInfo.FactoryTitle} \r\n \r\n";
                //Message += $" \r\n 樓層 : {repairInfo.FloorTitle} \r\n \r\n";
                //Message += $" \r\n 區域 : {repairInfo.AreaTitle} \r\n \r\n";
                //Message += $" \r\n 設備 : {repairInfo.DeviceName} \r\n \r\n";
                //Message += $" \r\n 故障現況 : {repairInfo.FaultStateOptions.Where(q => q.Value == repairInfo.FaultState.ToString()).FirstOrDefault().Text} \r\n \r\n";
                //Message += $" \r\n 申請時間 : {repairInfo.CreateDate} \r\n \r\n";
                Message += $" 查看 : {urlMain} \r\n \r\n";

                if (!string.IsNullOrEmpty(Message))
                {
                    foreach (var item in notifyList)
                    {
                        repairController.SetNotify(Message, "", "", item.ID, item.TokenKey);
                    }
                    return "successed";
                }
                return "failed : Repair information not found.";
            }
            catch (Exception ex)
            {
                return "failed : " + ex.Message;
            }
        }

        public string SendFinishCheckLineNotify(int repairId, int adminType, string urlMain = "", string personId = "")
        {
            try
            {
                var repairInfo = Mapper.Map<Areas.Admin.ViewModels.Repair.RepairView>(repairRepository.GetById(repairId));              
                var query = adminRepository.Query(true, adminType);
                if (!string.IsNullOrEmpty(personId))
                {
                    query = adminRepository.Query(true, 0, personId);
                }

                var jidList = query.Select(q => q.Account).ToList();
                //jidList.Add("2416");
                //jidList.Add("2006");
                //jidList.Add("2192");
                var notifyList = interlockRepository.Query().Where(q => jidList.Contains(q.JobID));

                string Message = "";
                string notifyTitle = "【菲力工業報修】";
                urlMain = string.IsNullOrEmpty(urlMain) ? $"{Request.Url.Scheme}://{Request.Url.Authority}/Admin/RepairAdmin/Edit/{repairInfo.ID}" : urlMain;

                RepairAdminController repairController = new RepairAdminController();
                string repairInfoStr = repairController.ComposeRepairInfoStr(repairInfo);
                Message = $" \r\n{notifyTitle}待驗收報修單通知 \r\n \r\n";
                Message += repairInfoStr;
                Message += $" \r\n 維修人員已排除狀況，請驗收人員前往驗收，確認問題已妥善處理後，勾選完工(將計算工時)。 \r\n \r\n";
              
                Message += $" 查看 : {urlMain} \r\n \r\n";

                if (!string.IsNullOrEmpty(Message))
                {
                    foreach (var item in notifyList)
                    {
                        repairController.SetNotify(Message, "", "", item.ID, item.TokenKey);
                    }
                    return "successed";
                }
                return "failed : Repair information not found.";
            }
            catch (Exception ex)
            {
                return "failed : " + ex.Message;
            }
        }
    }
}