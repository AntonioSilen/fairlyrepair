﻿using FairlyRepair.Areas.Admin.ViewModels.Tags;
using FairlyRepair.MeetModels;
using FairlyRepair.Models;
using FairlyRepair.Repositories;
using FairlyRepair.Utility.Helpers;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FairlyRepair.Areas.Admin.Controllers
{
    public class BaseAdminController : Controller
    {
        public readonly string FileUploads = System.Web.Configuration.WebConfigurationManager.AppSettings["FileUploads"];

        public String g_OracleERPConnectionString = @"Data Source=
                                                           (DESCRIPTION =
                                                             (ADDRESS_LIST =
                                                               (ADDRESS = (PROTOCOL = TCP)(HOST = nts8)(PORT = 1521))
                                                             )
                                                             (CONNECT_DATA =
                                                               (SERVICE_NAME = orcl)
                                                             )
                                                           )
                                                         ;Persist Security Info=True;User ID=fairlyvin;Password=fairlyvin";

        public string ControllerName
        {
            get
            {
                return this.ControllerContext.RouteData.Values["controller"].ToString();
            }
        }

        /// <summary>
        /// 取得圖片儲存路徑，取得方式為controller名稱，然後將Admin替換成Photo        
        /// </summary>
        public string PhotoFolder
        {
            get
            {
                string controllerName = ControllerName;
                string fileFolder = controllerName.Replace("Admin", "Photo");
                string fileUploads = Server.MapPath(FileUploads);
                string savePath = Path.Combine(fileUploads, fileFolder);
                return savePath;
            }
        }

        /// <summary>
        /// 顯示訊息
        /// 成功時為綠色訊息
        /// 失敗時為紅色訊息
        /// </summary>        
        /// <param name="success">
        /// 是否成功 成功:true 失敗:false
        /// </param>
        /// <param name="message">
        /// 要顯示的訊息，若帶入空字串則預設成[success==true]為"成功"，[success==false]為"失敗"
        /// </param>
        public void ShowMessage(bool success, string message)
        {
            string tempDataKey = success ? "Result" : "Error";

            if (string.IsNullOrEmpty(message))
                message = success ? "Success" : "Failed";

            this.TempData[tempDataKey] = message;
        }

        /// <summary>
        /// 找不到時的頁面
        /// </summary>
        /// <returns></returns>
        public ActionResult NotFound()
        {
            return View("~/Areas/Admin/Views/Shared/NotFound.cshtml");
        }

        /// <summary>
        /// 錯誤頁面
        /// </summary>
        /// <param name="errorMessage">
        /// 錯誤訊息
        /// </param>
        /// <returns></returns>
        public ActionResult Error(string errorMessage = "")
        {
            TempData["ErrorMessage"] = errorMessage;
            return View("~/Areas/Admin/Views/Shared/Error.cshtml");
        }

        /// <summary>
        /// CKEditor上傳圖片
        /// </summary>
        /// <param name="upload"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult Upload(HttpPostedFileBase upload)
        {
            string newFileName = ImageHelper.SaveFile(upload, PhotoFolder);

            string photoFolder = ControllerName.Replace("Admin", "Photo");
            string savePath = string.Format("{0}/{1}/",
                FileUploads.Replace("~", ""),
                photoFolder
                );

            return Json(new { uploaded = 1, fileName = newFileName, url = savePath + newFileName });
        }
        public string GetAuditState(MeetModels.AuditLevel auditLevel)
        {
            string defaultPersonId = "0000";
            var levelStr = "";
            if (auditLevel.ManagerReview != defaultPersonId)
            {
                levelStr += ((int)MeetAdminType.Manager).ToString() + ",";
            }
            if (auditLevel.ViceReview != defaultPersonId)
            {
                levelStr += ((int)MeetAdminType.Vice).ToString() + ",";
            }
            if (auditLevel.PresidentReview != defaultPersonId)
            {
                levelStr += ((int)MeetAdminType.President).ToString() + ",";
            }

            return levelStr;
        }

        public DateTime? SetAuditDate(int audit, DateTime? date)
        {
            if (audit > 1 && (date == null || Convert.ToDateTime(date) < Convert.ToDateTime("2000/01/01")))
            {
                return DateTime.Now;
            }
            return Convert.ToDateTime(date) < Convert.ToDateTime("2000/01/01") ? null : date;
        }

        //public string GetAuditRevicer(int state, int officer, int type = 0, string personId = "")
        //{
        //    Repositories.MeetRepositories.AdminRepository adminRepository = new Repositories.MeetRepositories.AdminRepository(new Models.Meet.MeetingSignInDBEntities());
        //    Repositories.T8Repositories.PersonRepository t8personRepository = new Repositories.T8Repositories.PersonRepository(new T8ERPEntities());

        //    var superior = "";
        //    var superiorInfo = new Models.Meet.Admin();
        //    switch (state)
        //    {
        //        case (int)AuditState.Processing://通知承辦人
        //            superiorInfo = adminRepository.GetById(officer);
        //            superior = superiorInfo == null ? "" : superiorInfo.Account;
        //            break;
        //        case (int)AuditState.OfficerAudit://通知部門主管
        //            if (!string.IsNullOrEmpty(personId))
        //            {
        //                //填表人部門
        //                var personInfo = t8personRepository.FindByJobID(personId);
        //                var personDept = personInfo == null ? "" : personInfo.DeptID;

        //                var sameDeptPersonIdList = t8personRepository.GetPersonQuery("", "", personDept).Select(q => q.JobID).ToList();
        //                superiorInfo = adminRepository.Query(true, (int)MeetAdminType.Manager).Where(q => q.DeptId == personDept).FirstOrDefault();
        //                superior = superiorInfo == null ? "" : superiorInfo.Account;
        //            }
        //            break;
        //        case (int)AuditState.ManagerAudit://通知副總經理
        //            superiorInfo = adminRepository.Query(true, (int)MeetAdminType.Vice).FirstOrDefault();
        //            superior = superiorInfo == null ? "" : superiorInfo.Account;
        //            break;
        //        case (int)AuditState.ViceAudit://通知總經理
        //            superiorInfo = adminRepository.Query(true, (int)MeetAdminType.President).FirstOrDefault();
        //            superior = superiorInfo == null ? "" : superiorInfo.Account;
        //            break;
        //        case (int)AuditState.PresidentAudit://通知填寫人員
        //            superior = personId;
        //            break;
        //    }
        //    return superior;
        //}

        public int HandleState(string createrDeptId, int OfficerAudit, int ManagerAudit, int ViceAudit, int PresidentAudit)
        {
            int AuditProcessing = (int)FormAudit.Processing;
            int AuditApproval = (int)FormAudit.Approval;
            int AuditReject = (int)FormAudit.Reject;
            Repositories.MeetRepositories.AuditLevelRepository auditLevelRepository = new Repositories.MeetRepositories.AuditLevelRepository(new MeetModels.MeetingSignInDBEntities());
            var state = (int)AuditState.OfficerAudit;
            var auditFlow = MeetAdminType.Manager;
            var finalAudit = auditLevelRepository.FindByDept(createrDeptId);
            var levelArr = GetAuditState(finalAudit).Split(',');
            bool isManager = levelArr.Contains(((int)MeetAdminType.Manager).ToString());
            bool isVice = levelArr.Contains(((int)MeetAdminType.Vice).ToString());
            bool isPres = levelArr.Contains(((int)MeetAdminType.President).ToString());
            if (OfficerAudit == AuditApproval)
            {//承辦人核准
                if (isManager && auditFlow == MeetAdminType.Manager)
                {//需主管審核
                    if (ManagerAudit == AuditApproval)
                    {//核准
                        state = (int)AuditState.ManagerAudit;
                        auditFlow = MeetAdminType.Vice;
                    }
                    else if (ManagerAudit == AuditReject)
                    {//駁回
                        state = (int)AuditState.Reject;
                    }
                    else
                    {//待審
                        return state;
                    }
                }
                else
                {//傳簽
                    auditFlow = MeetAdminType.Vice;
                }

                if (state != (int)AuditState.Reject)
                {
                    if (isVice && auditFlow == MeetAdminType.Vice)
                    {//需副總經理審核
                        if (ViceAudit == AuditApproval)
                        {//核准
                            state = (int)AuditState.ViceAudit;
                            auditFlow = MeetAdminType.President;
                        }
                        else if (ViceAudit == AuditReject)
                        {//駁回
                            state = (int)AuditState.Reject;
                        }
                        else
                        {//待審
                            return (int)AuditState.ManagerAudit;
                        }
                    }
                    else
                    {//傳簽
                        auditFlow = MeetAdminType.President;
                    }
                }
                if (state != (int)AuditState.Reject)
                {
                    if (isPres && auditFlow == MeetAdminType.President)
                    {//需總經理審核
                        if (PresidentAudit == AuditApproval)
                        {//核准
                            state = (int)AuditState.Approval;
                        }
                        else if (PresidentAudit == AuditReject)
                        {//駁回
                            state = (int)AuditState.Reject;
                        }
                        else
                        {//待審
                            return (int)AuditState.ViceAudit;
                        }
                    }
                    else
                    {//傳簽
                        state = (int)AuditState.Approval;
                    }
                }
            }

            return state;
        }

        public MainAdmin GetMainAccountInfo(string account)
        {
            AdminRepository adminRepository = new AdminRepository(new FairlyRepairDBEntities());
            Repositories.MeetRepositories.AdminRepository meetAdminRepository = new Repositories.MeetRepositories.AdminRepository(new MeetingSignInDBEntities());
            var admin = adminRepository.Query(null, 0, account).FirstOrDefault();
            var data = meetAdminRepository.GetByAccount(admin.Account);
            if (data == null)
            {
                var webAdmin = adminRepository.GetById(admin.ID);
               meetAdminRepository.Insert(new MainAdmin()
                {
                    Account = webAdmin.Account,
                    Password = webAdmin.Password,
                    Type = (int)MeetAdminType.Employee,
                    DeptId = webAdmin.DeptId,
                    Department = webAdmin.DeptId,
                    Name = webAdmin.Name,
                    Status = true
                });
                return meetAdminRepository.GetByAccount(admin.Account);
            }
          
            return data;
        }

        public TagsRepository tagsRepository = new TagsRepository(new FairlyRepairDBEntities());
        [HttpPost]
        public JsonResult GetTags(string key)
        {
            var result = tagsRepository.GetAll().Where(t => t.TagName.Contains(key));
            var tagResult = new List<AutoCompleteView>();
            foreach (var item in result)
            {
                var tag = new AutoCompleteView();
                tag.label = tag.value = item.TagName;
                tagResult.Add(tag);
            }

            //result格式未處理，須為 {"XXX":[{"label":"A", "value":1},{"label":"B","value":2},{"label":"C","value":3}]} 

            return Json(tagResult);
        }
    }
}