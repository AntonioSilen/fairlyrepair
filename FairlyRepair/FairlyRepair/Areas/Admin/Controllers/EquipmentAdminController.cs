﻿using AutoMapper;
using FairlyRepair.Models.Others;
using FairlyRepair.Repositories.T8Repositories;
using FairlyRepair.Models;
using FairlyRepair.Repositories;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FairlyRepair.Utility.Helpers;
using FairlyRepair.Areas.Admin.ViewModels.Equipment;
using FairlyRepair.ActionFilters;

namespace FairlyRepair.Areas.Admin.Controllers
{
    [Authorize]
    //[ErrorHandleAdminActionFilter]
    public class EquipmentAdminController : BaseAdminController
    {
        private PersonRepository personRepository = new PersonRepository(new T8ERPEntities());
        private EquipmentRepository equipmentRepository = new EquipmentRepository(new FairlyRepairDBEntities());
        private RepairRepository repairRepository = new RepairRepository(new FairlyRepairDBEntities());
        private RepairImageRepository repairImageRepository = new RepairImageRepository(new FairlyRepairDBEntities());

        public ActionResult Index(EquipmentIndexView model)
        {
            if (model.Parent != 0)
            {
                var parentInfo = equipmentRepository.GetById(model.Parent);
                model.Class = parentInfo.Class + 1;
            }
            model.Class = model.Class == 0 ? 1 : model.Class;
            var query = equipmentRepository.Query(model.IsOnline, model.Parent, model.Class);
            var pageResult = query.ToPageResult<Equipment>(model);
            model.PageResult = Mapper.Map<PageResult<EquipmentView>>(pageResult);
            foreach (var item in model.PageResult.Data)
            {
                if (item.Parent == 0 && item.Class == 1)
                {
                    item.ParentTitle = "菲力工業";
                }
                else
                {
                    item.ParentTitle = equipmentRepository.GetEquipmentTitle(item.Parent);
                }
            }

            return View(model);
        }

        public ActionResult Edit(int id = 0, int Parent = 0, int Class = 0)
        {           
            var model = new EquipmentView();
            model.Parent = Parent;
            
            if (model.Parent != 0)
            {
                var parentInfo = equipmentRepository.GetById(model.Parent);
                model.Class = parentInfo.Class + 1;
            }
            model.Class = model.Class == 0 ? 1 : model.Class;
            if (id != 0)
            {
                var query = equipmentRepository.FindBy(id);
                model = Mapper.Map<EquipmentView>(query);
            }
            else
            {
                
            }
            model.ParentTitle = equipmentRepository.GetEquipmentTitle(model.Parent);
            model.BreadList = equipmentRepository.GetBreadList(model.Parent).OrderByDescending(q => q.Sort).ToList();

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public ActionResult Edit(EquipmentView model)
        {
            if (ModelState.IsValid)
            {
                Equipment data = Mapper.Map<Equipment>(model);
                int adminId = AdminInfoHelper.GetAdminInfo().ID;

                if (model.ID == 0)
                {
                    model.ID = equipmentRepository.Insert(data);
                    ShowMessage(true, "新增成功");
                }
                else
                {
                    equipmentRepository.Update(data);
                    ShowMessage(true, "修改成功");
                }

                #region 檔案處理
                bool hasFile = ImageHelper.CheckFileExists(model.MainPicFile);
                if (hasFile)
                {
                    ImageHelper.DeleteFile(PhotoFolder, model.MainPic);
                    data.MainPic = ImageHelper.SaveFile(model.MainPicFile, PhotoFolder);
                    equipmentRepository.Update(data);
                }
                #endregion

                return RedirectToAction("Edit", new { id = model.ID });
            }

            return View(model);
        }

        public ActionResult Delete(int id)
        {
            //DeleteImage(id, 1);

            equipmentRepository.Delete(id);
            ShowMessage(true, "刪除成功");
            return RedirectToAction("Index");
        }
    }
}