﻿using AutoMapper;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;
using FairlyRepair.Models;
using FairlyRepair.Models.Others;
using FairlyRepair.Repositories.T8Repositories;
using FairlyRepair.Repositories;
using FairlyRepair.Utility;
using FairlyRepair.Utility.Helpers;
using Ical.Net.DataTypes;
using Ical.Net.Serialization;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using FairlyRepair.Areas.Admin.ViewModels.Repair;
using FairlyRepair.ActionFilters;
using System.Net;
using System.Configuration;
using FairlyRepair.Areas.Admin.ViewModels.RepairRecord;
using OfficeOpenXml;
using System.Text.RegularExpressions;

namespace FairlyRepair.Areas.Admin.Controllers
{
    [Authorize]
    [ErrorHandleAdminActionFilter]
    public class RepairAdminController : BaseAdminController
    {
        public static Regex htmlRegex = new Regex("<.*?>", RegexOptions.Compiled);
        public string notifyUrl = ConfigurationManager.AppSettings["notifyUrl"];
        public string ClientID = ConfigurationManager.AppSettings["ClientID"];
        public string ClientSecret = ConfigurationManager.AppSettings["ClientSecret"];
        private PersonRepository personRepository = new PersonRepository(new T8ERPEntities());
        private RepairRepository repairRepository = new RepairRepository(new FairlyRepairDBEntities());
        private RepairImageRepository repairImageRepository = new RepairImageRepository(new FairlyRepairDBEntities());
        private DeviceRepository deviceRepository = new DeviceRepository(new FairlyRepairDBEntities());
        private GroupPersonRepository groupPersonRepository = new GroupPersonRepository(new T8ERPEntities());
        private AdminRepository adminRepository = new AdminRepository(new FairlyRepairDBEntities());
        private InterlockRepository interlockRepository = new InterlockRepository(new FairlyRepairDBEntities());
        private RepairRecordRepository repairRecordRepository = new RepairRecordRepository(new FairlyRepairDBEntities());
        private RepairRecordImageRepository repairRecordImageRepository = new RepairRecordImageRepository(new FairlyRepairDBEntities());
        private RepairRecordFeeRepository repairRecordFeeRepository = new RepairRecordFeeRepository(new FairlyRepairDBEntities());
        private EquipmentRepository equipmentRepository = new EquipmentRepository(new FairlyRepairDBEntities());

        public ActionResult OverView()
        {
            Repositories.T8Repositories.DepartmentRepository t8departmentRepository = new Repositories.T8Repositories.DepartmentRepository(new T8ERPEntities());
            OverViewView model = new OverViewView();
            model.StartDate = Convert.ToDateTime("2020/01/01");
            model.EndDate = DateTime.Now;

            return View(model);
        }

        public ActionResult Index(RepairIndexView model, int dt = 0)
        {
            if (dt == 0)
            {
                if (model.DeviceType == 0)
                {
                    return RedirectToAction("Index", "HomeAdmin");
                }
                dt = model.DeviceType;
            }
            var query = repairRepository.Query(model.IsPresApproval, model.IsViceApproval,
                model.PresAppStatus, model.ViceAppStatus,
                dt, model.DeviceID, model.FactoryID, model.FloorID, model.AreaID, model.DeptID, model.IsRevoke);

            model.MainterList = new List<MainterItem>();
            var selectMainters = new List<string>();
            if (!string.IsNullOrEmpty(model.Mainters))
            {
                selectMainters = model.Mainters.Split(',').ToList();
                foreach (var item in selectMainters)
                {
                    query = query.Where(q => q.MaintID.Contains(item));
                }
            }

            //var resultQuery = Mapper.Map<List<RepairView>>(query).AsQueryable();

            if (model.ProcessingStatus != null)
            {
                var recordIdList = repairRecordRepository.Query().Select(q => q.RepairID).Distinct().ToList();
                var dispatchedIdList = repairRepository.Query().Where(q => !string.IsNullOrEmpty(q.MaintID)).Select(q => q.ID).Distinct().ToList();
                var solvedIdList = repairRecordRepository.Query().Where(q => q.IsSolved).Select(q => q.RepairID).Distinct().ToList();
                switch (model.ProcessingStatus)
                {
                    case 0://尚未處裡
                        query = query.Where(q => !dispatchedIdList.Contains(q.ID) && !recordIdList.Contains(q.ID));
                        break;
                    case 1://處理中
                        query = query.Where(q => !solvedIdList.Contains(q.ID) && recordIdList.Contains(q.ID));
                        break;
                    case 2://已解決
                        query = query.Where(q => solvedIdList.Contains(q.ID) && recordIdList.Contains(q.ID));
                        break;
                    case 3://已派遣
                        query = query.Where(q => dispatchedIdList.Contains(q.ID) && !recordIdList.Contains(q.ID));
                        break;
                }
                query = query.Where(q => q.IsRevoke == false);
            }

            var Maintervalues = adminRepository.Query(true, (int)AdminType.Mainter);
            var Dispatchervalues = adminRepository.Query(true, (int)AdminType.Dispatcher);
            foreach (var item in Maintervalues)
            {
                if (item.Name != "測試人")
                {
                    model.MainterList.Add(new MainterItem()
                    {
                        ID = item.ID,
                        Name = item.Name,
                        IsSelected = selectMainters.Contains(item.Name),
                    });
                }
            }
            foreach (var item in Dispatchervalues)
            {
                if (item.Name != "測試人")
                {
                    model.MainterList.Add(new MainterItem()
                    {
                        ID = item.ID,
                        Name = item.Name,
                        IsSelected = selectMainters.Contains(item.Name),
                    });
                }
            }

            var pageResult = query.ToPageResult<Repair>(model);
            model.PageResult = Mapper.Map<PageResult<RepairView>>(pageResult);
            //var checkList = model.PageResult.Data.ToList();
            model.DeviceType = dt;

            foreach (var item in model.PageResult.Data)
            {
                var recordList = repairRecordRepository.Query(null, item.ID).OrderByDescending(q => q.CreateDate);
                if (recordList.Count() > 0)
                {
                    if (recordList.FirstOrDefault().IsSolved)
                    {
                        item.ProcessingStatus = 2;
                        continue;
                    }
                    item.ProcessingStatus = 1;
                }
                else if (!string.IsNullOrEmpty(item.MaintID))
                {
                    item.ProcessingStatus = 3;
                }
                else
                {
                    item.ProcessingStatus = 0;
                }
            }

            return View(model);
        }

        public ActionResult Edit(int id = 0, int dt = 1)
        {
            var model = new RepairView();
            var adminInfo = AdminInfoHelper.GetAdminInfo();

            if (id != 0)
            {
                var query = repairRepository.FindBy(id);
                model = Mapper.Map<RepairView>(query);
                model.PicList = repairImageRepository.Query(id).ToList();

                model.RecordList = Mapper.Map<List<RepairRecordView>>(repairRecordRepository.Query(null, id)).OrderByDescending(q => q.CreateDate).ToList();
                foreach (var item in model.RecordList)
                {
                    item.PicList = repairRecordImageRepository.Query(item.ID).ToList();
                }
                var recordList = repairRecordRepository.Query(true, id);
                if (recordList.Count() > 0 && !model.IsFinish && adminInfo.Type == (int)AdminType.Dispatcher)
                {
                    model.WaitForCheck = true;
                }
            }
            else
            {
                model.DeviceType = dt;
                //預設無須審核
                model.PresAppStatus = 0;
                model.ViceAppStatus = 0;
                model.IsPresApproval = false;
                model.IsViceApproval = false;
            }

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public ActionResult Edit(RepairView model/*, List<HttpPostedFileBase>pics*/)
        {
            if (ModelState.IsValid)
            {
                Repair data = Mapper.Map<Repair>(model);
                var adminInfo = AdminInfoHelper.GetAdminInfo();
                int adminId = adminInfo.ID;
                data.Updater = adminId;
                model.DispatcherID = GetDispatcherID(model);

                if (model.ID == 0)
                {
                    if (adminInfo.Type == (int)AdminType.Dispatcher)
                    {
                        data.PresAppStatus = model.IsPresApproval ? 1 : 0;
                        data.ViceAppStatus = model.IsViceApproval ? 1 : 0;
                    }

                    data.Creater = adminId;
                    if (!string.IsNullOrEmpty(model.MaintID))
                    {
                        model.DispatchTime = DateTime.Now;
                    }
                    model.ID = repairRepository.Insert(data);
                    ShowMessage(true, "新增成功");
                }
                else
                {
                    var oldData = repairRepository.FindBy(model.ID);

                    if (adminInfo.Type == (int)AdminType.Dispatcher)
                    {//派工、驗收人員
                        if (!oldData.IsPresApproval && model.IsPresApproval)
                        {
                            data.PresAppStatus = 1;
                        }
                        if (!oldData.IsViceApproval && model.IsViceApproval)
                        {
                            data.ViceAppStatus = 1;
                        }

                        if (string.IsNullOrEmpty(oldData.MaintID) && !string.IsNullOrEmpty(model.MaintID))
                        {//派工時間
                            data.DispatchTime = DateTime.Now;
                        }

                        if (model.IsFinish && (oldData.CompleteTime == null || oldData.Minute <= 0) || oldData.Minute > 16 * 60)
                        {//完工時間
                            var recordList = repairRecordRepository.Query(true, model.ID);
                            if (recordList.Count() > 0)
                            {
                                data.CompleteTime = recordList.OrderBy(q => q.CreateDate).FirstOrDefault().CreateDate;
                            }
                            else
                            {
                                data.CompleteTime = DateTime.Now;
                            }
                            //計算工時
                            data.Minute = CalculateMinutes(Convert.ToDateTime(data.DispatchTime), Convert.ToDateTime(data.CompleteTime));
                            //通知申報人員
                            try
                            {
                                var reciver = new List<string>();
                                var applicantAdminInfo = adminRepository.GetById(data.Creater);
                                if (applicantAdminInfo != null)
                                {
                                    var applicantInfo = groupPersonRepository.FindByJobID(applicantAdminInfo.Account);
                                    if (applicantInfo != null)
                                    {
                                        reciver.Add(applicantInfo.EMail);

                                        #region 寄送Email
                                        var repairInfo = Mapper.Map<Areas.Admin.ViewModels.Repair.RepairView>(repairRepository.GetById(data.ID));
                                        //string to = "seelen12@fairlybike.com";
                                        string to = applicantInfo.EMail;
                                        string subject = "";
                                        string mailBody = "";
                                        string DeviceItemName = !string.IsNullOrEmpty(repairInfo.DeviceItemName) ? " (" + repairInfo.DeviceItemName + ") " : "";
                                        mailBody += $"<p> 申報人 : {repairInfo.CreaterName} </p>";
                                        mailBody += $"<p> 課別 : {repairInfo.DepartmentTitle} </p>";
                                        mailBody += $" <p> 廠區 : {repairInfo.FactoryTitle} </p>";
                                        mailBody += $" <p> 樓層 : {repairInfo.FloorTitle} </p>";
                                        mailBody += $" <p> 區域 : {repairInfo.AreaTitle} </p>";
                                        mailBody += $" <p> 設備 : {repairInfo.DeviceName}  {DeviceItemName}</p>";
                                        mailBody += $" <p> 故障現況 : {repairInfo.FaultStateOptions.Where(q => q.Value == repairInfo.FaultState.ToString()).FirstOrDefault().Text} </p>";
                                        mailBody += $" <p> 申請時間 : {repairInfo.CreateDate} </p>" +
                                            $" <p>{ repairInfo.Description} </p> ";
                                        mailBody += $" 查看 : <a href=\"{Request.Url.Scheme}://{Request.Url.Authority}/Repair/Detail/{repairInfo.ID}\">前往察看</a> \r\n \r\n";

                                        //mailBody += $"<p>預定開始時間 : {repairInfo.CreateDate.ToString("yyyy/MM/dd HH:mm")}</p>" +
                                        //                            $"<p>{repairInfo.Description}</p>";

                                        subject = $"報修申請 - 完工通知";
                                        MailHelper.POP3Mail(to, subject, mailBody, null);
                                        #endregion
                                    }
                                }                             
                            }
                            catch (Exception e)
                            {

                            }
                        }

                        if (model.IsPurchase)
                        {//新增採購，更新為結案
                            data.IsClose = true;
                        }
                    }

                    if (data.IsFinish && !data.IsClose)
                    {
                        var haveFeeRecordIdList = repairRecordFeeRepository.Query(0).Where(q => q.Fee > 0).Select(q => q.RecordID).ToList();
                        var solvedRecordIdList = repairRecordRepository.Query(true, data.ID, haveFeeRecordIdList).Select(q => q.ID).ToList();
                        if (solvedRecordIdList.Count() > 0)
                        {
                            data.IsClose = true;
                        }
                    }

                    #region 通知
                    int repairType = 1;//報修類型
                    IdentifyNotifyType(model, adminInfo, oldData, repairType);
                    #endregion

                    repairRepository.Update(data);
                    ShowMessage(true, "修改成功");
                }

                #region 檔案處理
                if (model.PicsFiles != null)
                {
                    foreach (HttpPostedFileBase pic in model.PicsFiles)
                    {
                        bool hasFile = ImageHelper.CheckFileExists(pic);
                        if (hasFile)
                        {
                            RepairImage repairData = new RepairImage();
                            //ImageHelper.DeleteFile(PhotoFolder, model.FileOne);
                            repairData.RepairID = model.ID;
                            repairData.Pics = ImageHelper.SaveFile(pic, PhotoFolder);
                            repairImageRepository.Insert(repairData);
                        }
                    }
                }
                #endregion

                return RedirectToAction("Edit", new { id = model.ID });
            }

            return View(model);
        }

        /// <summary>
        /// 分辨Line Notify 訊息類型及對象
        /// </summary>
        /// <param name="model">新資料</param>
        /// <param name="adminInfo">發送人層級</param>
        /// <param name="oldData">原資料</param>
        /// <param name="repairType">1.報修  2.新增設備</param>
        private void IdentifyNotifyType(RepairView model, AdminInfo adminInfo, Repair oldData, int repairType)
        {
            switch (adminInfo.Type)
            {
                case (int)AdminType.Dispatcher:
                    //通知維修人員(安衛課全體)
                    if (oldData.MaintID != model.MaintID && !string.IsNullOrEmpty(model.MaintID))
                    {
                        var personList = model.MaintID.Split(',');
                        if (personList.Count() > 0)
                        {
                            foreach (var item in personList)
                            {
                                if (!string.IsNullOrEmpty(item))
                                {
                                    var mainterInfo = personRepository.GetPersonQuery(item).FirstOrDefault();
                                    var mainterAdminInfo = adminRepository.Query(true, 0, mainterInfo.JobID).FirstOrDefault();
                                    if (mainterAdminInfo != null)
                                    {
                                        SendLineNotify(model.ID, repairType, (int)AdminType.Mainter, false, false, "", mainterAdminInfo.Account, true, mainterAdminInfo.ID.ToString());//指定維修員
                                    }
                                }
                            }
                        }
                        //SendLineNotify(model.ID, repairType, (int)AdminType.Mainter, false, false, "", "", true, model.MaintID);//維修員
                        //SendLineNotify(model.ID, repairType, (int)AdminType.Dispatcher, false, false, "", "", true, model.MaintID);//調度員

                        //SendLineNotify(model.ID, repairType, 0, false, false, "", "2416");//2416
                    }
                    //通知總經理審核
                    if (!oldData.IsPresApproval && model.IsPresApproval)
                    {
                        SendLineNotify(model.ID, repairType, (int)AdminType.President);
                    }
                    //通知副總審核
                    if (!oldData.IsViceApproval && model.IsViceApproval)
                    {
                        SendLineNotify(model.ID, repairType, (int)AdminType.VicePresident);
                    }
                    break;
                case (int)AdminType.President:
                    //通知調度員審核結果
                    if (oldData.IsPresApproval && oldData.PresAppStatus != model.PresAppStatus)
                    {
                        SendLineNotify(model.ID, repairType, (int)AdminType.Dispatcher, true);
                    }
                    break;
                case (int)AdminType.VicePresident:
                    //通知調度員審核結果
                    if (oldData.IsViceApproval && oldData.ViceAppStatus != model.ViceAppStatus)
                    {
                        SendLineNotify(model.ID, repairType, (int)AdminType.Dispatcher, false, true);
                    }
                    break;
            }
        }

        public ActionResult Delete(int id)
        {
            //DeleteImage(id, 1);

            repairRepository.Delete(id);
            ShowMessage(true, "刪除成功");
            return RedirectToAction("Index");
        }

        public decimal CalculateMinutes(DateTime dispatchTime, DateTime completeTime)
        {
            // 定義工作時間
            TimeSpan workStartTime = new TimeSpan(8, 0, 0); // 08:00
            TimeSpan workEndTime = new TimeSpan(15, 0, 0); // 15:00

            // 如果 dispatchTime 超過工作時間，將其調整到下一個工作日的開始時間
            if (dispatchTime.TimeOfDay > workEndTime)
            {
                dispatchTime = dispatchTime.AddDays(1).Date.Add(workStartTime);
            }

            // 如果 completeTime 超過工作時間，也將其調整到當天的工作結束時間
            if (completeTime.TimeOfDay > workEndTime)
            {
                completeTime = completeTime.Date.Add(workEndTime);
            }

            // 初始化總分鐘數
            double totalMinutes = 0;

            // 迴圈計算每一天的有效工作時間
            while (dispatchTime < completeTime)
            {
                // 如果不是週末
                if (dispatchTime.DayOfWeek != DayOfWeek.Saturday && dispatchTime.DayOfWeek != DayOfWeek.Sunday)
                {
                    // 如果是同一天
                    if (dispatchTime.Date == completeTime.Date)
                    {
                        // 計算當天的剩餘有效時間
                        totalMinutes += (completeTime - dispatchTime).TotalMinutes;
                        break;
                    }
                    else
                    {
                        // 計算當天剩餘的有效工作時間
                        totalMinutes += (dispatchTime.Date.Add(workEndTime) - dispatchTime).TotalMinutes;
                    }
                }

                // 移動到下一個工作日的開始時間
                dispatchTime = dispatchTime.AddDays(1).Date.Add(workStartTime);
            }

            return Convert.ToDecimal(totalMinutes);
        }

        public int GetDispatcherID(RepairView model)
        {
            var adminInfo = AdminInfoHelper.GetAdminInfo();
            if (!string.IsNullOrEmpty(model.MaintID))
            {
                return 0;
            }
            if (adminInfo.Type == (int)AdminType.Dispatcher)
            {
                return adminInfo.ID;
            }
            return 0;
        }
        public ActionResult SendNotify(int repairId, int notifyType = 2)
        {
            try
            {
                //var repairInfo = Mapper.Map<RepairView>(repairRepository.FindBy(repairId));
                //if (repairInfo == null)
                //{
                //    ShowMessage(true, "發送失敗，查無會議資料");
                //}
                //var jobIdList = adminRepository.Query(true, notifyType).Select(q => q.Account).ToList();
                //var reciverList = groupPersonRepository.Query().Where(q => jobIdList.Contains(q.PersonId));
                //foreach (var item in reciverList)
                //{
                //    //寄送Email
                //    //string to = "seelen12@fairlybike.com";
                //    string to = item.EMail;
                //    string subject = "";
                //    string mailBody = "";
                //    mailBody = $"<h3>【{repairInfo.Title}】 {repairInfo.StartDate.ToString("yyyy/MM/dd HH:mm")} 於{repairInfo.RoomStr}</h3>" +
                //                                $"<p>發起人 : {repairInfo.Organiser}</p>" +
                //                                $"<p>通知人 : 「{item.DepartmentStr}」 {item.Name + item.EngName}</p>";

                //    mailBody += $"<p>預定開始時間 : {repairInfo.StartDate.ToString("yyyy/MM/dd HH:mm")}</p>" +
                //                                $"<p>預定結束時間 : {repairInfo.EndDate.ToString("yyyy/MM/dd HH:mm")}</p>";

                //    subject = $"會議通知 - 系統信件";

                //    var calendar = new Ical.Net.Calendar();

                //    DateTime today = DateTime.Now;
                //    calendar.Events.Add(new Ical.Net.CalendarComponents.CalendarEvent
                //    {
                //        Class = "PUBLIC",
                //        Summary = subject,
                //        Created = new CalDateTime(today),
                //        Description = $"【{repairInfo.Title}】報修單待審通知",
                //        Start = new CalDateTime(repairInfo.StartDate),
                //        End = new CalDateTime(repairInfo.EndDate),
                //        Sequence = 0,
                //        Uid = Guid.NewGuid().ToString(),
                //        Location = repairInfo.RoomStr,
                //    });

                //    var serializer = new CalendarSerializer(new SerializationContext());
                //    var serializedCalendar = serializer.SerializeToString(calendar);
                //    var bytesCalendar = Encoding.UTF8.GetBytes(serializedCalendar);
                //    MemoryStream ms = new MemoryStream(bytesCalendar);

                //    MailHelper.POP3Mail(to, subject, mailBody, ms);

                //    //    //新增報修紀錄

                //    //}

                //    ShowMessage(true, "通知發送成功");
                //}
            }
            catch (Exception ex)
            {
                ShowMessage(true, "發送失敗，錯誤訊息 : " + ex.Message);
            }
            return RedirectToAction("Edit", "RepairAdmin", new { id = repairId });
        }

        public JsonResult GetDeviceItem(int deviceId)
        {
            var query = deviceRepository.Query((int)DeviceClass.Third, deviceId);
            //result格式未處理，須為 {"XXX":[{"label":"A", "value":1},{"label":"B","value":2},{"label":"C","value":3}]} 
            var resultList = query.ToList();
            return Json(resultList);
        }

        /// <summary>
        /// 帶入訊息資料
        /// </summary>
        /// <param name="repairId">報修單編號</param>
        /// <param name="adminType">接收通知層級</param>
        /// <param name="pres">總經理發送</param>
        /// <param name="vice">副總發送</param>
        /// <returns></returns>
        public string SendLineNotify(int repairId, int repairType, int adminType, bool pres = false, bool vice = false, string urlMain = "", string personId = "", bool isChangeMainter = false, string mainterId = "")
        {
            try
            {
                var repairInfo = Mapper.Map<Areas.Admin.ViewModels.Repair.RepairView>(repairRepository.GetById(repairId));


                string Message = "";
                string notifyTitle = repairType == 1 ? "【菲力工業報修通知】" : "【菲力工業】添加、更換設備申請通知";
                urlMain = string.IsNullOrEmpty(urlMain) ? $"{Request.Url.Scheme}://{Request.Url.Authority}/Admin" : urlMain;
                string repairInfoStr = ComposeRepairInfoStr(repairInfo);
                if (repairType == 1)
                {
                    switch (adminType)
                    {
                        #region 調度員
                        case (int)AdminType.Dispatcher://調度員
                            if (isChangeMainter)
                            {
                                string newMainter = repairInfo.MaintStaffOptions.Where(q => q.Value == mainterId).FirstOrDefault().Text;
                                Message = $" {notifyTitle}指派人員處理  \r\n \r\n";
                                Message += repairInfoStr;
                                Message += $" 維修人員 :{newMainter} \r\n \r\n";
                                Message += $" 查看 : {urlMain}/RepairAdmin/Edit/{repairInfo.ID} \r\n \r\n";
                            }
                            else
                            {
                                if (pres)
                                {
                                    Message = $" {notifyTitle}總經理審核結果  \r\n \r\n";
                                    Message += repairInfoStr;
                                    Message += $" 查看 : {urlMain}/RepairAdmin/Edit/{repairInfo.ID} \r\n \r\n";
                                }
                                else if (vice)
                                {
                                    Message = $" {notifyTitle}副總審核結果  \r\n \r\n";
                                    Message += repairInfoStr;
                                    Message += $" 查看 : {urlMain}/RepairAdmin/Edit/{repairInfo.ID} \r\n \r\n";
                                }
                            }
                            break;
                        #endregion
                        #region 維修員
                        case (int)AdminType.Mainter://維修員
                            if (isChangeMainter)
                            {
                                string newMainter = repairInfo.MaintStaffOptions.Where(q => q.Value == mainterId).FirstOrDefault().Text;
                                Message = $" {notifyTitle}指派人員處理  \r\n \r\n";
                                Message += repairInfoStr;
                                Message += $" 維修人員 :{newMainter} \r\n \r\n";
                                Message += $" 查看 : {urlMain}/RepairAdmin/Edit/{repairInfo.ID} \r\n \r\n";
                            }

                            break;
                        #endregion
                        #region 總經理
                        case (int)AdminType.President://總經理
                            Message = $" {notifyTitle}申請總經理審核  \r\n \r\n";
                            Message += repairInfoStr;
                            Message += $" 審閱 : {urlMain}/ApprovalAdmin/Edit/{repairInfo.ID} \r\n \r\n";
                            break;
                        #endregion
                        #region 副總
                        case (int)AdminType.VicePresident://副總
                            Message = $" {notifyTitle}申請副總審核  \r\n \r\n";
                            Message += repairInfoStr;
                            Message += $" 審閱 : {urlMain}/ApprovalAdmin/Edit/{repairInfo.ID} \r\n \r\n";
                            break;
                            #endregion
                    }
                }
                else if (repairType == 2)
                {
                    switch (adminType)
                    {
                        case (int)AdminType.Mainter://維修員
                            Message = $" \r\n{notifyTitle}指派處理  \r\n \r\n";
                            Message += repairInfoStr;
                            Message += $" \r\n 查看 : {urlMain}/ReplaceAdmin/Edit/{repairInfo.ID} \r\n \r\n";
                            break;
                        case (int)AdminType.President://總經理
                            Message = $" \r\n{notifyTitle}申請總經理審核  \r\n \r\n";
                            Message += repairInfoStr;
                            Message += $" \r\n 審閱 : {urlMain}/ApprovalAdmin/ReplaceEdit/{repairInfo.ID} \r\n \r\n";
                            break;
                        case (int)AdminType.VicePresident://副總
                            Message = $" \r\n{notifyTitle}申請副總審核  \r\n \r\n";
                            Message += repairInfoStr;
                            Message += $" \r\n 審閱 : {urlMain}/ApprovalAdmin/ReplaceEdit/{repairInfo.ID} \r\n \r\n";
                            break;
                        case (int)AdminType.Dispatcher://調度員
                            if (pres)
                            {
                                Message = $" \r\n{notifyTitle}總經理審核結果  \r\n \r\n";
                                Message += repairInfoStr;
                                Message += $" \r\n 查看 : {urlMain}/ReplaceAdmin/Edit/{repairInfo.ID} \r\n \r\n";
                            }
                            else if (vice)
                            {
                                Message = $" \r\n{notifyTitle}副總審核結果  \r\n \r\n";
                                Message += repairInfoStr;
                                Message += $" \r\n 查看 : {urlMain}/ReplaceAdmin/Edit/{repairInfo.ID} \r\n \r\n";
                            }
                            break;
                    }
                }
                if (!string.IsNullOrEmpty(Message))
                {
                    var query = adminRepository.Query(true, adminType);
                    if (isChangeMainter)
                    {
                        query = adminRepository.Query(true).Where(q => q.Type == (int)AdminType.Dispatcher || q.Type == (int)AdminType.Mainter);
                    }
                    if (!string.IsNullOrEmpty(personId))
                    {
                        query = adminRepository.Query(true, 0, personId);
                    }

                    var jidList = query.Select(q => q.Account).ToList();
                    var notifyList = interlockRepository.Query().Where(q => jidList.Contains(q.JobID));
                    foreach (var item in notifyList)
                    {
                        SetNotify(Message, "", "", item.ID, item.TokenKey);
                    }
                    return "successed";
                }
                return "failed : Repair information not found.";
            }
            catch (Exception ex)
            {
                return "failed : " + ex.Message;
            }
        }

        public string ComposeRepairInfoStr(RepairView repairInfo)
        {
            string DeviceItemName = !string.IsNullOrEmpty(repairInfo.DeviceItemName) ? " (" + repairInfo.DeviceItemName + ") " : "";
            string repairInfoStr = $" 課別 : {repairInfo.DepartmentTitle} \r\n";
            repairInfoStr += $" 廠區 : {repairInfo.FactoryTitle} \r\n";
            repairInfoStr += $" 樓層 : {repairInfo.FloorTitle} \r\n";
            repairInfoStr += $" 區域 : {repairInfo.AreaTitle} \r\n";
            repairInfoStr += $" 設備 : {repairInfo.DeviceName} \r\n";
            repairInfoStr += $" 故障現況 : {repairInfo.FaultStateOptions.Where(q => q.Value == repairInfo.FaultState.ToString()).FirstOrDefault().Text} \r\n";
            repairInfoStr += $" 申請時間 : {repairInfo.CreateDate}  {DeviceItemName}\r\n";
            return repairInfoStr;
        }

        #region Send Notify Message
        /// <summary>
        /// 發送Notify (multipart)
        /// </summary>
        /// <param name="MemberID"></param>
        /// <param name="ClientID"></param>
        /// <param name="CallbackUrl"></param>
        /// <param name="Token"></param>
        /// <param name="ClientSecret"></param>
        /// <param name="Message"></param>
        /// <param name="ImageFile"></param>
        /// <param name="ImageUrl"></param>
        /// <param name="ImageThumbnailUrl"></param>
        /// <param name="STKID"></param>
        /// <param name="STKPKGID"></param>
        /// <returns></returns>
        public string SetNotify(string Message, string ImageUrl, string Sticker, int Iid, string TokenKey)
        {
            //var serviceInfo = serviceRepository.GetById(sid);
            var stk = Sticker.Split(',');
            MessageView imgmodel = new MessageView
            {
                ClientID = ClientID,
                CallbackUrl = "http://220.228.58.44:18153/Api/Callback",
                ClientSecret = ClientSecret,
                Message = "",
                ImageUrl = ImageUrl,
            };
            MessageView model = new MessageView
            {
                ClientID = ClientID,
                CallbackUrl = "http://220.228.58.44:18153/Api/Callback",
                ClientSecret = ClientSecret,
                Message = Message,
                ImageUrl = "",
            };
            if (stk.Count() == 2)
            {
                model.STKID = Convert.ToInt32(stk[0]);
                model.STKPKGID = Convert.ToInt32(stk[1]);
            }

            List<int> delList = new List<int>();

            //res = multipartTest(imgmodel, TokenKey);
            int res = multipartTest(model, TokenKey);
            if (res == 0)
            {
                interlockRepository.Delete(Iid);
                return "delete";
            }

            return "success";
        }

        /// <summary>
        /// multipart 發送訊息
        /// </summary>
        /// <param name="message"></param>
        /// <param name="token"></param>
        private int multipartTest(MessageView model, string tokenKey)
        {
            try
            {
                if (string.IsNullOrEmpty(model.Message))
                    model.Message = "發送訊息~";
                Dictionary<string, object> d = new Dictionary<string, object>()
                {
                    // message , imageFile ... name is provided by LINE API
                    { "message", model.Message },
                    //{ "imageFile", new FormFile()
                    //    {
                    //        Name = "notify.jpg", ContentType = "image/jpeg", FilePath="notify.jpg"
                    //    }
                    //}
                };
                if (model.STKID != 0 && model.STKPKGID != 0)
                {
                    d.Add("stickerPackageId", model.STKPKGID.ToString());
                    d.Add("stickerId", model.STKID.ToString());
                    //postData += "&stickerPackageId=" + model.STKPKGID.ToString();
                    //postData += "&stickerId=" + model.STKID.ToString();
                }

                if (!string.IsNullOrEmpty(model.ImageFile))
                {
                    //d.Add("imageFile", new FormFile()
                    //{ Name = Path.GetFileName(model.ImageFile), ContentType = "image/jpeg", FilePath = model.ImageFile }
                    //);
                }

                if (!string.IsNullOrEmpty(model.ImageUrl))
                {
                    d.Add("imageFullsize", model.ImageUrl);
                    //postData += "&imageFullsize=" + model.ImageUrl;
                    if (!string.IsNullOrEmpty(model.ImageThumbnailUrl))
                        d.Add("imageThumbnail", model.ImageThumbnailUrl);
                    //postData += "&imageThumbnail=" + model.ImageThumbnailUrl;
                    else
                        d.Add("imageThumbnail", model.ImageUrl);
                    //postData += "&imageThumbnail=" + model.ImageUrl;
                }

                string boundary = "Boundary";
                List<byte[]> output = genMultPart(d, boundary);
                int res = lineNotifyMultipart(tokenKey, boundary, output);
                return res;
            }
            catch (Exception ex)
            {
                return 7;
            }
        }

        private int lineNotifyMultipart(string access_token, string boundary, List<byte[]> output)
        {
            try
            {
                #region POST multipart/form-data
                StringBuilder sb = new StringBuilder();
                HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create(notifyUrl);
                request.Method = "POST";
                request.ContentType = "multipart/form-data; boundary=Boundary";
                request.Timeout = 30000;

                // header
                sb.Clear();
                sb.Append("Bearer ");
                sb.Append(access_token);
                request.Headers.Add("Authorization", sb.ToString());
                // note: multipart/form-data boundary must exist in headers ContentType
                sb.Clear();
                sb.Append("multipart/form-data; boundary=");
                sb.Append(boundary);
                request.ContentType = sb.ToString();

                // write Post Body Message
                BinaryWriter bw = new BinaryWriter(request.GetRequestStream());
                foreach (byte[] bytes in output)
                    bw.Write(bytes);

                #endregion

                int res = getResponse(request, access_token);

                return res;

            }
            catch (Exception ex)
            {
                #region Exception
                StringBuilder sbEx = new StringBuilder();
                sbEx.Append(ex.GetType());
                sbEx.AppendLine();
                sbEx.AppendLine(ex.Message);
                sbEx.AppendLine(ex.StackTrace);
                if (ex.InnerException != null)
                    sbEx.AppendLine(ex.InnerException.Message);
                //myException ex2 = new myException(sbEx.ToString());
                //message(ex2.Message);
                return 6;
                #endregion
            }
        }

        private int getResponse(HttpWebRequest request, string access_token)
        {
            StringBuilder sb = new StringBuilder();
            string result = string.Empty;
            StreamReader sr = null;
            try
            {
                #region Get Response
                if (request == null)
                    return 99;
                // HttpWebRequest GetResponse() if error happened will trigger WebException
                using (HttpWebResponse response = request.GetResponse() as HttpWebResponse)
                {
                    sb.AppendLine();
                    foreach (var x in response.Headers)
                    {
                        sb.Append(x);
                        sb.Append(" : ");
                        sb.Append(response.Headers[x.ToString()]);
                        if (x.ToString() == "X-RateLimit-Reset")
                        {
                            sb.Append(" ( ");
                            sb.Append(DateTimeOffset.FromFileTime(long.Parse(response.Headers[x.ToString()])));
                            sb.Append(" )");
                        }
                        sb.AppendLine();
                    }
                    using (sr = new StreamReader(response.GetResponseStream()))
                    {
                        result = sr.ReadToEnd();
                        sb.Append(result);
                    }
                }
                return 1;
                //message(sb.ToString());

                #endregion
            }
            catch (WebException ex)
            {
                if (ex.Message == "遠端伺服器傳回一個錯誤: (401) 未經授權。")
                {
                    //var unLink = interlockRepository.GetAll().Where(q => q.TokenKey == access_token).FirstOrDefault();
                    //interlockRepository.Delete(unLink.ID);
                    return 0;
                }
                else
                {
                    #region WebException handle
                    //var wrong_token = tokenRepository.GetAll().Where(q => q.TokenKey == access_token).FirstOrDefault();
                    //tokenRepository.Delete(wrong_token.ID);

                    // WebException Response
                    using (HttpWebResponse response = (HttpWebResponse)ex.Response)
                    {
                        sb.AppendLine("Error");
                        foreach (var x in response.Headers)
                        {
                            sb.Append(x);
                            sb.Append(" : ");
                            sb.Append(response.Headers[x.ToString()]);
                            sb.AppendLine();
                        }
                        using (sr = new StreamReader(response.GetResponseStream()))
                        {
                            result = sr.ReadToEnd();
                            sb.Append(result);
                        }

                        //message(sb.ToString());
                    }
                    return 5;
                    #endregion
                }
            }
        }

        public List<byte[]> genMultPart(Dictionary<string, object> parameters, string boundary)
        {
            StringBuilder sb = new StringBuilder();
            sb.Clear();
            sb.Append("\r\n--");
            sb.Append(boundary);
            sb.Append("\r\n");
            string beginBoundary = sb.ToString();
            sb.Clear();
            sb.Append("\r\n--");
            sb.Append(boundary);
            sb.Append("--\r\n");
            string endBoundary = sb.ToString();
            sb.Clear();
            sb.Append("Content-Type: multipart/form-data; boundary=");
            sb.Append(boundary);
            sb.Append("\r\n");
            List<byte[]> byteList = new List<byte[]>();
            byteList.Add(System.Text.Encoding.UTF8.GetBytes(sb.ToString()));

            foreach (KeyValuePair<string, object> pair in parameters)
            {
                //if (pair.Value is FormFile)
                //{
                //    byteList.Add(System.Text.Encoding.ASCII.GetBytes(beginBoundary));
                //    FormFile form = pair.Value as FormFile;

                //    sb.Clear();
                //    sb.Append("Content-Disposition: form-data; name=\"");
                //    sb.Append(pair.Key);
                //    sb.Append("\"; filename=\"");
                //    sb.Append(form.Name);
                //    sb.Append("\"\r\nContent-Type: ");
                //    sb.Append(form.ContentType);
                //    sb.Append("\r\n\r\n");
                //    byte[] bytes = System.Text.Encoding.UTF8.GetBytes(sb.ToString());
                //    byteList.Add(bytes);
                //    if (form.bytes == null && !string.IsNullOrEmpty(form.FilePath))
                //    {
                //        FileStream fs = new FileStream(form.FilePath, FileMode.Open, FileAccess.Read);
                //        MemoryStream ms = new MemoryStream();
                //        fs.CopyTo(ms);
                //        byteList.Add(ms.ToArray());
                //    }
                //    else
                //        byteList.Add(form.bytes);
                //}
                //else
                //{
                byteList.Add(System.Text.Encoding.ASCII.GetBytes(beginBoundary));
                sb.Clear();
                sb.Append("Content-Disposition: form-data; name=\"");
                sb.Append(pair.Key);
                sb.Append("\"");
                sb.Append("\r\n\r\n");
                sb.Append(pair.Value);
                string data = sb.ToString();
                byte[] bytes = System.Text.Encoding.UTF8.GetBytes(data);
                byteList.Add(bytes);
                //}
            }

            byteList.Add(System.Text.Encoding.ASCII.GetBytes(endBoundary));
            return byteList;
        }

        #endregion

        /// <summary>
        /// 匯出Excel清單 (非同步)
        /// </summary>
        /// <returns></returns>
        public FileResult ExportListExcel(bool? isPres = null, bool? isVice = null,
            int? presStatus = null, int? viceStatus = null, int deviceType = 0,
            int deviceId = 0, int factory = 0, int floor = 0, int area = 0, string deptId = "", bool? isRevoke = null)
        {
            //取出要匯出Excel的資料
            var query = repairRepository.Query(isPres, isVice, presStatus, viceStatus,
                deviceType, deviceId, factory, floor, area, deptId, isRevoke).ToList();
            List<RepairView> repairList = Mapper.Map<IEnumerable<RepairView>>(query).ToList();

            string Id = "";
            string field = "";
            try
            {
                ExcelPackage.LicenseContext = LicenseContext.NonCommercial; // 關閉新許可模式通知
                //建立Excel
                ExcelPackage ep = new ExcelPackage();

                //建立第一個Sheet，後方為定義Sheet的名稱
                ExcelWorksheet sheet = ep.Workbook.Worksheets.Add("報修清單");

                int col = 1;    //欄:直的，因為要從第1欄開始，所以初始為1

                //第1列是標題列 
                //標題列部分，是取得DataAnnotations中的DisplayName，這樣比較一致，
                //這也可避免後期有修改欄位名稱需求，但匯出excel標題忘了改的問題發生。
                //取得做法可參考最後的參考連結。

                sheet.Cells[1, col++].Value = "ID";
                sheet.Cells[1, col++].Value = "申請人";
                sheet.Cells[1, col++].Value = "維修員";
                sheet.Cells[1, col++].Value = "撤銷狀態";
                sheet.Cells[1, col++].Value = "廠區";
                sheet.Cells[1, col++].Value = "樓層";
                sheet.Cells[1, col++].Value = "區域";
                sheet.Cells[1, col++].Value = "設備";
                sheet.Cells[1, col++].Value = "故障現況";
                sheet.Cells[1, col++].Value = "說明";
                sheet.Cells[1, col++].Value = "需總經理審核";
                sheet.Cells[1, col++].Value = "總經理審核結果";
                sheet.Cells[1, col++].Value = "總經理審核說明";
                sheet.Cells[1, col++].Value = "需副總審核";
                sheet.Cells[1, col++].Value = "副總審核結果";
                sheet.Cells[1, col++].Value = "副總審核說明";
                sheet.Cells[1, col++].Value = "送審原因";
                sheet.Cells[1, col++].Value = "申請時間";

                //資料從第2列開始
                int row = 2;    //列:橫的
                foreach (RepairView item in repairList)
                {
                    Id = item.ID.ToString();
                    col = 1;//每換一列，欄位要從1開始
                            //指定Sheet的欄與列(欄名列號ex.A1,B20，在這邊都是用數字)，將資料寫入

                    sheet.Cells[row, col++].Value = item.ID;
                    sheet.Column(col).AutoFit();
                    sheet.Cells[row, col++].Value = item.CreaterName;
                    sheet.Column(col).AutoFit();
                    sheet.Cells[row, col++].Value = item.MaintID;
                    sheet.Column(col).AutoFit();
                    sheet.Cells[row, col++].Value = item.IsRevoke ? "撤銷" : "";
                    sheet.Column(col).AutoFit();
                    sheet.Cells[row, col++].Value = item.FactoryTitle;
                    sheet.Column(col).AutoFit();
                    sheet.Cells[row, col++].Value = item.FloorTitle;
                    sheet.Column(col).AutoFit();
                    sheet.Cells[row, col++].Value = item.AreaTitle;
                    sheet.Column(col).AutoFit();
                    sheet.Cells[row, col++].Value = item.DeviceName;
                    sheet.Column(col).AutoFit();
                    field = "FaultState";
                    sheet.Cells[row, col++].Value = item.FaultState == 0 ? "" : EnumHelper.GetDescription((FaultState)item.FaultState);
                    sheet.Column(col).AutoFit();
                    field = "Description";
                    sheet.Cells[row, col++].Value = item.Description;
                    sheet.Column(col).AutoFit();
                    field = "IsPresApproval";
                    sheet.Cells[row, col++].Value = item.IsPresApproval ? "須審核" : "";
                    sheet.Column(col).AutoFit();
                    field = "PresAppStatus";
                    sheet.Cells[row, col++].Value = GetAppStatus(item.PresAppStatus);
                    sheet.Column(col).AutoFit();
                    field = "PresResult";
                    sheet.Cells[row, col++].Value = item.PresResult;
                    sheet.Column(col).AutoFit();
                    field = "IsViceApproval";
                    sheet.Cells[row, col++].Value = item.IsViceApproval ? "須審核" : "";
                    sheet.Column(col).AutoFit();
                    field = "ViceAppStatus";
                    sheet.Cells[row, col++].Value = GetAppStatus(item.ViceAppStatus);
                    sheet.Column(col).AutoFit();
                    field = "ViceResult";
                    sheet.Cells[row, col++].Value = item.ViceResult;
                    sheet.Column(col).AutoFit();
                    field = "SubmitReason";
                    sheet.Cells[row, col++].Value = !string.IsNullOrEmpty(item.SubmitReason) ? RemoveHTMLTagsCompiled(item.SubmitReason) : "";
                    sheet.Column(col).AutoFit();
                    field = "CreateDate";
                    sheet.Cells[row, col++].Value = item.CreateDate.ToString("yyyy/HH/mm HH:mm");
                    sheet.Column(col).AutoFit();
                    row++;
                }

                //因為ep.Stream是加密過的串流，故要透過SaveAs將資料寫到MemoryStream，在將MemoryStream使用FileStreamResult回傳到前端
                MemoryStream fileStream = new MemoryStream();
                ep.SaveAs(fileStream);
                ep.Dispose();//如果這邊不下Dispose，建議此ep要用using包起來，但是要記得先將資料寫進MemoryStream在Dispose。
                fileStream.Position = 0;//不重新將位置設為0，excel開啟後會出現錯誤

                return File(fileStream, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", DateTime.Now.ToString("yyyy-MM-dd HH:mm") + "報修清單.xlsx");
            }
            catch (Exception e)
            {
                //return Content(Id + ", " + field + ", " + e.Message);
                throw;
            }
        }

        public static string RemoveHTMLTagsCompiled(string html)
        {
            return htmlRegex.Replace(html, string.Empty);
        }

        /// <summary>
        /// 匯出Excel (非同步)
        /// </summary>
        /// <returns></returns>
        public ActionResult ExportExcel(int id)
        {
            //取出要匯出Excel的資料
            var query = repairRepository.GetById(id);
            RepairView repairInfo = Mapper.Map<RepairView>(query);

            try
            {
                ExcelPackage.LicenseContext = LicenseContext.NonCommercial; // 關閉新許可模式通知
                //建立Excel
                ExcelPackage ep = new ExcelPackage();

                //建立第一個Sheet，後方為定義Sheet的名稱
                ExcelWorksheet sheet = ep.Workbook.Worksheets.Add("報修資料");

                int row = 1;
                sheet.Cells[row++, 1].Value = "ID";
                sheet.Cells[row++, 1].Value = "申請人";
                sheet.Cells[row++, 1].Value = "撤銷狀態";
                sheet.Cells[row++, 1].Value = "廠區";
                sheet.Cells[row++, 1].Value = "樓層";
                sheet.Cells[row++, 1].Value = "區域";
                sheet.Cells[row++, 1].Value = "設備";
                sheet.Cells[row++, 1].Value = "故障現況";
                sheet.Cells[row++, 1].Value = "說明";
                sheet.Cells[row++, 1].Value = "需總經理審核";
                sheet.Cells[row++, 1].Value = "總經理審核結果";
                sheet.Cells[row++, 1].Value = "總經理審核說明";
                sheet.Cells[row++, 1].Value = "需副總審核";
                sheet.Cells[row++, 1].Value = "副總審核結果";
                sheet.Cells[row++, 1].Value = "副總審核說明";
                sheet.Cells[row++, 1].Value = "送審原因";
                sheet.Cells[row++, 1].Value = "申請時間";

                int col = 2;
                row = 1;
                sheet.Cells[row++, col].Value = repairInfo.ID;
                sheet.Column(col).AutoFit();
                sheet.Cells[row++, col].Value = repairInfo.CreaterName;
                sheet.Column(col).AutoFit();
                sheet.Cells[row++, col].Value = repairInfo.IsRevoke ? "撤銷" : "";
                sheet.Column(col).AutoFit();
                sheet.Cells[row++, col].Value = repairInfo.FactoryTitle;
                sheet.Column(col).AutoFit();
                sheet.Cells[row++, col].Value = repairInfo.FloorTitle;
                sheet.Column(col).AutoFit();
                sheet.Cells[row++, col].Value = repairInfo.AreaTitle;
                sheet.Column(col).AutoFit();
                sheet.Cells[row++, col].Value = repairInfo.DeviceName;
                sheet.Column(col).AutoFit();
                sheet.Cells[row++, col].Value = EnumHelper.GetDescription((FaultState)repairInfo.FaultState);
                sheet.Column(col).AutoFit();
                sheet.Cells[row++, col].Value = repairInfo.Description;
                sheet.Column(col).AutoFit();
                sheet.Cells[row++, col].Value = repairInfo.IsPresApproval ? "須審核" : "";
                sheet.Column(col).AutoFit();
                sheet.Cells[row++, col].Value = GetAppStatus(repairInfo.PresAppStatus);
                sheet.Column(col).AutoFit();
                sheet.Cells[row++, col].Value = repairInfo.PresResult;
                sheet.Column(col).AutoFit();
                sheet.Cells[row++, col].Value = repairInfo.IsViceApproval ? "須審核" : "";
                sheet.Column(col).AutoFit();
                sheet.Cells[row++, col].Value = GetAppStatus(repairInfo.ViceAppStatus);
                sheet.Column(col).AutoFit();
                sheet.Cells[row++, col].Value = repairInfo.ViceResult;
                sheet.Column(col).AutoFit();
                sheet.Cells[row++, col].Value = repairInfo.SubmitReason;
                sheet.Column(col).AutoFit();
                sheet.Cells[row++, col].Value = repairInfo.CreateDate.ToString("yyyy/HH/mm HH:mm");
                sheet.Column(col).AutoFit();


                //因為ep.Stream是加密過的串流，故要透過SaveAs將資料寫到MemoryStream，在將MemoryStream使用FileStreamResult回傳到前端
                MemoryStream fileStream = new MemoryStream();
                ep.SaveAs(fileStream);
                ep.Dispose();//如果這邊不下Dispose，建議此ep要用using包起來，但是要記得先將資料寫進MemoryStream在Dispose。
                fileStream.Position = 0;//不重新將位置設為0，excel開啟後會出現錯誤

                return File(fileStream, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", DateTime.Now.ToString("yyyy-MM-dd HH:mm") + "報修資料.xlsx");
            }
            catch (Exception e)
            {

                throw;
            }
        }

        [HttpPost]
        public JsonResult GetChart(string type, int factoryId, int floorId, int areaId, int deviceId, DateTime StartDate, DateTime EndDate)
        {
            var query = repairRepository.GetNumAndPercentage(StartDate, EndDate, factoryId, floorId, areaId, deviceId, type);
            foreach (var data in query)
            {
                var eq = equipmentRepository.GetInfoById(Convert.ToInt32(data.Title));
                data.Title = eq == null ? "其他" : eq.Title;
            }

            return Json(query.OrderBy(q => q.Title), JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// 複數圖表
        /// </summary>
        /// <param name="type"></param>
        /// <param name="factoryId"></param>
        /// <param name="floorId"></param>
        /// <param name="areaId"></param>
        /// <param name="deviceId"></param>
        /// <param name="StartDate"></param>
        /// <param name="EndDate"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult GetMultipleChart(string type, int factoryId, int floorId, int areaId, int deviceId, DateTime StartDate, DateTime EndDate)
        {
            MultipleChartModel response = new MultipleChartModel();
            //response.FinishedDataList = repairRepository.GetNumAndPercentageQuery(StartDate, EndDate, factoryId, floorId, areaId, deviceId, type, true).ToList();
            //response.UnFinishedDataList = repairRepository.GetNumAndPercentageQuery(StartDate, EndDate, factoryId, floorId, areaId, deviceId, type, false).ToList();
            response.FinishedList = repairRepository.GetNumAndPercentage(StartDate, EndDate, factoryId, floorId, areaId, deviceId, type, true).ToList();
            response.UnFinishedList = repairRepository.GetNumAndPercentage(StartDate, EndDate, factoryId, floorId, areaId, deviceId, type, false).ToList();
            int parentId = 0;
            int thisClass = 1;
            if (type == "floor")
            {
                parentId = factoryId;
                thisClass = 2;
            }
            else if (type == "area")
            {
                parentId = floorId;
                thisClass = 3;
            }
            else if (type == "device")
            {
                parentId = areaId;
                thisClass = 4;
            }

            var equipmentList = equipmentRepository.Query(true, parentId, thisClass);
            foreach (var item in equipmentList)
            {
                var finishedEqui = response.FinishedList.Where(q => q.Title == item.ID.ToString()).FirstOrDefault();
                if (finishedEqui == null)
                {
                    response.FinishedList.Add(new RepairRepository.NumAndPercentageModel()
                    {
                        Title = item.Title,
                        Num = 0,
                        CateCount = 0,
                        AllCount = 0
                    });
                }
                else
                {
                    finishedEqui.Title = item.Title;
                }

                var unFinishedEqui = response.UnFinishedList.Where(q => q.Title == item.ID.ToString()).FirstOrDefault();
                if (unFinishedEqui == null)
                {
                    response.UnFinishedList.Add(new RepairRepository.NumAndPercentageModel()
                    {
                        Title = item.Title,
                        Num = 0,
                        CateCount = 0,
                        AllCount = 0
                    });
                }
                else
                {
                    unFinishedEqui.Title = item.Title;
                }
            }
            //foreach (var data in response.FinishedList)
            //{
            //    int id = Convert.ToInt32(data.Title);
            //    var eq = equipmentRepository.GetInfoById(id);
            //    data.Title = eq == null ? "其他" : eq.Title;
            //}
            //foreach (var data in response.UnFinishedList)
            //{
            //    int id = Convert.ToInt32(data.Title);
            //    var eq = equipmentRepository.GetInfoById(id);
            //    data.Title = eq == null ? "其他" : eq.Title;
            //}

            response.FinishedList = response.FinishedList.OrderBy(q => q.Title).ToList();
            response.UnFinishedList = response.UnFinishedList.OrderBy(q => q.Title).ToList();

            return Json(response, JsonRequestBehavior.AllowGet);
        }

        private static string GetAppStatus(int AppStatus)
        {
            switch (AppStatus)
            {
                case 0:
                    return "無須審核";
                case 1:
                    return "待審";
                case 2:
                    return "核准";
                case 3:
                    return "駁回";
            }
            return "";
        }

        private class MultipleChartModel
        {
            public List<RepairRepository.NumAndPercentageModel> FinishedList { get; set; }
            public List<Repair> FinishedDataList { get; set; }
            public List<RepairRepository.NumAndPercentageModel> UnFinishedList { get; set; }
            public List<Repair> UnFinishedDataList { get; set; }
        }
    }
}