﻿using AutoMapper;
using FairlyRepair.ActionFilters;
using FairlyRepair.Areas.Admin.ViewModels.Manager;
using FairlyRepair.Models;
using FairlyRepair.Models.Others;
using FairlyRepair.Repositories;
using FairlyRepair.Utility.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FairlyRepair.Areas.Admin.Controllers
{
    [Authorize]
    [ErrorHandleAdminActionFilter]
    public class ManagerAdminController : BaseAdminController
    {
        private AdminRepository adminRepository;

        public ManagerAdminController() : this(null) { }

        public ManagerAdminController(AdminRepository repo)
        {
            adminRepository = repo ?? new AdminRepository(new FairlyRepairDBEntities());
        }

        // GET: Admin/ManagerAdmin
        public ActionResult Index(ManagerIndexView model)
        {
            var adminInfo = AdminInfoHelper.GetAdminInfo();
            if (adminInfo.Type != 1)
            {
                return RedirectToAction("Edit", new { id = adminInfo.ID });
            }
            var thisAcc = adminInfo.Account;
            var admintype = adminRepository.GetAll().Where(a => a.Account == thisAcc).FirstOrDefault().Type;
            var query = adminRepository.Query(model.Status, model.Type, model.Account);
            if (admintype != 1)
            {
                query = query.Where(a => a.Type == 2);
            }
            var pageResult = query.ToPageResult<Models.Admin>(model);
            model.PageResult = Mapper.Map<PageResult<ManagerView>>(pageResult);
            return View(model);
        }

        public ActionResult Edit(int id = 0)
        {
            var adminInfo = AdminInfoHelper.GetAdminInfo();
            if (adminInfo.Type > 1 && adminInfo.ID != id)
            {
                return RedirectToAction("Edit", new { id = adminInfo.ID});
            }
            ManagerView model;
            if (id == 0)
            {
                model = new ManagerView();
                model.Type = (int)AdminType.Applicant;
            }
            else
            {
                var query = adminRepository.GetById(id);
                model = Mapper.Map<ManagerView>(query);
            }
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public ActionResult Edit(ManagerView model)
        {
            if (ModelState.IsValid)
            {
                Models.Admin admin = Mapper.Map<Models.Admin>(model);
                admin.UpdateDate = DateTime.Now;
                admin.Updater = 1;
                int accountCount = adminRepository.GetAll().Where(a => a.Account == model.Account).Count();
                //var adminInfo = AdminInfoHelper.GetAdminInfo();
                if (accountCount > 0 && model.ID == 0)
                {                  
                    ShowMessage(false, "該帳號已被使用，請重新輸入");
                    return View(model);                 
                }
                if (model.ID == 0)
                {
                    admin.CreateDate = DateTime.Now;
                    admin.Creater = 1;
                    model.ID = adminRepository.Insert(admin);                    
                }
                else
                {
                    adminRepository.Update(admin);
                }
                ShowMessage(true, "");
                return RedirectToAction(nameof(ManagerAdminController.Edit), new { id = model.ID });
            }

            ShowMessage(false, "輸入資料有誤");
            return View(model);
        }

        public ActionResult Delete(int id)
        {
            Models.Admin admin = adminRepository.GetById(id);
            adminRepository.Delete(admin.ID);
            ShowMessage(true, "Data deleted.");
            return RedirectToAction(nameof(ManagerAdminController.Index));
        }

    }
}