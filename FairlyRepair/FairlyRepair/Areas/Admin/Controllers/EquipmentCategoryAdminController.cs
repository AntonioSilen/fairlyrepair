﻿using AutoMapper;
using FairlyRepair.Models.Others;
using FairlyRepair.Repositories.T8Repositories;
using FairlyRepair.Models;
using FairlyRepair.Repositories;
using FairlyRepair.Repositories.T8Repositories;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FairlyRepair.Utility.Helpers;
using FairlyRepair.Areas.Admin.ViewModels.EquipmentCategory;
using FairlyRepair.ActionFilters;

namespace FairlyRepair.Areas.Admin.Controllers
{
    [Authorize]
    [ErrorHandleAdminActionFilter]
    public class EquipmentCategoryAdminController : BaseAdminController
    {
        private PersonRepository personRepository = new PersonRepository(new T8ERPEntities());
        private EquipmentCategoryRepository equipmentCategoryRepository = new EquipmentCategoryRepository(new FairlyRepairDBEntities());
        private RepairRepository repairRepository = new RepairRepository(new FairlyRepairDBEntities());
        private RepairImageRepository repairImageRepository = new RepairImageRepository(new FairlyRepairDBEntities());

        public ActionResult Index(EquipmentCategoryIndexView model)
        {       
            var query = equipmentCategoryRepository.Query(model.IsOnline);
            var pageResult = query.ToPageResult<EquipmentCategory>(model);
            model.PageResult = Mapper.Map<PageResult<EquipmentCategoryView>>(pageResult);

            return View(model);
        }

        public ActionResult Edit(int id = 0, int Parent = 0, int Class = 0)
        {           
            var model = new EquipmentCategoryView();
          
            if (id != 0)
            {
                var query = equipmentCategoryRepository.FindBy(id);
                model = Mapper.Map<EquipmentCategoryView>(query);
            }          

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public ActionResult Edit(EquipmentCategoryView model)
        {
            if (ModelState.IsValid)
            {
                EquipmentCategory data = Mapper.Map<EquipmentCategory>(model);
                int adminId = AdminInfoHelper.GetAdminInfo().ID;

                if (model.ID == 0)
                {
                    model.ID = equipmentCategoryRepository.Insert(data);
                    ShowMessage(true, "新增成功");
                }
                else
                {
                    equipmentCategoryRepository.Update(data);
                    ShowMessage(true, "修改成功");
                }

                return RedirectToAction("Edit", new { id = model.ID });
            }

            return View(model);
        }

        //public ActionResult Delete(int id)
        //{
        //    //DeleteImage(id, 1);

        //    equipmentRepository.Delete(id);
        //    ShowMessage(true, "刪除成功");
        //    return RedirectToAction("Index");
        //}
    }
}