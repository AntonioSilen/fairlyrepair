﻿using AutoMapper;
using FairlyRepair.ActionFilters;
using FairlyRepair.Areas.Admin.ViewModels.Manufacture;
using FairlyRepair.Models;
using FairlyRepair.Models.Others;
using FairlyRepair.Repositories;
using FairlyRepair.Utility.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FairlyRepair.Areas.Admin.Controllers
{
    [Authorize]
    [ErrorHandleAdminActionFilter]
    public class ManufactureAdminController : BaseAdminController
    {
        private ManufactureRepository manufactureRepository = new ManufactureRepository(new FairlyRepairDBEntities());

        public ActionResult Index(ManufactureIndexView model)
        {         
            var query = manufactureRepository.Query(model.IsOnline);
            var pageResult = query.ToPageResult<Manufacture>(model);
            model.PageResult = Mapper.Map<PageResult<ManufactureView>>(pageResult);

            return View(model);
        }

        public ActionResult Edit(int id = 0, int RepairID = 0)
        {
            var model = new ManufactureView();
            if (id != 0)
            {
                var query = manufactureRepository.FindBy(id);
                model = Mapper.Map<ManufactureView>(query);
            }
            else
            {
                model.IsOnline = false;
            }

            model.Type = 1;

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public ActionResult Edit(ManufactureView model, List<HttpPostedFileBase> pics)
        {
            if (ModelState.IsValid)
            {
                Manufacture data = Mapper.Map<Manufacture>(model);
                int adminId = AdminInfoHelper.GetAdminInfo().ID;

                if (model.ID == 0)
                {
                    model.ID = manufactureRepository.Insert(data);
                    ShowMessage(true, "新增成功");
                }
                else
                {
                    manufactureRepository.Update(data);
                    ShowMessage(true, "修改成功");
                }

                System.IO.Directory.CreateDirectory(Server.MapPath("/FileUploads/Qrcode") + "/" + model.ID);

                return RedirectToAction("Edit", new { id = model.ID });
            }

            return View(model);
        }

        public ActionResult Delete(int id)
        {
            manufactureRepository.Delete(id);
            ShowMessage(true, "刪除成功");
            return RedirectToAction("Index");
        }
    }
}