﻿using AutoMapper;
using FairlyRepair.ActionFilters;
using FairlyRepair.Areas.Admin.ViewModels.ProductionType;
using FairlyRepair.Models.FAIRLYVIN;
using FairlyRepair.Models.Others;
using FairlyRepair.Utility.Helpers;
using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using static FairlyRepair.Repositories.OracleRepositories.RepositorySetting;

namespace FairlyRepair.Areas.Admin.Controllers
{
    [Authorize]
    [ErrorHandleAdminActionFilter]
    public class ProductionTypeAdminController : BaseAdminController
    {
        // GET: Admin/ProductionTypeAdmin
        public ActionResult Index(ProductionTypeIndexView model)
        {
            var adminInfo = AdminInfoHelper.GetAdminInfo();

            var query = TypeQuery().AsQueryable();
            var pageResult = query.ToPageResult<W_DAYTYPE>(model);
            model.PageResult = Mapper.Map<PageResult<ProductionTypeView>>(pageResult);

            model.CateOptions = new List<SelectListItem>();
            var typeQuery = TypeQuery();

            model.CateOptions.Add(new SelectListItem()
            {
                Value = "0001",
                Text = "項目"
            });
            model.CateOptions.Add(new SelectListItem()
            {
                Value = "0002",
                Text = "客戶"
            });

            return View(model);
        }

        public ActionResult Edit(string type_num = "0000")
        {
            var adminInfo = AdminInfoHelper.GetAdminInfo();

            ProductionTypeView model = new ProductionTypeView();
            if (string.IsNullOrEmpty(type_num) || type_num == "0000")
            {
                model = new ProductionTypeView();
                model.TYPE_NUM = type_num;
            }
            else
            {
                var query = GetType(type_num);
                model = Mapper.Map<ProductionTypeView>(query);
            }

            model.CateOptions = new List<SelectListItem>();
            var typeQuery = TypeQuery();
           
            model.CateOptions.Add(new SelectListItem()
            {
                Value = "0001",
                Text = "項目"
            });
            model.CateOptions.Add(new SelectListItem()
            {
                Value = "0002",
                Text = "客戶"
            });
         

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public ActionResult Edit(ProductionTypeView model)
        {
            if (ModelState.IsValid)
            {
                OracleConnection oralceConnection = new OracleConnection(g_OracleERPConnectionString);
                oralceConnection.Open();
                var adminInfo = AdminInfoHelper.GetAdminInfo();
                W_DAYTYPE data = Mapper.Map<W_DAYTYPE>(model);
                if (string.IsNullOrEmpty(model.TYPE_NUM) || model.TYPE_NUM == "0000")
                {
                    model.TYPE_NUM = (Convert.ToInt32(TypeQuery().OrderByDescending(q => q.TYPE_NUM).FirstOrDefault().TYPE_NUM) + 1).ToString("0000");
                    InsertType(model, oralceConnection);
                }
                else
                {
                    UpdateType(model, oralceConnection);
                }
                ShowMessage(true, "");
                return RedirectToAction(nameof(ProductionTypeAdminController.Edit), new { type_num = model.TYPE_NUM });
            }

            ShowMessage(false, "輸入資料有誤");
            return View(model);
        }

        //public ActionResult Delete(int id)
        //{
        //    W_DAYTYPE data = eightDCategoryRepository.GetById(id);
        //    eightDCategoryRepository.Delete(data.ID);
        //    ShowMessage(true, "Data deleted.");
        //    return RedirectToAction(nameof(ProductionTypeAdminController.Index));
        //}

        public List<W_DAYTYPE> TypeQuery()
        {
            OracleConnection oralceConnection = new OracleConnection(g_OracleERPConnectionString);
            oralceConnection.Open();
            string sltSql = "select *" +
                                 " from W_DAYTYPE t";
            OracleCommand sltCmd = new OracleCommand(sltSql, oralceConnection);
            sltCmd.CommandType = CommandType.Text;

            OracleDataAdapter DataAdapter = new OracleDataAdapter();
            DataAdapter.SelectCommand = sltCmd;
            DataSet ds = new DataSet();
            DataAdapter.Fill(ds, "Table");
            DataTable dt = ds.Tables["Table"];

            var queryData = new List<W_DAYTYPE>();
            if (dt.Rows.Count > 0)
            {
                queryData = CommonMethod.ConvertToList<W_DAYTYPE>(dt).ToList();
            }

            oralceConnection.Close();
            oralceConnection.Dispose();
            return queryData;
        }

        private W_DAYTYPE GetType(string type_num)
        {
            // 連線資料庫
            OracleConnection oralceConnection = new OracleConnection(g_OracleERPConnectionString);
            oralceConnection.Open();

            string sql = @"select t.* from W_DAYTYPE t
                                    where type_num = :TYPE_NUM";

            OracleCommand cmd = new OracleCommand(sql, oralceConnection);
            cmd.CommandType = CommandType.Text;

            if (!string.IsNullOrEmpty(type_num))
            {
                cmd.Parameters.Add(":TYPE_NUM", type_num);
            }

            OracleDataAdapter DataAdapter = new OracleDataAdapter();
            DataAdapter.SelectCommand = cmd;
            DataSet ds = new DataSet();
            DataAdapter.Fill(ds, "Table");
            DataTable dt = ds.Tables["Table"];
            if (dt.Rows.Count > 0)
            {
                W_DAYTYPE data = new W_DAYTYPE();
                data = CommonMethod.ConvertToList<W_DAYTYPE>(dt).ToList().FirstOrDefault();
                oralceConnection.Close();
                oralceConnection.Dispose();

                return data;
            }

            oralceConnection.Close();
            oralceConnection.Dispose();

            return null;
        }

        private static string InsertType(ProductionTypeView model, OracleConnection oralceConnection)
        {
            if (!string.IsNullOrEmpty(model.TYPE_NUM) && !string.IsNullOrEmpty(model.TYPE_NAME))
            {
                try
                {
                    //新增
                    string sql = $@"INSERT INTO W_DAYTYPE
                                    (TYPE_NUM, 
                                    TYPE_NAME, 
                                    TYPE_CATE)
                                    VALUES
                                    (:TYPE_NUM,                                   
                                    :TYPE_NAME,
                                    :TYPE_CATE
                                    )";
                    OracleCommand cmd = new OracleCommand(sql, oralceConnection);
                    cmd.CommandType = CommandType.Text;
                    cmd.Parameters.Add(":TYPE_NUM", model.TYPE_NUM);
                    cmd.Parameters.Add(":TYPE_NAME", model.TYPE_NAME);
                    cmd.Parameters.Add(":TYPE_CATE", model.TYPE_CATE);

                    cmd.ExecuteNonQuery();

                    oralceConnection.Close();
                    oralceConnection.Dispose();
                }
                catch (Exception e)
                {
                    oralceConnection.Close();
                    oralceConnection.Dispose();
                    return "0000";
                }

                return model.TYPE_NUM;
            }
            return "0000";
        }

        private static void UpdateType(ProductionTypeView model, OracleConnection oralceConnection)
        {
            if (!string.IsNullOrEmpty(model.TYPE_NUM) && !string.IsNullOrEmpty(model.TYPE_NAME))
            {
                //更新
                string sql = "Update W_DAYTYPE ";
                sql += " set TYPE_NAME = :TYPE_NAME, ";//項目代碼
                sql += " TYPE_CATE = :TYPE_CATE ";//項目代碼
                sql += " where  TYPE_NUM = :TYPE_NUM";
                OracleCommand cmd = new OracleCommand(sql, oralceConnection);
                cmd.CommandType = CommandType.Text;
                cmd.Parameters.Add(":TYPE_NAME", model.TYPE_NAME);
                cmd.Parameters.Add(":TYPE_CATE", model.TYPE_CATE);
                cmd.Parameters.Add(":TYPE_NUM", model.TYPE_NUM);

                cmd.ExecuteNonQuery();
            }
        }

        //private static bool DeleteType(W_DAYN_PIC data, OracleConnection oralceConnection)
        //{
        //    if (!string.IsNullOrEmpty(data.DIARYDATE) && !string.IsNullOrEmpty(data.LIN_PSCT))
        //    {
        //        try
        //        {
        //            //新增
        //            string sql = $@"delete W_DAYTYPE
        //                         where diarydate = :DIARYDATE and lin_psct = :LIN_PSCT";
        //            OracleCommand cmd = new OracleCommand(sql, oralceConnection);
        //            cmd.ExecuteNonQuery();
        //            cmd.Parameters.Add(":DIARYDATE", data.DIARYDATE);
        //            cmd.Parameters.Add(":LIN_PSCT", data.LIN_PSCT);

        //            oralceConnection.Close();
        //            oralceConnection.Dispose();
        //        }
        //        catch (Exception e)
        //        {
        //            oralceConnection.Close();
        //            oralceConnection.Dispose();
        //            return false;
        //        }

        //        return true;
        //    }
        //    return false;
        //}
    }
}