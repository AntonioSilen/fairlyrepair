﻿using AutoMapper;
using FairlyRepair.ActionFilters;
using FairlyRepair.Areas.Admin.ViewModels.Replace;
using FairlyRepair.Models;
using FairlyRepair.Models.Others;
using FairlyRepair.Repositories;
using FairlyRepair.Repositories.T8Repositories;
using FairlyRepair.Utility.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FairlyRepair.Areas.Admin.Controllers
{
    [Authorize]
    [ErrorHandleAdminActionFilter]
    public class ReplaceAdminController : BaseAdminController
    {
        private PersonRepository personRepository = new PersonRepository(new T8ERPEntities());
        private ReplaceRepository replaceRepository = new ReplaceRepository(new FairlyRepairDBEntities());
        private DeviceRepository deviceRepository = new DeviceRepository(new FairlyRepairDBEntities());

        public ActionResult Index(ReplaceIndexView model)
        {
            var query = replaceRepository.Query(model.IsPresApproval, model.IsViceApproval,
                model.PresAppStatus, model.ViceAppStatus,
                model.DeviceID, model.FactoryID);
            var pageResult = query.ToPageResult<Replace>(model);
            model.PageResult = Mapper.Map<PageResult<ReplaceView>>(pageResult);

            return View(model);
        }

        public ActionResult Edit(int id = 0)
        {
            var model = new ReplaceView();

            if (id != 0)
            {
                var query = replaceRepository.FindBy(id);
                model = Mapper.Map<ReplaceView>(query);
            }
            else
            {
                model.ExpectDate = DateTime.Now.AddMonths(1);
                model.CompletionDate = DateTime.Now.AddMonths(2);
            }
            model.DeviceType = 1;

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public ActionResult Edit(ReplaceView model/*, List<HttpPostedFileBase>pics*/)
        {
            if (ModelState.IsValid)
            {
                Replace data = Mapper.Map<Replace>(model);
                var adminInfo = AdminInfoHelper.GetAdminInfo();
                int adminId = adminInfo.ID;
                data.Updater = adminId;

                if (model.ID == 0)
                {
                    if (adminInfo.Type == (int)AdminType.Dispatcher)
                    {
                        data.PresAppStatus = model.IsPresApproval ? 1 : 0;
                        data.ViceAppStatus = model.IsViceApproval ? 1 : 0;
                    }

                    data.Creater = adminId;
                    model.ID = replaceRepository.Insert(data);
                    ShowMessage(true, "新增成功");
                }
                else
                {
                    var oldData = replaceRepository.FindBy(model.ID);

                    if (adminInfo.Type == (int)AdminType.Dispatcher)
                    {
                        if (!oldData.IsPresApproval && model.IsPresApproval)
                        {
                            data.PresAppStatus = 1;
                        }
                        if (!oldData.IsViceApproval && model.IsViceApproval)
                        {
                            data.ViceAppStatus = 1;
                        }
                    }

                    #region 通知
                    int repairType = 2;//報修類型
                    IdentifyNotifyType(model, adminInfo, oldData, repairType);
                    #endregion

                    replaceRepository.Update(data);
                    ShowMessage(true, "修改成功");
                }

                return RedirectToAction("Edit", new { id = model.ID });
            }

            return View(model);
        }

        RepairAdminController repairAdmin = new RepairAdminController();
        /// <summary>
        /// 分辨Line Notify 訊息類型及對象
        /// </summary>
        /// <param name="model">新資料</param>
        /// <param name="adminInfo">發送人層級</param>
        /// <param name="oldData">原資料</param>
        /// <param name="repairType">1.報修  2.新增設備</param>
        private void IdentifyNotifyType(ReplaceView model, AdminInfo adminInfo, Replace oldData, int repairType)
        {
            string urlMain = $"{Request.Url.Scheme}://{Request.Url.Authority}/Admin";
            switch (adminInfo.Type)
            {
                case (int)AdminType.Dispatcher:
                    //通知總經理
                    if (!oldData.IsPresApproval && model.IsPresApproval)
                    {
                        repairAdmin.SendLineNotify(model.ID, repairType, (int)AdminType.President, false, false, urlMain);
                    }
                    //通知副總
                    if (!oldData.IsViceApproval && model.IsViceApproval)
                    {
                        repairAdmin.SendLineNotify(model.ID, repairType, (int)AdminType.VicePresident, false, false, urlMain);
                    }
                    break;
                case (int)AdminType.President:
                    //通知調度員
                    if (oldData.IsPresApproval && oldData.PresAppStatus != model.PresAppStatus)
                    {
                        repairAdmin.SendLineNotify(model.ID, repairType, (int)AdminType.Dispatcher, true, false, urlMain);
                    }
                    break;
                case (int)AdminType.VicePresident:
                    //通知調度員
                    if (oldData.IsViceApproval && oldData.ViceAppStatus != model.ViceAppStatus)
                    {
                        repairAdmin.SendLineNotify(model.ID, repairType, (int)AdminType.Dispatcher, false, true, urlMain);
                    }
                    break;
            }
        }

        public ActionResult Delete(int id)
        {
            //DeleteImage(id, 1);

            replaceRepository.Delete(id);
            ShowMessage(true, "刪除成功");
            return RedirectToAction("Index");
        }
        
        public ActionResult SendNotify(int mid)
        {
            try
            {
                var repairInfo = Mapper.Map<ReplaceView>(replaceRepository.FindBy(mid));
                if (repairInfo == null)
                {
                    ShowMessage(true, "發送失敗，查無會議資料");
                }
                //var reciverList = ;
                //foreach (var item in reciverList)
                //{
                //    var thisSignIn = notifyList.Where(q => q.JobID == item.JobID).FirstOrDefault();

                //    //生成Qrcode圖片
                //    string host = $"{Request.Url.Scheme}://{Request.Url.Authority}";
                //    string fileName = $"/{repairInfo.ID}_{item.JobID}.png";
                //    string qrcodeUrl = thisSignIn.SignCode;
                //    string savePath = Server.MapPath("/FileUploads/Qrcode") + $"/{repairInfo.ID}/{fileName}";
                //    QRCodeHelper.GeneratorQrCodeImage(qrcodeUrl, savePath);

                //    //寄送Email
                //    //string to = "seelen12@fairlybike.com";
                //    string to = item.Email;
                //    string subject = "";
                //    string mailBody = "";
                //    mailBody = $"<h3>【{repairInfo.Title}】 {repairInfo.StartDate.ToString("yyyy/MM/dd HH:mm")} 於{repairInfo.RoomStr}</h3>" +
                //                                $"<p>發起人 : {repairInfo.Organiser}</p>" +
                //                                $"<p>通知人 : 「{item.DepartmentStr}」 {item.Name + item.EngName}</p>";

                //    mailBody += $"<p>預定開始時間 : {repairInfo.StartDate.ToString("yyyy/MM/dd HH:mm")}</p>" +
                //                                $"<p>預定結束時間 : {repairInfo.EndDate.ToString("yyyy/MM/dd HH:mm")}</p>";

                //    subject = $"會議通知 - 系統信件";

                //    var calendar = new Ical.Net.Calendar();

                //    DateTime today = DateTime.Now;
                //    calendar.Events.Add(new Ical.Net.CalendarComponents.CalendarEvent
                //    {
                //        Class = "PUBLIC",
                //        Summary = subject,
                //        Created = new CalDateTime(today),
                //        Description = $"【{repairInfo.Title}】會議通知",
                //        Start = new CalDateTime(repairInfo.StartDate),
                //        End = new CalDateTime(repairInfo.EndDate),
                //        Sequence = 0,
                //        Uid = Guid.NewGuid().ToString(),
                //        Location = repairInfo.RoomStr,
                //    });

                //    var serializer = new CalendarSerializer(new SerializationContext());
                //    var serializedCalendar = serializer.SerializeToString(calendar);
                //    var bytesCalendar = Encoding.UTF8.GetBytes(serializedCalendar);
                //    MemoryStream ms = new MemoryStream(bytesCalendar);

                //    MailHelper.POP3Mail(to, subject, mailBody, ms);

                //    //新增報修紀錄

                //}

                ShowMessage(true, "通知發送成功");
            }
            catch (Exception ex)
            {
                ShowMessage(true, "發送失敗，錯誤訊息 : " + ex.Message);
            }
            return RedirectToAction("Edit", "RepairAdmin", new { id = mid });
        }

        public JsonResult GetDeviceItem(int deviceId)
        {
            var query = deviceRepository.Query((int)DeviceClass.Third, deviceId);
            //result格式未處理，須為 {"XXX":[{"label":"A", "value":1},{"label":"B","value":2},{"label":"C","value":3}]} 
            var resultList = query.ToList();
            return Json(resultList);
        }
    }
}