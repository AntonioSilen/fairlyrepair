﻿using AutoMapper;
using FairlyRepair.ActionFilters;
using FairlyRepair.Areas.Admin.ViewModels.AuditLevel;
//using FairlyRepair.Models;
using FairlyRepair.MeetModels;
using FairlyRepair.Models.Others;
using FairlyRepair.Repositories.MeetRepositories;
using FairlyRepair.Utility.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FairlyRepair.Areas.Admin.Controllers
{
    [Authorize]
    //[ErrorHandleAdminActionFilter]
    public class AuditLevelAdminController : BaseAdminController
    {
        private AuditLevelRepository auditLevelRepository = new AuditLevelRepository(new MeetingSignInDBEntities());
        private Repositories.T8Repositories.DepartmentRepository t8departmentRepository = new Repositories.T8Repositories.DepartmentRepository(new Models.T8ERPEntities());
        private Repositories.T8Repositories.PersonRepository t8personRepository = new Repositories.T8Repositories.PersonRepository(new Models.T8ERPEntities());

        public ActionResult Index(AuditLevelIndexView model)
        {
            var existDeptList = t8personRepository.GetDeptQuery();
            //var deptIdList = existDeptList.Select(q => q.DeptId).ToList();
            var query = auditLevelRepository.Query(model.IsOnline, model.PresidentReview, model.ViceReview, model.ManagerReview);
            foreach (var item in existDeptList)
            {
                if (auditLevelRepository.GetAll().Where(q => q.DeptId == item.DeptId).FirstOrDefault() == null)
                {
                    auditLevelRepository.Insert(new AuditLevel()
                    {
                        DeptId = item.DeptId,
                        DeptName = item.DeptName,
                        PresidentReview = "0000",
                        ViceReview = "0000",
                        ManagerReview = "0000",
                        IsOnline = true,
                        UpdateDate = DateTime.Now,
                    });
                }
            }
         
            var pageResult = query.ToPageResult<AuditLevel>(model);
            model.PageResult = Mapper.Map<PageResult<AuditLevelView>>(pageResult);
            foreach (var item in model.PageResult.Data)
            {
                var presidentData = t8personRepository.FindByJobID(item.PresidentReview);
                item.PresidentReview = presidentData == null ? "---" : presidentData.Name;
                var viceData = t8personRepository.FindByJobID(item.ViceReview);
                item.ViceReview = viceData == null ? "---" : viceData.Name;
                var managerData = t8personRepository.FindByJobID(item.ManagerReview);
                item.ManagerReview = managerData == null ? "---" : managerData.Name;
            }

            return View(model);
        }

        public ActionResult Edit(int id = 0)
        {
            var model = new AuditLevelView();
            if (id != 0)
            {
                var query = auditLevelRepository.FindBy(id);
                model = Mapper.Map<AuditLevelView>(query);
            }
            else
            {

            }

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public ActionResult Edit(AuditLevelView model)
        {
            if (ModelState.IsValid)
            {
                AuditLevel data = Mapper.Map<AuditLevel>(model);

                if (model.ID == 0)
                {
                    model.ID = auditLevelRepository.Insert(data);
                    ShowMessage(true, "新增成功");
                }
                else
                {
                    auditLevelRepository.Update(data);
                    ShowMessage(true, "修改成功");
                }

                return RedirectToAction("Edit", new { id = model.ID });
            }

            return View(model);
        }
    }
}