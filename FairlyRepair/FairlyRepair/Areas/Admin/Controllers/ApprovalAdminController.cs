﻿using AutoMapper;
using FairlyRepair.ActionFilters;
using FairlyRepair.Areas.Admin.ViewModels.Approval;
using FairlyRepair.Areas.Admin.ViewModels.Repair;
using FairlyRepair.Areas.Admin.ViewModels.RepairRecord;
using FairlyRepair.Areas.Admin.ViewModels.Replace;
using FairlyRepair.Models;
using FairlyRepair.Models.Others;
using FairlyRepair.Repositories;
using FairlyRepair.Utility.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FairlyRepair.Areas.Admin.Controllers
{
    [Authorize]
    //[ErrorHandleAdminActionFilter]
    public class ApprovalAdminController : BaseAdminController
    {
        private RepairRepository repairRepository = new RepairRepository(new FairlyRepairDBEntities());
        private RepairImageRepository repairImageRepository = new RepairImageRepository(new FairlyRepairDBEntities());
        private RepairRecordRepository repairRecordRepository = new RepairRecordRepository(new FairlyRepairDBEntities());
        private ReplaceRepository replaceRepository = new ReplaceRepository(new FairlyRepairDBEntities());
        // GET: Admin/ApprovalAdmin
        public ActionResult Index(ApprovalIndexView model)
        {
            var adminInfo = AdminInfoHelper.GetAdminInfo();
            var adminInfoType = adminInfo.Type;
            if (adminInfoType == (int)AdminType.President)
            {
                model.IsPresApproval = true;
            }
            else if (adminInfoType == (int)AdminType.VicePresident)
            {
                model.IsViceApproval = true;
            }
            else
            {
                return RedirectToAction("Index", "HomeAdmin");
            }

            var query1 = repairRepository.Query(model.IsPresApproval, model.IsViceApproval,
                model.PresAppStatus, model.ViceAppStatus,
                0, model.DeviceID, model.FactoryID, model.FloorID, model.AreaID, model.DeptID);
            var query2 = replaceRepository.Query(model.IsPresApproval, model.IsViceApproval,
                model.PresAppStatus, model.ViceAppStatus,
                model.DeviceID, model.FactoryID);
            var list1 = Mapper.Map<List<ApprovalView>>(query1);
            var list2 = Mapper.Map<List<ApprovalView>>(query2);
            var query = list1.Concat(list2).AsQueryable();

            var pageResult = query.ToPageResult<ApprovalView>(model);
            model.PageResult = Mapper.Map<PageResult<ApprovalView>>(pageResult);
            foreach (var item in model.PageResult.Data)
            {
                item.DeviceType = item.DeviceType == 0 ? 1 : item.DeviceType;
            }

            return View(model);
        }

        public ActionResult Edit(int id = 0)
        {
            var model = new RepairView();

            if (id == 0)
            {
                return RedirectToAction("Index");
            }
            var query = repairRepository.FindBy(id);
            model = Mapper.Map<RepairView>(query);
            model.PicList = repairImageRepository.Query(id).ToList();

            model.RecordList = Mapper.Map<List<RepairRecordView>>(repairRecordRepository.Query(null, id)).OrderByDescending(q => q.CreateDate).ToList();

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public ActionResult Edit(RepairView model/*, List<HttpPostedFileBase>pics*/)
        {
            var adminInfo = AdminInfoHelper.GetAdminInfo();
            if (ModelState.IsValid)
            {
                Repair data = Mapper.Map<Repair>(model);

                var oldData = repairRepository.FindBy(model.ID);

                repairRepository.Update(data);
                ShowMessage(true, "修改成功");

                RepairAdminController repairAdminController = new RepairAdminController();
                //repairAdminController.SendLineNotify(model.ID, 1, (int)AdminType.Dispatcher, true);

                switch (adminInfo.Type)
                {
                    case (int)AdminType.President:
                        //通知調度員
                        if (oldData.IsPresApproval && oldData.PresAppStatus != model.PresAppStatus)
                        {
                            repairAdminController.SendLineNotify(model.ID, 1, (int)AdminType.Dispatcher, true, false, $"{Request.Url.Scheme}://{Request.Url.Authority}/Admin");
                        }
                        break;
                    case (int)AdminType.VicePresident:
                        //通知調度員
                        if (oldData.IsViceApproval && oldData.ViceAppStatus != model.ViceAppStatus)
                        {
                            repairAdminController.SendLineNotify(model.ID, 1, (int)AdminType.Dispatcher, false, true, $"{Request.Url.Scheme}://{Request.Url.Authority}/Admin");
                        }
                        break;
                }

                return RedirectToAction("Edit", new { id = model.ID });
            }

            return View(model);
        }
        public ActionResult ReplaceEdit(int id = 0)
        {
            var model = new ReplaceView();

            if (id == 0)
            {
                return RedirectToAction("Index");
            }
            var query = replaceRepository.FindBy(id);
            model = Mapper.Map<ReplaceView>(query);

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public ActionResult ReplaceEdit(ReplaceView model/*, List<HttpPostedFileBase>pics*/)
        {
            if (ModelState.IsValid)
            {
                Replace data = Mapper.Map<Replace>(model);

                replaceRepository.Update(data);
                ShowMessage(true, "修改成功");

                return RedirectToAction("Edit", new { id = model.ID });
            }

            return View(model);
        }
    }
}