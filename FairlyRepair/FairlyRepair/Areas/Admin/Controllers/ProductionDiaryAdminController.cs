﻿using AutoMapper;
using FairlyRepair.ActionFilters;
using FairlyRepair.Areas.Admin.ViewModels.ProductionDiary;
using FairlyRepair.Models;
using FairlyRepair.Models.FAIRLYVIN;
using FairlyRepair.Models.Others;
using FairlyRepair.Utility.Helpers;
using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using static FairlyRepair.Repositories.OracleRepositories.RepositorySetting;

namespace FairlyRepair.Areas.Admin.Controllers
{
    [Authorize]
    [ErrorHandleAdminActionFilter]
    public class ProductionDiaryAdminController : BaseAdminController
    {
        [Authorize]
        // GET: Admin/ProductionDiary
        public ActionResult Index(ProductionDiaryIndexView model)
        {
            // 連線資料庫
            OracleConnection oralceConnection = new OracleConnection(g_OracleERPConnectionString);
            oralceConnection.Open();

            #region 查詢資料
            string sql = @"select t.* from w_dayn_note t ";
            if (!string.IsNullOrEmpty(model.StartDate) || !string.IsNullOrEmpty(model.EndDate))
            {
                if (model.StartDate == null)
                {
                    model.StartDate = Convert.ToDateTime(model.EndDate).AddMonths(-1).ToString("yyyy-MM-dd");
                }
                if (model.EndDate == null)
                {
                    model.EndDate = Convert.ToDateTime(model.StartDate).AddMonths(1).ToString("yyyy-MM-dd");
                }
                if (!string.IsNullOrEmpty(model.StartDate))
                {
                    sql += " and t.diarydate >= '" + model.StartDate.Replace("-", "") + "' ";
                }
                if (!string.IsNullOrEmpty(model.EndDate))
                {
                    sql += " and t.diarydate <= '" + model.EndDate.Replace("-", "") + "' ";
                }
            }
            if (model.Type.HasValue)
            {
                sql += " and t.type_num = '" + Convert.ToInt32(model.Type).ToString("0000") + "' ";
            }
            if (!string.IsNullOrEmpty(model.ENTER_USER))
            {
                sql += " and t.enter_user = '" + model.ENTER_USER + "' ";
            }
            if (!string.IsNullOrEmpty(model.COD_CUST))
            {
                sql += " and t.cod_cust = '" + model.COD_CUST + "' ";
            }
            if (!string.IsNullOrEmpty(model.NAM_ITEM))
            {
                sql += " and t.nam_item = '" + model.NAM_ITEM + "' ";
            }
            OracleCommand cmd = new OracleCommand(sql, oralceConnection);
            cmd.CommandType = CommandType.Text;
            // 使用參數化填值
            //if (!string.IsNullOrEmpty(model.StartDate) || !string.IsNullOrEmpty(model.EndDate))
            //{
            //    if (model.StartDate == null)
            //    {
            //        model.StartDate = Convert.ToDateTime(model.EndDate).AddMonths(-1).ToString("yyyy-MM-dd");
            //    }
            //    if (model.EndDate == null)
            //    {
            //        model.EndDate = Convert.ToDateTime(model.StartDate).AddMonths(1).ToString("yyyy-MM-dd");
            //    }
            //    if (!string.IsNullOrEmpty(model.StartDate))
            //    {
            //        cmd.Parameters.Add(":diarydate", model.StartDate.Replace("-", ""));
            //    }
            //    if (!string.IsNullOrEmpty(model.EndDate))
            //    {
            //        cmd.Parameters.Add(":diarydate", model.EndDate.Replace("-", ""));
            //    }
            //}

            //if (model.Type.HasValue)
            //{
            //    cmd.Parameters.Add(":type_num", Convert.ToInt32(model.Type).ToString("0000"));
            //}
            //if (!string.IsNullOrEmpty(model.ENTER_USER))
            //{
            //    cmd.Parameters.Add(":enter_user", model.ENTER_USER);
            //}
            //if (!string.IsNullOrEmpty(model.COD_CUST))
            //{
            //    cmd.Parameters.Add(":cod_cust", model.COD_CUST);
            //}
            //if (!string.IsNullOrEmpty(model.NAM_ITEM))
            //{
            //    cmd.Parameters.Add(":nam_item", model.NAM_ITEM);
            //}
            #endregion

            OracleDataAdapter DataAdapter = new OracleDataAdapter();
            DataAdapter.SelectCommand = cmd;
            DataSet ds = new DataSet();
            DataAdapter.Fill(ds, "Table");
            DataTable dt = ds.Tables["Table"];
            if (dt.Rows.Count > 0)
            {
                //model.QueryData = CommonMethod.ConvertToList<ProductionDiaryIndexView>(dt).ToList();

                var query = Mapper.Map<List<W_DAYN_NOTE>>(CommonMethod.ConvertToList<ProductionDiaryIndexView>(dt)).AsQueryable();
                var pageResult = query.ToPageResult<W_DAYN_NOTE>(model);
                model.PageResult = Mapper.Map<PageResult<ProductionDiaryView>>(pageResult);
            }
            else
            {
                model.PageResult = new PageResult<ProductionDiaryView>();
            }

            oralceConnection.Close();
            oralceConnection.Dispose();

            model.TypeOptions = new List<SelectListItem>();
            var typeQuery = TypeQuery();
            foreach (var item in typeQuery.Where(q => q.TYPE_CATE == "0001").OrderBy(q => q.TYPE_NUM))
            {
                model.TypeOptions.Add(new SelectListItem()
                {
                    Value = item.TYPE_NUM,
                    Text = item.TYPE_NAME
                });
            }

            model.CustOptions = new List<SelectListItem>();
            foreach (var item in typeQuery.Where(q => q.TYPE_CATE == "0002").OrderBy(q => q.TYPE_NUM))
            {
                model.CustOptions.Add(new SelectListItem()
                {
                    Value = item.TYPE_NUM,
                    Text = item.TYPE_NAME
                });
            }

            return View(model);
        }

        public ActionResult Edit(string dd, string user, string lin = "0000")
        {
            var adminInfo = AdminInfoHelper.GetAdminInfo();

            ProductionDiaryView model = new ProductionDiaryView();
            if (lin == "0000")
            {
                model = new ProductionDiaryView();
                model.ENTER_USER = adminInfo.Account;
                model.DIARYDATE = DateTime.Now.ToString("yyyyMMdd");
                //model.ENTER_USER = user;
            }
            else
            {
                var query = GetDiary(dd, user, lin);
                model = Mapper.Map<ProductionDiaryView>(query);
                model.FilesList = DiaryPicQuery(model.DIARYDATE, model.LIN_PSCT).ToList();

                if (model == null)
                {
                    ShowMessage(false, "找不到資料");
                    return RedirectToAction("Index");
                }
            }

            model.TypeOptions = new List<SelectListItem>();
            var typeQuery = TypeQuery();
            foreach (var item in typeQuery.Where(q => q.TYPE_CATE == "0001").OrderBy(q => q.TYPE_NUM))
            {
                model.TypeOptions.Add(new SelectListItem()
                {
                    Value = item.TYPE_NUM,
                    Text = item.TYPE_NAME
                });
            }

            model.CustOptions = new List<SelectListItem>();
            foreach (var item in typeQuery.Where(q => q.TYPE_CATE == "0002").OrderBy(q => q.TYPE_NUM))
            {
                model.CustOptions.Add(new SelectListItem()
                {
                    Value = item.TYPE_NUM,
                    Text = item.TYPE_NAME
                });
            }

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public ActionResult Edit(ProductionDiaryView model)
        {
            if (ModelState.IsValid)
            {
                var adminInfo = AdminInfoHelper.GetAdminInfo();
                OracleConnection oralceConnection = new OracleConnection(g_OracleERPConnectionString);
                oralceConnection.Open();
                try
                {

                    //#region 檔案處理
                    //bool hasFile = ImageHelper.CheckFileExists(model.PicFile);
                    //if (hasFile)
                    //{//檔案
                    //    ImageHelper.DeleteFile(PhotoFolder, model.DATE_PIC1);
                    //    model.DATE_PIC1 = ImageHelper.SaveFile(model.PicFile, PhotoFolder, model.PicFile.FileName.Split('.')[0] + "_" + DateTime.Now.ToString("yyyyMMddHHmm"));
                    //}
                    //#endregion

                    model.DIARYDATE = DateTime.Now.ToString("yyyyMMdd");
                    var dateCount = DiaryQuery(model.DIARYDATE).Count();
                    if (string.IsNullOrEmpty(model.LIN_PSCT))
                    {
                        if (dateCount <= 0)
                        {
                            model.LIN_PSCT = "0001";
                        }
                        else
                        {
                            model.LIN_PSCT = (dateCount + 1).ToString("0000");
                        }
                        model.LIN_PSCT = InsertDiary(model, oralceConnection);
                    }
                    else
                    {
                        if (adminInfo.Account != model.ENTER_USER && adminInfo.AccountType > (int)MeetAdminType.Employee)
                        {
                            ShowMessage(false, "儲存失敗，無修改權限");
                            return RedirectToAction(nameof(EightDAdminController.Edit), new { id = model.LIN_PSCT, dd = model.DIARYDATE });
                        }
                        UpdateDiary(model, oralceConnection);
                    }

                    #region 檔案處理
                    if (model.FilesFiles != null)
                    {
                        foreach (HttpPostedFileBase pic in model.FilesFiles)
                        {
                            bool hasFile = ImageHelper.CheckFileExists(pic);
                            if (hasFile)
                            {
                                W_DAYN_PIC file = new W_DAYN_PIC();
                                //ImageHelper.DeleteFile(PhotoFolder, model.FileOne);
                                file.DIARYDATE = model.DIARYDATE;
                                file.LIN_PSCT = model.LIN_PSCT;
                                file.PIC_NAME = ImageHelper.SaveFile(pic, PhotoFolder, pic.FileName.Split('.')[0] + "_" + DateTime.Now.ToString("yyyyMMddHHmm"));
                                InsertDiaryPic(file, oralceConnection);
                            }
                        }
                    }
                    #endregion
                    oralceConnection.Close();
                    oralceConnection.Dispose();
                }
                catch (Exception e)
                {
                    oralceConnection.Close();
                    oralceConnection.Dispose();
                    ShowMessage(false, e.Message);
                }
                ShowMessage(true, "儲存成功");
                return RedirectToAction(nameof(EightDAdminController.Edit), new { dd = model.DIARYDATE, user = adminInfo.Account, lin = model.LIN_PSCT });
            }
            else
            {
                var msg = ModelState.Values.Where(q => q.Errors.Count() > 0).FirstOrDefault().Errors[0].ErrorMessage;
                ShowMessage(false, msg);
            }

            ShowMessage(false, "輸入資料有誤");
            return View(model);
        }

        private static string InsertDiary(ProductionDiaryView model, OracleConnection oralceConnection)
        {
            if (!string.IsNullOrEmpty(model.DIARYDATE) && !string.IsNullOrEmpty(model.ENTER_USER))
            {
                try
                {
                    string sql1 = @"select *
                                from w_dayn_note f
                                where f.diarydate = :diarydate";
                    OracleCommand cmd1 = new OracleCommand(sql1, oralceConnection);
                    cmd1.CommandType = CommandType.Text;
                    // 使用參數化填值
                    cmd1.Parameters.Add(":diarydate", model.DIARYDATE);

                    OracleDataAdapter DataAdapter = new OracleDataAdapter();
                    DataAdapter.SelectCommand = cmd1;
                    DataSet ds = new DataSet();
                    DataAdapter.Fill(ds, "Table");
                    DataTable dt = ds.Tables["Table"];

                    var queryData = new List<W_DAYN_NOTE>();
                    if (dt.Rows.Count > 0)
                    {
                        queryData = CommonMethod.ConvertToList<W_DAYN_NOTE>(dt).ToList();
                    }
                    model.LIN_PSCT = (queryData.Count() + 1).ToString("0000");
                    cmd1.Dispose();

                    //新增
                    string sql = $@"INSERT INTO W_DAYN_NOTE
                                    (DIARYDATE, 
                                    TYPE_NUM, 
                                    DATE_NOTE, 
                                    DATE_PIC1, 
                                    ENTER_USER, 
                                    COD_CUST, 
                                    NAM_ITEM, 
                                    LIN_PSCT)
                                    VALUES
                                    ('{model.DIARYDATE}', 
                                    '{model.TYPE_NUM}', 
                                    '{model.DATE_NOTE}', 
                                    '', 
                                    '{model.ENTER_USER}', 
                                    '{model.COD_CUST}', 
                                    '{model.NAM_ITEM}', 
                                    '{model.LIN_PSCT}'
                                    )";
                    OracleCommand cmd = new OracleCommand(sql, oralceConnection);
                    cmd.ExecuteNonQuery();

                }
                catch (Exception e)
                {
                    return "0000";
                }

                return model.LIN_PSCT;
            }
            return "0000";
        }

        private static void UpdateDiary(ProductionDiaryView model, OracleConnection oralceConnection)
        {
            if (!string.IsNullOrEmpty(model.DIARYDATE) && !string.IsNullOrEmpty(model.ENTER_USER))
            {
                //更新
                string sql = "Update W_DAYN_NOTE ";
                sql += " set TYPE_NUM = '" + model.TYPE_NUM + "', ";//項目代碼
                sql += " DATE_NOTE = '" + model.DATE_NOTE + "', ";//文字記錄
                sql += " DATE_PIC1 = '" + model.DATE_PIC1 + "', ";//圖檔紀錄
                sql += " ENTER_USER = '" + model.ENTER_USER + "', ";//填寫人員
                sql += " COD_CUST = '" + model.COD_CUST + "', ";//客戶
                sql += " NAM_ITEM = '" + model.NAM_ITEM + "', ";//型號
                sql += " LIN_PSCT = '" + model.LIN_PSCT + "'";//當日紀錄次數
                sql += " where  DIARYDATE = '" + model.DIARYDATE + "' and ENTER_USER = '" + model.ENTER_USER + "'";
                OracleCommand cmd = new OracleCommand(sql, oralceConnection);

                cmd.ExecuteNonQuery();
            }
        }

        private static string InsertDiaryPic(W_DAYN_PIC data, OracleConnection oralceConnection)
        {
            if (!string.IsNullOrEmpty(data.DIARYDATE) && !string.IsNullOrEmpty(data.LIN_PSCT))
            {
                try
                {
                    //新增
                    string sql = $@"INSERT INTO W_DAYN_PIC
                                    (DIARYDATE, 
                                    LIN_PSCT,                                     
                                    PIC_NAME)
                                    VALUES
                                    ('{data.DIARYDATE}',                                   
                                    '{data.LIN_PSCT}', 
                                    '{data.PIC_NAME}'
                                    )";
                    OracleCommand cmd = new OracleCommand(sql, oralceConnection);
                    cmd.ExecuteNonQuery();
                }
                catch (Exception e)
                {
                    return "0000";
                }

                return data.LIN_PSCT;
            }
            return "0000";
        }

        //public JsonResult GetDiaryRecord(string NUM_BUY, string LIN_BUY)
        //{
        //    var query = DiaryQuery(NUM_BUY, LIN_BUY);
        //    return Json(query, JsonRequestBehavior.AllowGet);
        //}

        public List<W_DAYN_NOTE> DiaryQuery(string DIARYDATE)
        {
            OracleConnection oralceConnection = new OracleConnection(g_OracleERPConnectionString);
            oralceConnection.Open();
            string sltSql = "select *" +
                                 " from W_DAYN_NOTE t" +
                                 " where t.diarydate = '" + DIARYDATE + "'";
            OracleCommand sltCmd = new OracleCommand(sltSql, oralceConnection);
            sltCmd.CommandType = CommandType.Text;

            OracleDataAdapter DataAdapter = new OracleDataAdapter();
            DataAdapter.SelectCommand = sltCmd;
            DataSet ds = new DataSet();
            DataAdapter.Fill(ds, "Table");
            DataTable dt = ds.Tables["Table"];

            var queryData = new List<W_DAYN_NOTE>();
            if (dt.Rows.Count > 0)
            {
                queryData = CommonMethod.ConvertToList<W_DAYN_NOTE>(dt).ToList();
            }

            oralceConnection.Close();
            oralceConnection.Dispose();
            return queryData;
        }

        public List<W_DAYN_PIC> DiaryPicQuery(string DIARYDATE, string LIN_PSCT)
        {
            OracleConnection oralceConnection = new OracleConnection(g_OracleERPConnectionString);
            oralceConnection.Open();
            string sltSql = "select *" +
                                 " from W_DAYN_PIC t" +
                                 " where t.diarydate = :DIARYDATE and t.lin_psct = :LIN_PSCT";
            OracleCommand sltCmd = new OracleCommand(sltSql, oralceConnection);
            sltCmd.CommandType = CommandType.Text;
            sltCmd.Parameters.Add(":DIARYDATE", DIARYDATE);
            sltCmd.Parameters.Add(":LIN_PSCT", LIN_PSCT);

            OracleDataAdapter DataAdapter = new OracleDataAdapter();
            DataAdapter.SelectCommand = sltCmd;
            DataSet ds = new DataSet();
            DataAdapter.Fill(ds, "Table");
            DataTable dt = ds.Tables["Table"];

            var queryData = new List<W_DAYN_PIC>();
            if (dt.Rows.Count > 0)
            {
                queryData = CommonMethod.ConvertToList<W_DAYN_PIC>(dt).ToList();
            }

            oralceConnection.Close();
            oralceConnection.Dispose();
            return queryData;
        }

        public List<W_DAYTYPE> TypeQuery()
        {
            OracleConnection oralceConnection = new OracleConnection(g_OracleERPConnectionString);
            oralceConnection.Open();
            string sltSql = "select * from W_DAYTYPE t";
            OracleCommand sltCmd = new OracleCommand(sltSql, oralceConnection);
            sltCmd.CommandType = CommandType.Text;

            OracleDataAdapter DataAdapter = new OracleDataAdapter();
            DataAdapter.SelectCommand = sltCmd;
            DataSet ds = new DataSet();
            DataAdapter.Fill(ds, "Table");
            DataTable dt = ds.Tables["Table"];

            var queryData = new List<W_DAYTYPE>();
            if (dt.Rows.Count > 0)
            {
                queryData = CommonMethod.ConvertToList<W_DAYTYPE>(dt).ToList();
            }

            oralceConnection.Close();
            oralceConnection.Dispose();
            return queryData;
        }

        private W_DAYN_NOTE GetDiary(string diarydate, string user, string lin)
        {
            // 連線資料庫
            OracleConnection oralceConnection = new OracleConnection(g_OracleERPConnectionString);
            oralceConnection.Open();

            string sql = @"select t.* from W_DAYN_NOTE t
                                    where enter_user = :ENTER_USER and diarydate = :DIARYDATE and lin_psct = :LIN_PSCT";

            OracleCommand cmd = new OracleCommand(sql, oralceConnection);
            cmd.CommandType = CommandType.Text;

            cmd.Parameters.Add(":ENTER_USER", user);
            cmd.Parameters.Add(":DIARYDATE", diarydate);
            cmd.Parameters.Add(":LIN_PSCT", lin);

            OracleDataAdapter DataAdapter = new OracleDataAdapter();
            DataAdapter.SelectCommand = cmd;
            DataSet ds = new DataSet();
            DataAdapter.Fill(ds, "Table");
            DataTable dt = ds.Tables["Table"];
            if (dt.Rows.Count > 0)
            {
                W_DAYN_NOTE data = new W_DAYN_NOTE();
                data = CommonMethod.ConvertToList<W_DAYN_NOTE>(dt).ToList().FirstOrDefault();
                oralceConnection.Close();
                oralceConnection.Dispose();

                return data;
            }

            oralceConnection.Close();
            oralceConnection.Dispose();

            return null;
        }

        public string Delete(string dd = "", string lin = "")
        {
            if (!string.IsNullOrEmpty(dd) && !string.IsNullOrEmpty(lin))
            {
                OracleConnection oralceConnection = new OracleConnection(g_OracleERPConnectionString);
                oralceConnection.Open();

                //刪除圖片
                string sql1 = @"select *
                                from w_dayn_pic f
                                where f.diarydate = :DIARYDATE and f.lin_psct = :LIN_PSCT";
                OracleCommand cmd1 = new OracleCommand(sql1, oralceConnection);
                cmd1.CommandType = CommandType.Text;
                // 使用參數化填值
                cmd1.Parameters.Add(":DIARYDATE", dd);
                cmd1.Parameters.Add(":LIN_PSCT", lin);

                OracleDataAdapter DataAdapter = new OracleDataAdapter();
                DataAdapter.SelectCommand = cmd1;
                DataSet ds = new DataSet();
                DataAdapter.Fill(ds, "Table");
                DataTable dt = ds.Tables["Table"];

                var picQuery = new List<W_DAYN_PIC>();
                if (dt.Rows.Count > 0)
                {
                    picQuery = CommonMethod.ConvertToList<W_DAYN_PIC>(dt).ToList();
                }
                cmd1.Dispose();

                foreach (var item in picQuery)
                {
                    DeleteDiaryPic(dd, lin, item.PIC_NAME, oralceConnection);
                    ImageHelper.DeleteFile(PhotoFolder, item.PIC_NAME);
                }

                //刪除日誌
                DeleteDiary(dd, lin, oralceConnection);

                oralceConnection.Close();
                oralceConnection.Dispose();

                return "success";
            }
            return "not found";
        }

        public string DeleteDiaryPic(string dd = "", string lin = "", string pic = "")
        {
            if (!string.IsNullOrEmpty(dd) && !string.IsNullOrEmpty(lin) && !string.IsNullOrEmpty(pic))
            {
                OracleConnection oralceConnection = new OracleConnection(g_OracleERPConnectionString);
                oralceConnection.Open();
                var result = DeleteDiaryPic(dd, lin, pic, oralceConnection);

                oralceConnection.Close();
                oralceConnection.Dispose();
                ImageHelper.DeleteFile(PhotoFolder, pic);

                return result;
            }
            return "not found";
        }

        private static string DeleteDiaryPic(string dd, string lin, string pic, OracleConnection oralceConnection)
        {
            if (!string.IsNullOrEmpty(dd) && !string.IsNullOrEmpty(lin))
            {
                try
                {
                    //新增
                    string sql = $@"delete W_DAYN_PIC
                                where diarydate = :DIARYDATE 
                                and lin_psct = :LIN_PSCT
                                and pic_name = :PIC_NAME";
                    OracleCommand cmd = new OracleCommand(sql, oralceConnection);
                    cmd.Parameters.Add(":DIARYDATE", dd);
                    cmd.Parameters.Add(":LIN_PSCT", lin);
                    cmd.Parameters.Add(":PIC_NAME", pic);
                    try
                    {
                        cmd.ExecuteNonQuery();
                    }
                    catch (Exception ex)
                    {
                        oralceConnection.Close();
                        oralceConnection.Dispose();
                        return "ex : " + ex.Message;
                    }

                    oralceConnection.Close();
                    oralceConnection.Dispose();
                }
                catch (Exception e)
                {
                    oralceConnection.Close();
                    oralceConnection.Dispose();
                    return e.Message;
                }

                return "e : " + "success";
            }
            return "failed";
        }

        private static bool DeleteDiary(string dd, string lin, OracleConnection oralceConnection)
        {
            if (!string.IsNullOrEmpty(dd) && !string.IsNullOrEmpty(lin))
            {
                try
                {
                    //新增
                    string sql = $@"delete W_DAYN_NOTE
                                where diarydate = :DIARYDATE 
                                and lin_psct = :LIN_PSCT";
                    OracleCommand cmd = new OracleCommand(sql, oralceConnection);
                    cmd.Parameters.Add(":DIARYDATE", dd);
                    cmd.Parameters.Add(":LIN_PSCT", lin);
                    cmd.ExecuteNonQuery();

                    oralceConnection.Close();
                    oralceConnection.Dispose();
                }
                catch (Exception e)
                {
                    oralceConnection.Close();
                    oralceConnection.Dispose();
                    return false;
                }

                return true;
            }
            return false;
        }
    }
}