﻿using FairlyRepair.Models;
using FairlyRepair.Models.Others;
using FairlyRepair.Repositories;
using FairlyRepair.Repositories.T8Repositories;
using FairlyRepair.Utility.Helpers;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FairlyRepair.Areas.Admin.ViewModels.Replace
{
    public class ReplaceIndexView : PageQuery
    {
        AdminRepository adminRepository = new AdminRepository(new FairlyRepairDBEntities());
        PersonRepository personRepository = new PersonRepository(new T8ERPEntities());
        GroupPersonRepository groupPersonRepository = new GroupPersonRepository(new T8ERPEntities());
        DeviceRepository deviceRepository = new DeviceRepository(new FairlyRepairDBEntities());

        public ReplaceIndexView()
        {
            this.Sorting = "CreateDate";
            this.IsDescending = true;
        }

        public PageResult<ReplaceView> PageResult { get; set; }

        [Required]
        [Display(Name = "ID")]
        public int ID { get; set; }

        [Required]
        [Display(Name = "廠區")]
        public int FactoryID { get; set; }
        public string FactoryTitle
        {
            get
            {
                return EnumHelper.GetDescription((Factory)this.FactoryID);
            }
        }

        [Display(Name = "設備類型")]
        public int DeviceType { get; set; }

        [Required]
        [Display(Name = "設備")]
        public int DeviceID { get; set; }

        [Required]
        [Display(Name = "聯絡廠商")]
        public int Manufacturer { get; set; }

        [Required]
        [Display(Name = "說明")]
        public string Description { get; set; }

        [Required]
        [Display(Name = "總經理審核狀態")]
        public int? PresAppStatus { get; set; }

        [Required]
        [Display(Name = "需總經理審核")]
        public bool? IsPresApproval { get; set; }

        [Required]
        [Display(Name = "副總審核狀態")]
        public int? ViceAppStatus { get; set; }

        [Required]
        [Display(Name = "需副總審核")]
        public bool? IsViceApproval { get; set; }

        [Display(Name = "總經理審核日期")]
        public DateTime? PresAppDate { get; set; }

        [Display(Name = "副總審核日期")]
        public DateTime? ViceAppDate { get; set; }

        [Display(Name = "撤銷狀態")]
        public bool? IsRevoke { get; set; }

        [Display(Name = "預計達成日期")]
        public DateTime ExpectDate { get; set; }

        [Display(Name = "達成日期")]
        public DateTime CompletionDate { get; set; }

        [Display(Name = "金額(未稅)")]
        public int Amount { get; set; }

        [Display(Name = "結案狀態")]
        public int CaseStatus { get; set; }

        [Required]
        [Display(Name = "更新時間")]
        public DateTime UpdateDate { get; set; }

        [Required]
        [Display(Name = "更新者")]
        public int Updater { get; set; }

        [Required]
        [Display(Name = "建立時間")]
        public DateTime CreateDate { get; set; }

        [Required]
        [Display(Name = "建立者")]
        public int Creater { get; set; }

        public string CreaterName
        {
            get
            {
                try
                {
                    if (this.Creater == 0)
                    {
                        this.Creater = AdminInfoHelper.GetAdminInfo().ID;
                    }
                    var jobID = adminRepository.GetById(this.Creater).Account;
                    var nameInfo = groupPersonRepository.FindByJobID(jobID);
                    if (nameInfo == null)
                    {
                        return adminRepository.GetById(this.Creater).Name;
                    }

                    return nameInfo.PersonName;
                }
                catch (Exception e)
                {
                    return "";
                    //return adminRepository.GetById(this.Creater).Name;
                }
            }
        }

        public List<SelectListItem> FactoryOptions
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                var values = EnumHelper.GetValues<Factory>();
                foreach (var v in values)
                {
                    result.Add(new SelectListItem()
                    {
                        Value = ((int)v).ToString(),
                        Text = EnumHelper.GetDescription(v)
                    });
                }
                return result;
            }
        }
        public List<SelectListItem> ManufacturerOptions
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                var values = deviceRepository.Query((int)DeviceClass.Second, 1);
                foreach (var v in values)
                {
                    result.Add(new SelectListItem()
                    {
                        Value = v.ID.ToString(),
                        Text = v.Title
                    });
                }
                return result;
            }
        }

        public List<SelectListItem> DeviceOptions
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                var values = deviceRepository.Query((int)DeviceClass.Second, 1);
                foreach (var v in values)
                {
                    result.Add(new SelectListItem()
                    {
                        Value = v.ID.ToString(),
                        Text = v.Title
                    });
                }
                return result;
            }
        }

    }
}