﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FairlyRepair.Areas.Admin.ViewModels.Tags
{
    public class AutoCompleteView
    {
        public string label { get; set; }
        public string value { get; set; }
    }
}