﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace FairlyRepair.Areas.Admin.ViewModels.Emissions
{
    public class EmissionsT6View
    {        
        [Required]
        [Display(Name = "編號")]
        public int ID { get; set; }
        
        [Required]
        [Display(Name = "排放紀錄編號")]
        public int EmissionID { get; set; }
        
        [Required]
        [Display(Name = "進行不確定性評估之排放量絕對值加總")]
        public decimal Uncertainty { get; set; }
        
        [Required]
        [Display(Name = "排放總量絕對值加總")]
        public decimal AbsoluteEmissions { get; set; }
        
        [Required]
        [Display(Name = "進行不確定性評估之排放量佔總排放量之比例")]
        public decimal UncertaintyPercent { get; set; }
        
        [Required]
        [Display(Name = "95%信賴區間下限")]
        public decimal NintyfiveMin { get; set; }
        
        [Required]
        [Display(Name = "95%信賴區間上限")]
        public decimal NintyfiveMax { get; set; }
    }
}