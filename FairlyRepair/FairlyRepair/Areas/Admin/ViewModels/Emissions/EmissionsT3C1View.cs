﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace FairlyRepair.Areas.Admin.ViewModels.Emissions
{
    public class EmissionsT3C1View
    {
        [Required]
        [Display(Name = "編號")]
        public int ID { get; set; }

        [Required]
        [Display(Name = "排放紀錄編號")]
        public int EmissionID { get; set; }

        [Required]
        [Display(Name = "CO2")]
        public decimal CO2 { get; set; }

        [Required]
        [Display(Name = "CO2比例")]
        public decimal CO2Percent { get; set; }

        [Required]
        [Display(Name = "CH4")]
        public decimal CH4 { get; set; }

        [Required]
        [Display(Name = "CH4比例")]
        public decimal CH4Percent { get; set; }

        [Required]
        [Display(Name = "N2O")]
        public decimal N2O { get; set; }

        [Required]
        [Display(Name = "N2O比例")]
        public decimal N2OPercent { get; set; }

        [Required]
        [Display(Name = "HFCS")]
        public decimal HFCS { get; set; }

        [Required]
        [Display(Name = "HFCS比例")]
        public decimal HFCSPercent { get; set; }

        [Required]
        [Display(Name = "PFCS")]
        public decimal PFCS { get; set; }

        [Required]
        [Display(Name = "PFCS比例")]
        public decimal PFCSPercent { get; set; }

        [Required]
        [Display(Name = "SF6")]
        public decimal SF6 { get; set; }

        [Required]
        [Display(Name = "SF6比例")]
        public decimal SF6Percent { get; set; }

        [Required]
        [Display(Name = "NF3")]
        public decimal NF3 { get; set; }

        [Required]
        [Display(Name = "NF3比例")]
        public decimal NF3Percent { get; set; }

        [Required]
        [Display(Name = "七種溫室氣體年總排放當量")]
        public decimal GreenHouseGases { get; set; }

        [Required]
        [Display(Name = "七種溫室氣體年總排放當量比例")]
        public decimal GreenHouseGasesPercent { get; set; }
    }
}