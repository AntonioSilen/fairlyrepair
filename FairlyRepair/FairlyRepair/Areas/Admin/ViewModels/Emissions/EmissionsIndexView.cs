﻿using FairlyRepair.Models;
using FairlyRepair.Models.Others;
using FairlyRepair.Repositories;
using FairlyRepair.Repositories.T8Repositories;
using FairlyRepair.Utility.Helpers;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FairlyRepair.Areas.Admin.ViewModels.Emissions
{
    public class EmissionsIndexView : PageQuery
    {
        public EmissionsIndexView()
        {
            this.Sorting = "CreateDate";
            this.IsDescending = true;
        }

        public PageResult<EmissionsView> PageResult { get; set; }

        [Display(Name = "編號")]
        public int ID { get; set; }

        [Display(Name = "課別")]
        public string DeptID { get; set; }

        [Display(Name = "調度員")]
        public int DispatcherID { get; set; }

        [Display(Name = "維修員")]
        public string MaintID { get; set; }

        [Display(Name = "總經理審核狀態")]
        public int? PresAppStatus { get; set; }

        [Display(Name = "需總經理審核")]
        public bool? IsPresApproval { get; set; }

        [Display(Name = "副總審核狀態")]
        public int? ViceAppStatus { get; set; }

        [Display(Name = "需副總審核")]
        public bool? IsViceApproval { get; set; }

        [Display(Name = "撤銷狀態")]
        public bool? IsRevoke { get; set; }

        //[Display(Name = "已派工")]
        //public bool IsDispatch { get; set; }

        //[Display(Name = "已解決")]
        //public bool IsSloved { get; set; }

        [Display(Name = "工時(分鐘)")]
        public decimal Minute { get; set; }

        [Display(Name = "報修申請時間")]
        public DateTime CreateDate { get; set; }

        [Required]
        [Display(Name = "建立者")]
        public int Creater { get; set; }

        [Display(Name = "處理狀態")]
        public int? ProcessingStatus { get; set; }

        [Display(Name = "開始日期(啟)")]
        public DateTime? StartDate { get; set; }

        [Display(Name = "結束日期(迄)")]
        public DateTime? EndDate { get; set; }
    }
}