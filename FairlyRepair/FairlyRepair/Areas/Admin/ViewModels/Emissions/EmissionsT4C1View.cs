﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace FairlyRepair.Areas.Admin.ViewModels.Emissions
{
    public class EmissionsT4C1View
    {
        [Required]
        [Display(Name = "編號")]
        public int ID { get; set; }

        [Required]
        [Display(Name = "排放紀錄編號")]
        public int EmissionID { get; set; }
        
        [Required]
        [Display(Name = "固定排放")]
        public decimal Fixed { get; set; }
        
        [Required]
        [Display(Name = "固定排放比例")]
        public decimal FixedPercent { get; set; }
        
        [Required]
        [Display(Name = "移動排放")]
        public decimal Mobile { get; set; }
        
        [Required]
        [Display(Name = "移動排放比例")]
        public decimal MobilePercent { get; set; }
        
        [Required]
        [Display(Name = "製程排放")]
        public decimal Process { get; set; }
        
        [Required]
        [Display(Name = "製程排放比例")]
        public decimal ProcessPercent { get; set; }
        
        [Required]
        [Display(Name = "逸散排放")]
        public decimal Fugitive { get; set; }
        
        [Required]
        [Display(Name = "逸散排放比例")]
        public decimal FugitivePercent { get; set; }
        
        [Required]
        [Display(Name = "範疇1")]
        public decimal Category1 { get; set; }
        
        [Required]
        [Display(Name = "範疇1比例")]
        public decimal Category1Percent { get; set; }
        
        [Required]
        [Display(Name = "範疇2 能源間接排放")]
        public decimal Category2Energy { get; set; }
        
        [Required]
        [Display(Name = "範疇2 能源間接排放比例")]
        public decimal Category2EnergyPercent { get; set; }
        
        [Required]
        [Display(Name = "範疇3 其他間接排放")]
        public decimal Category3Other { get; set; }
        
        [Required]
        [Display(Name = "範疇3 其他間接排放比例")]
        public decimal Category3OtherPercent { get; set; }
        
        [Required]
        [Display(Name = "總排放當量")]
        public decimal Total { get; set; }
        
        [Required]
        [Display(Name = "總排放當量比例")]
        public decimal TotalPercent { get; set; }
    }
}