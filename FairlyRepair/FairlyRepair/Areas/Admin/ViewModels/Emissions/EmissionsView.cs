﻿using FairlyRepair.Models;
using FairlyRepair.Repositories;
using FairlyRepair.Repositories.T8Repositories;
using FairlyRepair.Utility.Helpers;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FairlyRepair.Areas.Admin.ViewModels.Emissions
{
    public class EmissionsView
    {
        AdminRepository adminRepository = new AdminRepository(new FairlyRepairDBEntities());
        PersonRepository personRepository = new PersonRepository(new T8ERPEntities());
        GroupPersonRepository groupPersonRepository = new GroupPersonRepository(new T8ERPEntities());

        public EmissionsView()
        {
            this.EmissionsT2 = new EmissionsT2View();
            this.EmissionsT3C1 = new EmissionsT3C1View();
            this.EmissionsT4C1 = new EmissionsT4C1View();
            this.EmissionsT6 = new EmissionsT6View();
        }

        [Required]
        [Display(Name = "編號")]
        public int ID { get; set; }

        [Required]
        [Display(Name = "備註")]
        public string Remark { get; set; }

        [Required]
        [Display(Name = "紀錄時間")]
        public DateTime EmissionDate { get; set; }

        [Required]
        [Display(Name = "填單時間")]
        public DateTime CreateDate { get; set; }

        [Required]
        [Display(Name = "工號")]
        public string PersonId { get; set; }

        [Required]
        [Display(Name = "建立者")]
        public int Creater { get; set; }

        public EmissionsT2View EmissionsT2 { get; set; }
        public EmissionsT3C1View EmissionsT3C1 { get; set; }
        public EmissionsT4C1View EmissionsT4C1 { get; set; }
        public EmissionsT6View EmissionsT6 { get; set; }
    }
}