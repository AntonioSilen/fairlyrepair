﻿using FairlyRepair.Models;
using FairlyRepair.Repositories;
using FairlyRepair.Utility.Helpers;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FairlyRepair.Areas.Admin.ViewModels.Equipment
{
    public class EquipmentView
    {
        EquipmentRepository equipmentRepository = new EquipmentRepository(new Models.FairlyRepairDBEntities());

        public EquipmentView()
        {
            this.IsOnline = true;
            this.Category = 0;
        }

        [Required]
        [Display(Name = "ID")]
        public int ID { get; set; }

        //[Required]
        //[Display(Name = "廠區")]
        //public int FactoryID { get; set; }
        //public string FactoryTitle
        //{
        //    get
        //    {
        //        return this.FactoryID != 0 ? EnumHelper.GetDescription((Factory)this.FactoryID) : "";
        //    }
        //}

        [Required]
        [Display(Name = "位置")]
        public int Parent { get; set; }

        public string ParentTitle { get; set; }
        //{
        //    get
        //    {
        //        return equipmentRepository.GetEquipmentTitle(this.Parent);
        //    }
        //}
        public List<BreadModel> BreadList { get; set; }

        [Required]
        [Display(Name = "層級")]
        public int Class { get; set; }

        [Display(Name = "類別")]
        public int Category { get; set; }

        public List<SelectListItem> CategoryOptions
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                EquipmentCategoryRepository equipmentCategoryRepository = new EquipmentCategoryRepository(new FairlyRepairDBEntities());
                var values = equipmentCategoryRepository.Query(true);
                foreach (var v in values)
                {
                    result.Add(new SelectListItem()
                    {
                        Value = v.ID.ToString(),
                        Text = v.Title
                    });
                }
                return result;
            }
        }

        [Required]
        [Display(Name = "名稱")]
        public string Title { get; set; }

        [Display(Name = "圖片")]
        public string MainPic { get; set; }

        [Required]
        [Display(Name = "啟用狀態")]
        public bool IsOnline { get; set; }
        public HttpPostedFileBase MainPicFile { get; set; }

        //public List<SelectListItem> FactoryOptions
        //{
        //    get
        //    {
        //        List<SelectListItem> result = new List<SelectListItem>();
        //        var values = EnumHelper.GetValues<Factory>();
        //        foreach (var v in values)
        //        {
        //            result.Add(new SelectListItem()
        //            {
        //                Value = ((int)v).ToString(),
        //                Text = EnumHelper.GetDescription(v)
        //            });
        //        }
        //        return result;
        //    }
        //}

        public class BreadModel
        {
            public string Controller { get; set; }
            public string Action { get; set; }
            public string Query { get; set; }
            public string Title { get; set; }
            public int Sort { get; set; }
        }
    }
}