﻿using FairlyRepair.Models;
using FairlyRepair.Models.Others;
using FairlyRepair.Repositories;
using FairlyRepair.Utility.Helpers;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using static FairlyRepair.Areas.Admin.ViewModels.Equipment.EquipmentView;

namespace FairlyRepair.Areas.Admin.ViewModels.Equipment
{
    public class EquipmentIndexView : PageQuery
    {
        EquipmentRepository equipmentRepository = new EquipmentRepository(new Models.FairlyRepairDBEntities());

        public EquipmentIndexView()
        {
            this.Sorting = "Parent";
            this.IsDescending = false;
        }

        public PageResult<EquipmentView> PageResult { get; set; }

        [Display(Name = "廠區")]
        public int FactoryID { get; set; }
        public string FactoryTitle
        {
            get
            {
                return this.FactoryID != 0 ? EnumHelper.GetDescription((Factory)this.FactoryID) : "";
            }
        }

        [Display(Name = "位置")]
        public int Parent { get; set; }

        public string ParentTitle
        {
            get
            {
                return equipmentRepository.GetEquipmentTitle(this.Parent);
                //var info = equipmentRepository.Query().FirstOrDefault();
                //if (info != null)
                //{
                //    return info.Title;
                //}
                //return "";
            }
        }

        public List<BreadModel> BreadList
        {
            get
            {
                return equipmentRepository.GetBreadList(this.Parent).OrderByDescending(q => q.Sort).ToList();
            }
        }

        [Display(Name = "層級")]
        public int Class { get; set; }

        [Display(Name = "類別")]
        public int Category { get; set; }

        [Display(Name = "名稱")]
        public string Title { get; set; }

        [Display(Name = "圖片")]
        public string MainPic { get; set; }

        [Display(Name = "啟用狀態")]
        public bool? IsOnline { get; set; }
    }
}