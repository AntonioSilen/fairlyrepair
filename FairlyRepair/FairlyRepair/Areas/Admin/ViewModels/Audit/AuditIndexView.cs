﻿using FairlyRepair.Models;
using FairlyRepair.Models.Others;
using FairlyRepair.Utility.Helpers;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FairlyRepair.Areas.Admin.ViewModels.Audit
{
    public class AuditIndexView : PageQuery
    {
        public AuditIndexView()
        {
            this.Sorting = "CreateDate";
            this.IsDescending = true;
            //this.IsInvalid = null;
            this.IsInvalid = false;
            this.IsOfficer = true;
        }
        public PageResult<AuditItemView> PageResult { get; set; }

        [Display(Name = "審核編號")]
        public int ID { get; set; }

        [Display(Name = "報告編號")]
        public int ReportID { get; set; }

        [Display(Name = "報告標題")]
        public string Title { get; set; }

        [Display(Name = "人員工號")]
        public string PersonID { get; set; }

        [Display(Name = "類型")]
        public int Type { get; set; }

        [Display(Name = "簽核狀態")]
        public int? State { get; set; }

        [Display(Name = "是否駁回")]
        public bool? IsInvalid { get; set; }

        [Display(Name = "承辦人")]
        public int Officer { get; set; }

        [Display(Name = "建立時間")]
        public DateTime CreateDate { get; set; }

        [Display(Name = "顯示(本帳號)承辦的訓練")]
        public bool IsOfficer { get; set; }

        public List<SelectListItem> TypeOptions
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                var values = EnumHelper.GetValues<AuditType>();
                foreach (var v in values)
                {
                    result.Add(new SelectListItem()
                    {
                        Value = ((int)v).ToString(),
                        Text = EnumHelper.GetDescription(v)
                    });
                }
                return result;
            }
        }

        public List<SelectListItem> StateOptions
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                var values = EnumHelper.GetValues<AuditState>();
                foreach (var v in values)
                {
                    result.Add(new SelectListItem()
                    {
                        Value = ((int)v).ToString(),
                        Text = EnumHelper.GetDescription(v)
                    });
                }
                return result;
            }
        }
    }
}