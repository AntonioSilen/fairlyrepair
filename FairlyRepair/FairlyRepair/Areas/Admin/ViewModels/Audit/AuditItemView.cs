﻿using FairlyRepair.Models;
using FairlyRepair.Utility.Helpers;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FairlyRepair.Areas.Admin.ViewModels.Audit
{
    public class AuditItemView
    {
        [Required]
        [Display(Name = "審核編號")]
        public int ID { get; set; }

        [Required]
        [Display(Name = "報告編號")]
        public int ReportID { get; set; }

        [Required]
        [Display(Name = "報告編號")]
        public int ParentID { get; set; }

        [Display(Name = "報告標題")]
        public string Title { get; set; }

        //[Display(Name = "人員工號")]
        //public string PersonID { get; set; }
        //public string PersonName { get; set; }

        [Required]
        [Display(Name = "類型")]
        public int Type { get; set; }

        [Required]
        [Display(Name = "簽核狀態")]
        public int State { get; set; }

        [Required]
        [Display(Name = "是否駁回")]
        public bool IsInvalid { get; set; }

        [Required]
        [Display(Name = "承辦人")]
        public string Officer { get; set; }
        public string OfficerName { get; set; }

        [Required]
        [Display(Name = "建立時間")]
        public DateTime CreateDate { get; set; }

        public List<SelectListItem> TypeOptions
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                var values = EnumHelper.GetValues<AuditType>();
                foreach (var v in values)
                {
                    result.Add(new SelectListItem()
                    {
                        Value = ((int)v).ToString(),
                        Text = EnumHelper.GetDescription(v)
                    });
                }
                return result;
            }
        }

        public List<SelectListItem> StateOptions
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                var values = EnumHelper.GetValues<AuditState>();
                foreach (var v in values)
                {
                    result.Add(new SelectListItem()
                    {
                        Value = ((int)v).ToString(),
                        Text = EnumHelper.GetDescription(v)
                    });
                }
                return result;
            }
        }
    }
}