﻿using FairlyRepair.Models;
using FairlyRepair.Models.Others;
using FairlyRepair.Repositories;
using FairlyRepair.Repositories.T8Repositories;
using FairlyRepair.Utility.Helpers;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FairlyRepair.Areas.Admin.ViewModels.Repair
{
    public class RepairIndexView : PageQuery
    {
        EquipmentRepository equipmentRepository = new EquipmentRepository(new FairlyRepairDBEntities());

        public RepairIndexView()
        {
            this.Sorting = "CreateDate";
            this.IsDescending = true;
        }

        public PageResult<RepairView> PageResult { get; set; }

        [Display(Name = "維修人員")]
        public string Mainters { get; set; }
        public List<MainterItem> MainterList { get; set; }

        [Display(Name = "編號")]
        public int ID { get; set; }

        [Display(Name = "課別")]
        public string DeptID { get; set; }

        [Display(Name = "廠區")]
        public int FactoryID { get; set; }

        public string FactoryTitle
        {
            get
            {
                return EnumHelper.GetDescription((Factory)this.FactoryID);
            }
        }

        [Required]
        [Display(Name = "樓層")]
        public int FloorID { get; set; }
        public string FloorTitle
        {
            get
            {
                var data = equipmentRepository.GetById(this.FloorID);
                return data == null ? "" : data.Title;
            }
        }

        [Required]
        [Display(Name = "區域")]
        public int AreaID { get; set; }
        public string AreaTitle
        {
            get
            {
                var data = equipmentRepository.GetById(this.AreaID);
                return data == null ? "" : data.Title;
            }
        }

        [Required]
        [Display(Name = "報修類型")]
        public int DeviceType { get; set; }
        public string DeviceTypeName
        {
            get
            {
                return deviceRepository.FindBy(this.DeviceType).Title;
            }
        }


        [Required]
        [Display(Name = "設備")]
        public int DeviceID { get; set; }
        public string DeviceName
        {
            get
            {
                var data = equipmentRepository.GetById(this.DeviceID);
                return data == null ? "" : data.Title;
                //return deviceRepository.FindBy(this.DeviceID).Title;
            }
        }

        [Display(Name = "設備項目")]
        public int DeviceItem { get; set; }
        public string DeviceItemName
        {
            get
            {
                //var ItemInfo = deviceRepository.FindBy(this.DeviceItem);
                //return ItemInfo == null ? "" : ItemInfo.Title;
                var data = equipmentRepository.GetById(this.AreaID);
                return data == null ? "" : data.Title;
            }
        }

        [Display(Name = "調度員")]
        public int DispatcherID { get; set; }

        [Display(Name = "維修員")]
        public string MaintID { get; set; }

        [Display(Name = "總經理審核狀態")]
        public int? PresAppStatus { get; set; }

        [Display(Name = "需總經理審核")]
        public bool? IsPresApproval { get; set; }

        [Display(Name = "副總審核狀態")]
        public int? ViceAppStatus { get; set; }

        [Display(Name = "需副總審核")]
        public bool? IsViceApproval { get; set; }

        [Display(Name = "撤銷狀態")]
        public bool? IsRevoke { get; set; }

        //[Display(Name = "已派工")]
        //public bool IsDispatch { get; set; }

        //[Display(Name = "已解決")]
        //public bool IsSloved { get; set; }

        [Display(Name = "工時(分鐘)")]
        public decimal Minute { get; set; }

        [Display(Name = "報修申請時間")]
        public DateTime CreateDate { get; set; }

        [Required]
        [Display(Name = "建立者")]
        public int Creater { get; set; }

        [Display(Name = "處理狀態")]
        public int? ProcessingStatus { get; set; }

        public List<SelectListItem> FactoryOptions
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                var values = EnumHelper.GetValues<Factory>();
                foreach (var v in values)
                {
                    result.Add(new SelectListItem()
                    {
                        Value = ((int)v).ToString(),
                        Text = EnumHelper.GetDescription(v)
                    });
                }
                return result;
            }
        }

        public List<SelectListItem> FloorOptions
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem()
                {
                    Value = "0",
                    Text = "---選擇樓層---"
                });

                return result;
            }
        }
        public List<SelectListItem> AreaOptions
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem()
                {
                    Value = "0",
                    Text = "---選擇區域---"
                });

                return result;
            }
        }

        PersonRepository personRepository = new PersonRepository(new T8ERPEntities());

        public List<SelectListItem> DepartmentOptions
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                var values = personRepository.GetDepartments();
                foreach (var v in values)
                {
                    result.Add(new SelectListItem()
                    {
                        Value = v.DeptId,
                        Text = v.DeptName
                    });
                }
                return result;
            }
        }

        DeviceRepository deviceRepository = new DeviceRepository(new FairlyRepairDBEntities());

        public List<SelectListItem> DeviceTypeOptions
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                if (this.DeviceType != 0)
                {
                    var values = deviceRepository.Query((int)DeviceClass.Main);
                    foreach (var v in values)
                    {
                        result.Add(new SelectListItem()
                        {
                            Value = v.ID.ToString(),
                            Text = v.Title
                        });
                    }
                }

                return result;
            }
        }

        public List<SelectListItem> DeviceOptions
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                //result.Add(new SelectListItem()
                //{
                //    Value = "0",
                //    Text = ""
                //});
                if (this.DeviceType != 0)
                {                    
                    var values = deviceRepository.Query((int)DeviceClass.Second, this.DeviceType);
                    foreach (var v in values)
                    {
                        result.Add(new SelectListItem()
                        {
                            Value = v.ID.ToString(),
                            Text = v.Title
                        });
                    }
                }
               
                return result;
            }
        }

        public List<SelectListItem> DeviceItemOptions
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem()
                {
                    Value = "0",
                    Text = ""
                });
                if (this.DeviceID != 0)
                {
                    var values = deviceRepository.Query((int)DeviceClass.Third, this.DeviceID);
                    foreach (var v in values)
                    {
                        result.Add(new SelectListItem()
                        {
                            Value = v.ID.ToString(),
                            Text = v.Title
                        });
                    }
                }
                return result;
            }
        }
    }

    public class MainterItem
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public bool IsSelected { get; set; }
    }
}