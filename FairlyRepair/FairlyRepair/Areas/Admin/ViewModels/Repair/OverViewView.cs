﻿using FairlyRepair.Models;
using FairlyRepair.Repositories;
using FairlyRepair.Repositories.T8Repositories;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FairlyRepair.Areas.Admin.ViewModels.Repair
{
    public class OverViewView
    {
        PersonRepository personRepository = new PersonRepository(new T8ERPEntities());
        GroupPersonRepository groupPersonRepository = new GroupPersonRepository(new T8ERPEntities());
        DeviceRepository deviceRepository = new DeviceRepository(new FairlyRepairDBEntities());
        EquipmentRepository equipmentRepository = new EquipmentRepository(new FairlyRepairDBEntities());

        [Display(Name = "訓練單位")]
        public string DeptID { get; set; }

        [Display(Name = "費用")]
        public int Fee { get; set; }

        [Display(Name = "訓練資料")]
        public List<RepairView> RepairList { get; set; }

        //public List<DeptItem> DeptList { get; set; }
        //public List<DeptItem> DepartmentList { get; set; }

        [Display(Name = "廠區")]
        public int FactoryID { get; set; }
        public List<SelectListItem> FactoryOptions
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                var values = equipmentRepository.Query(true, 0, 1);
                foreach (var v in values)
                {
                    result.Add(new SelectListItem()
                    {
                        Value = v.ID.ToString(),
                        Text = v.Title
                    });
                }
                return result;
            }
        }

        [Display(Name = "樓層")]
        public int FloorID { get; set; }
        public List<SelectListItem> FloorOptions
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem()
                {
                    Value = "0",
                    Text = "---選擇樓層---"
                });

                return result;
            }
        }

        [Display(Name = "區域")]
        public int AreaID { get; set; }
        public List<SelectListItem> AreaOptions
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem()
                {
                    Value = "0",
                    Text = "---選擇區域---"
                });

                return result;
            }
        }

        [Display(Name = "設備")]
        public int DeviceID { get; set; }
        public List<SelectListItem> DeviceOptions
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem()
                {
                    Value = "0",
                    Text = "---選擇設備---"
                });

                return result;
            }
        }

        public DateTime DateRange { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
    }

    public class DeptItem
    {
        public string DeptID { get; set; }
        public string ParentDeptID { get; set; }
        public string Title { get; set; }
    }
}