﻿using FairlyRepair.Models;
using FairlyRepair.Repositories;
using FairlyRepair.Repositories.OracleRepositories;
using FairlyRepair.Repositories.T8Repositories;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FairlyRepair.Areas.Admin.ViewModels.EightD
{
    public class OverViewView
    {
        CDPTRepository cDPTRepository = new CDPTRepository();
        EightDCategoryRepository eightDCategoryRepository = new EightDCategoryRepository(new FairlyRepairDBEntities());
        PersonRepository personRepository = new PersonRepository(new T8ERPEntities());
        GroupPersonRepository groupPersonRepository = new GroupPersonRepository(new T8ERPEntities());

        [Display(Name = "相關部門")]
        public string DeptID { get; set; }

        [Display(Name = "費用")]
        public int Fee { get; set; }

        [Display(Name = "8D資料")]
        public List<EightDView> EightDList { get; set; }

        [Display(Name = "客戶")]
        public string Customer { get; set; }
        public List<SelectListItem> CustomerOptions
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem()
                {
                    Value = "無",
                    Text = "無"
                });
                var dataList = cDPTRepository.GetAll();
                foreach (var item in dataList)
                {
                    result.Add(new SelectListItem()
                    {
                        Value = item.COD_CUST,
                        Text = item.COD_CUST
                    });
                }
                result.Add(new SelectListItem()
                {
                    Value = "其他",
                    Text = "其他"
                });

                return result;
            }
        }

        [Display(Name = "類別")]
        public int Category { get; set; }
        public List<SelectListItem> CategoryOptions
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                var dataList = eightDCategoryRepository.Query(true, "");
                foreach (var item in dataList)
                {
                    result.Add(new SelectListItem()
                    {
                        Value = item.ID.ToString(),
                        Text = item.Title
                    });
                }

                return result;
            }
        }
   
        public DateTime DateRange { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
    }

    public class DeptItem
    {
        public string DeptID { get; set; }
        public string ParentDeptID { get; set; }
        public string Title { get; set; }
    }
}
