﻿using FairlyRepair.Models;
using FairlyRepair.MeetModels;
using FairlyRepair.Models.Others;
using FairlyRepair.Repositories;
using FairlyRepair.Repositories.OracleRepositories;
using FairlyRepair.Repositories.T8Repositories;
using FairlyRepair.Utility.Helpers;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FairlyRepair.Areas.Admin.ViewModels.EightD
{
    public class EightDView : AuditView
    {
        private PersonRepository t8personRepository = new Repositories.T8Repositories.PersonRepository(new T8ERPEntities
());

        public EightDView()
        {            
            this.IsRevoke = false;
        }

        public bool FromAudit { get; set; }

        [Required]
        [Display(Name = "ID")]
        public int ID { get; set; }

        [Display(Name = "流水號")]
        public string SerialNumber { get; set; }

        [Required]
        [Display(Name = "標題")]
        public string Title { get; set; }

        [Required]
        [Display(Name = "客戶")]
        public string Customer { get; set; }

        [Required]
        [Display(Name = "類型")]
        public int Category { get; set; }

        [Required]
        [Display(Name = "相關部門")]
        public string Depts { get; set; }

        [Display(Name = "8D表單")]
        public string ReportFile { get; set; }
        public HttpPostedFileBase ReportFilePost { get; set; }

        [Display(Name = "費用")]
        public int Fee { get; set; }

        [Required]
        [Display(Name = "是否撤銷")]
        public bool IsRevoke { get; set; }

        [Required]
        [Display(Name = "是否結案")]
        public bool IsFinished { get; set; }

        [Required]
        [Display(Name = "建立時間")]
        public DateTime CreateDate { get; set; }

        [Display(Name = "結案時間")]
        public DateTime? FinishDate { get; set; }

        [Required]
        [Display(Name = "建立者")]
        public string CreaterId { get; set; }

        [Display(Name = "8D檔案")]
        public string Files { get; set; }
        public List<EightDFile> FilesList { get; set; }
        public List<HttpPostedFileBase> FilesFiles { get; set; }

        public List<SelectListItem> CustomerOptions
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem()
                {
                    Text = "無",
                    Value = "無"
                });
                CDPTRepository cDPTRepository = new CDPTRepository();
                var values = cDPTRepository.GetAll();
                foreach (var v in values)
                {
                    result.Add(new SelectListItem()
                    {
                        Text = v.COD_CUST,
                        Value = v.COD_CUST
                    });
                }

                result.Add(new SelectListItem()
                {
                    Text = "其他",
                    Value = "其他"
                });

                return result;
            }
        }

        public List<SelectListItem> CategoryOptions
        {
            get
            {
                EightDCategoryRepository eightDCategoryRepository = new EightDCategoryRepository(new FairlyRepairDBEntities());
                List<SelectListItem> result = new List<SelectListItem>();
                var values = eightDCategoryRepository.Query(true, "", 1);
                foreach (var v in values)
                {
                    result.Add(new SelectListItem()
                    {
                        Text = v.Title,
                        Value = v.ID.ToString()
                    });
                }

                return result;
            }
        }

        public List<SelectListItem> DeptOptions
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                comPersonRepository compersonRepository = new comPersonRepository(new T8ERPEntities());
                var values = compersonRepository.GetPersonQuery().GroupBy(x => new { x.DeptId, x.DeptName }).ToList();
                foreach (var v in values)
                {
                    result.Add(new SelectListItem()
                    {
                        Text = v.Key.DeptName,
                        Value = (v.Key.DeptId).ToString()
                    });
                }

                return result;
            }
        }

        public string CreaterDeptId
        {
            get
            {
                Repositories.AdminRepository adminRepository = new Repositories.AdminRepository(new FairlyRepairDBEntities());
                string account = adminRepository.GetAll().Where(q => q.Account == this.CreaterId).FirstOrDefault().Account;
                var t8Info = t8personRepository.FindByJobID(account);
                return t8Info == null ? "0001" : t8Info.DeptID;
            }
        }

        public AdminInfo AdminInfo
        {
            get
            {
                return AdminInfoHelper.GetAdminInfo(); ;
            }
        }

        #region 審核狀態
        public int AuditState { get; set; }
        public string AuditStatus { get; set; }
        public bool IsInvalid { get; set; }
        #endregion
    }
}