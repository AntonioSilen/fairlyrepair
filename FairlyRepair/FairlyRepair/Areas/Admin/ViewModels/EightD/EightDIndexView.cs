﻿using FairlyRepair.Models;
using FairlyRepair.Models.Others;
using FairlyRepair.Repositories;
using FairlyRepair.Repositories.OracleRepositories;
using FairlyRepair.Repositories.T8Repositories;
using FairlyRepair.Utility.Helpers;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FairlyRepair.Areas.Admin.ViewModels.EightD
{
    public class EightDIndexView : PageQuery
    {
        public EightDIndexView()
        {
            this.Sorting = "CreateDate";
            this.IsDescending = true;
        }

        public PageResult<EightDView> PageResult { get; set; }
        public int ID { get; set; }

        [Required]
        [Display(Name = "流水號")]
        public string SerialNumber { get; set; }

        [Required]
        [Display(Name = "標題")]
        public string Title { get; set; }

        [Required]
        [Display(Name = "客戶")]
        public string Customer { get; set; }

        [Required]
        [Display(Name = "類別")]
        public int Category { get; set; }
        public string CategoryStr { get; set; }

        [Required]
        [Display(Name = "相關部門")]
        public string Depts { get; set; }

        [Required]
        [Display(Name = "費用")]
        public int Fee { get; set; }

        [Required]
        [Display(Name = "是否結案")]
        public bool? IsFinished { get; set; }

        [Required]
        [Display(Name = "建立時間")]
        public DateTime CreateDate { get; set; }


        [Display(Name = "是否撤銷")]
        public bool IsRevoke { get; set; }

        [Display(Name = "結案時間")]
        public DateTime? FinishDate { get; set; }

        [Required]
        [Display(Name = "建立者")]
        public string CreaterId { get; set; }

        public List<SelectListItem> CustomerOptions
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                CDPTRepository cDPTRepository = new CDPTRepository();
                var values = cDPTRepository.GetAll();

                //var values = EnumHelper.GetValues<AccountType>();
                foreach (var v in values)
                {
                    result.Add(new SelectListItem()
                    {
                        Text = v.COD_CUST,
                        Value = v.COD_CUST
                    });
                }
                return result;
            }
        }

        public List<SelectListItem> CategoryOptions
        {
            get
            {
                EightDCategoryRepository eightDCategoryRepository = new EightDCategoryRepository(new FairlyRepairDBEntities());
                List<SelectListItem> result = new List<SelectListItem>();
                var values = eightDCategoryRepository.Query(true, "", 1);
                foreach (var v in values)
                {
                    result.Add(new SelectListItem()
                    {
                        Text = v.Title,
                        Value = v.ID.ToString()
                    });
                }
                return result;
            }
        }

        public List<SelectListItem> DeptOptions
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                comPersonRepository compersonRepository = new comPersonRepository(new T8ERPEntities());
                var values = compersonRepository.GetPersonQuery().GroupBy(x => new { x.DeptId, x.DeptName }).ToList();
                foreach (var v in values)
                {
                    result.Add(new SelectListItem()
                    {
                        Text = v.Key.DeptName,
                        Value = (v.Key.DeptId).ToString()
                    });
                }

                return result;
            }
        }
    }
}