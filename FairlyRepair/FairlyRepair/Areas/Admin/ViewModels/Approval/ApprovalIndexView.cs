﻿using FairlyRepair.Models;
using FairlyRepair.Models.Others;
using FairlyRepair.Repositories;
using FairlyRepair.Repositories.T8Repositories;
using FairlyRepair.Utility.Helpers;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FairlyRepair.Areas.Admin.ViewModels.Approval
{
    public class ApprovalIndexView : PageQuery
    {
        public ApprovalIndexView()
        {
            this.Sorting = "CreateDate";
            this.IsDescending = true;
        }

        public PageResult<ApprovalView> PageResult { get; set; }

        [Display(Name = "編號")]
        public int ID { get; set; }

        [Display(Name = "課別")]
        public string DeptID { get; set; }

        [Display(Name = "廠區")]
        public int FactoryID { get; set; }

        [Display(Name = "樓層")]
        public int FloorID { get; set; }

        [Display(Name = "區域")]
        public int AreaID { get; set; }


        [Display(Name = "報修類型")]
        public int DeviceType { get; set; }

        [Display(Name = "設備")]
        public int DeviceID { get; set; }     

        [Display(Name = "總經理審核狀態")]
        public int? PresAppStatus { get; set; }

        [Display(Name = "需總經理審核")]
        public bool? IsPresApproval { get; set; }

        [Display(Name = "副總審核狀態")]
        public int? ViceAppStatus { get; set; }

        [Display(Name = "需副總審核")]
        public bool? IsViceApproval { get; set; }

        [Display(Name = "建立時間")]
        public DateTime CreateDate { get; set; }

        [Required]
        [Display(Name = "建立者")]
        public int Creater { get; set; }       

        public List<SelectListItem> FactoryOptions
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                var values = EnumHelper.GetValues<Factory>();
                foreach (var v in values)
                {
                    result.Add(new SelectListItem()
                    {
                        Value = ((int)v).ToString(),
                        Text = EnumHelper.GetDescription(v)
                    });
                }
                return result;
            }
        }

        DeviceRepository deviceRepository = new DeviceRepository(new FairlyRepairDBEntities());

        public List<SelectListItem> DeviceTypeOptions
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                if (this.DeviceType != 0)
                {
                    var values = deviceRepository.Query((int)DeviceClass.Main);
                    foreach (var v in values)
                    {
                        result.Add(new SelectListItem()
                        {
                            Value = v.ID.ToString(),
                            Text = v.Title
                        });
                    }
                }

                return result;
            }
        }

        public List<SelectListItem> DeviceOptions
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem()
                {
                    Value = "0",
                    Text = ""
                });
                if (this.DeviceType != 0)
                {                    
                    var values = deviceRepository.Query((int)DeviceClass.Second, this.DeviceType);
                    foreach (var v in values)
                    {
                        result.Add(new SelectListItem()
                        {
                            Value = v.ID.ToString(),
                            Text = v.Title
                        });
                    }
                }
               
                return result;
            }
        }

    }
}