﻿using FairlyRepair.Models;
using FairlyRepair.Repositories;
using FairlyRepair.Repositories.T8Repositories;
using FairlyRepair.Utility.Helpers;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace FairlyRepair.Areas.Admin.ViewModels.Approval
{
    public class ApprovalView
    {
        AdminRepository adminRepository = new AdminRepository(new FairlyRepairDBEntities());
        GroupPersonRepository groupPersonRepository = new GroupPersonRepository(new T8ERPEntities());
        RepairRepository repairRepository = new RepairRepository(new FairlyRepairDBEntities());
        ReplaceRepository replaceRepository = new ReplaceRepository(new FairlyRepairDBEntities());
        DeviceRepository deviceRepository = new DeviceRepository(new FairlyRepairDBEntities());

        [Required]
        [Display(Name = "ID")]
        public int ID { get; set; }

        [Required]
        [Display(Name = "廠區")]
        public int FactoryID { get; set; }
        public string FactoryTitle
        {
            get
            {
                return EnumHelper.GetDescription((Factory)this.FactoryID);
            }
        }

        [Required]
        [Display(Name = "設備類型")]
        public int DeviceType { get; set; }
        public string DeviceTypeName
        {
            get
            {
                return deviceRepository.FindBy(this.DeviceType).Title;
            }
        }


        [Required]
        [Display(Name = "設備")]
        public int DeviceID { get; set; }
        public string DeviceName
        {
            get
            {
                var ItemInfo = deviceRepository.FindBy(this.DeviceID);
                return ItemInfo == null ? "" : ItemInfo.Title;
            }
        }

        [Display(Name = "設備項目")]
        public int DeviceItem { get; set; }
        public string DeviceItemName
        {
            get
            {
                var ItemInfo = deviceRepository.FindBy(this.DeviceItem);
                return ItemInfo == null ? "" : ItemInfo.Title;
            }
        }

        [Required]
        [Display(Name = "問題敘述")]
        public string Description { get; set; }

        [Required]
        [Display(Name = "總經理審核狀態")]
        public int PresAppStatus { get; set; }

        [Required]
        [Display(Name = "副總審核狀態")]

        public int ViceAppStatus { get; set; }
        [Display(Name = "建立時間")]
        public DateTime CreateDate { get; set; }

        [Required]
        [Display(Name = "建立者")]
        public int Creater { get; set; }

        public string CreaterName
        {
            get
            {
                try
                {
                    if (this.Creater == 0)
                    {
                        this.Creater = AdminInfoHelper.GetAdminInfo().ID;
                    }
                    var jobID = adminRepository.GetById(this.Creater).Account;
                    var nameInfo = groupPersonRepository.FindByJobID(jobID);
                    if (nameInfo == null)
                    {
                        return adminRepository.GetById(this.Creater).Name;
                    }

                    return nameInfo.PersonName;
                }
                catch (Exception e)
                {
                    return "";
                    //return adminRepository.GetById(this.Creater).Name;
                }
            }
        }

    }
}