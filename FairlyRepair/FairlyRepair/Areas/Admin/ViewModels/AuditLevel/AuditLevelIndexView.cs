﻿using FairlyRepair.Models;
using FairlyRepair.Models.Others;
using FairlyRepair.Repositories.MeetRepositories;
using FairlyRepair.Utility.Helpers;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FairlyRepair.Areas.Admin.ViewModels.AuditLevel
{
    public class AuditLevelIndexView : PageQuery
    {
        public AdminRepository adminRepository = new AdminRepository(new MeetModels.MeetingSignInDBEntities());
        public Repositories.T8Repositories.DepartmentRepository t8departmentRepository = new Repositories.T8Repositories.DepartmentRepository(new T8ERPEntities());

        public AuditLevelIndexView()
        {
            this.Sorting = "DeptId";
        }
        public PageResult<AuditLevelView> PageResult { get; set; }

        [Required]
        [Display(Name = "編號")]
        public int ID { get; set; }

        [Required]
        [Display(Name = "部門編號")]
        public string DeptId { get; set; }

        [Required]
        [Display(Name = "部門")]
        public string DeptName { get; set; }

        [Required]
        [Display(Name = "總經理審核")]
        public string PresidentReview { get; set; }

        [Required]
        [Display(Name = "副總經理審核")]
        public string ViceReview { get; set; }

        [Required]
        [Display(Name = "主管審核")]
        public string ManagerReview { get; set; }

        [Required]
        [Display(Name = "啟用狀態")]
        public bool? IsOnline { get; set; }

        [Display(Name = "更新時間")]
        public DateTime UpdateDate { get; set; }

        public List<SelectListItem> PresidentOptions
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem()
                {
                    Value = "0000",
                    Text = "無須審核"
                });
                var values = adminRepository.Query(true, (int)MeetAdminType.President);
                foreach (var v in values)
                {
                    result.Add(new SelectListItem()
                    {
                        Value = v.Account,
                        Text = v.Name
                    });
                }
                return result;
            }
        }
        public List<SelectListItem> ViceOptions
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem()
                {
                    Value = "0000",
                    Text = "無須審核"
                });
                var values = adminRepository.Query(true, (int)MeetAdminType.Vice);
                foreach (var v in values)
                {
                    result.Add(new SelectListItem()
                    {
                        Value = v.Account,
                        Text = v.Name
                    });
                }
                return result;
            }
        }
        public List<SelectListItem> ManagerOptions
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem()
                {
                    Value = "0000",
                    Text = "無須審核"
                });
                var values = adminRepository.Query(true, (int)MeetAdminType.Manager);
                foreach (var v in values)
                {
                    result.Add(new SelectListItem()
                    {
                        Value = v.Account,
                        Text = v.Name
                    });
                }
                return result;
            }
        }
    }
}