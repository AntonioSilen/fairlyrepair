﻿using FairlyRepair.Models;
using FairlyRepair.Models.Others;
using FairlyRepair.Repositories;
using FairlyRepair.Utility.Helpers;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using static FairlyRepair.Areas.Admin.ViewModels.EquipmentCategory.EquipmentCategoryView;

namespace FairlyRepair.Areas.Admin.ViewModels.EquipmentCategory
{
    public class EquipmentCategoryIndexView : PageQuery
    {
        EquipmentCategoryRepository equipmentRepository = new EquipmentCategoryRepository(new Models.FairlyRepairDBEntities());

        public EquipmentCategoryIndexView()
        {
            this.Sorting = "Title";
            this.IsDescending = false;
        }

        public PageResult<EquipmentCategoryView> PageResult { get; set; }

        [Display(Name = "編號")]
        public int ID { get; set; }

        [Display(Name = "名稱")]
        public string Title { get; set; }

        [Display(Name = "啟用狀態")]
        public bool? IsOnline { get; set; }
    }
}