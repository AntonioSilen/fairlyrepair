﻿using FairlyRepair.Models;
using FairlyRepair.Repositories;
using FairlyRepair.Utility.Helpers;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FairlyRepair.Areas.Admin.ViewModels.EquipmentCategory
{
    public class EquipmentCategoryView
    {
        EquipmentCategoryRepository equipmentRepository = new EquipmentCategoryRepository(new Models.FairlyRepairDBEntities());

        public EquipmentCategoryView()
        {
            this.IsOnline = true;
        }

        [Required]
        [Display(Name = "編號")]
        public int ID { get; set; }

        [Required]
        [Display(Name = "名稱")]
        public string Title { get; set; }

        [Required]
        [Display(Name = "啟用狀態")]
        public bool IsOnline { get; set; }
    }
}