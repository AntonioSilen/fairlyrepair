﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FairlyRepair.Areas.Admin.ViewModels.ProductionType
{
    public class ProductionTypeView
    {
        [Display(Name = "項目代碼")]
        public string TYPE_NUM { get; set; }

        [Display(Name = "項目名稱")]
        public string TYPE_NAME { get; set; }

        [Display(Name = "類別")]
        public string TYPE_CATE { get; set; }

        public List<SelectListItem> CateOptions { get; set; }        
    }
}