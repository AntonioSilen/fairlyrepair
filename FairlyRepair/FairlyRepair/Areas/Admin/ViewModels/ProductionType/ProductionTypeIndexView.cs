﻿using FairlyRepair.Models.Others;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FairlyRepair.Areas.Admin.ViewModels.ProductionType
{
    public class ProductionTypeIndexView : PageQuery
    {
        public ProductionTypeIndexView()
        {
            this.Sorting = "TYPE_NUM";
            this.IsDescending = false;
        }

        public PageResult<ProductionTypeView> PageResult { get; set; }

        [Display(Name = "項目代碼")]
        public string TYPE_NUM { get; set; }

        [Display(Name = "項目名稱")]
        public string TYPE_NAME { get; set; }

        [Display(Name = "類別")]
        public int? Cate { get; set; }
        public string TYPE_CATE { get; set; }

        public List<SelectListItem> CateOptions { get; set; }
    }
}