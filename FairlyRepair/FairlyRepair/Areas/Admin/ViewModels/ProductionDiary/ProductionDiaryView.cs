﻿using FairlyRepair.Models.FAIRLYVIN;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FairlyRepair.Areas.Admin.ViewModels.ProductionDiary
{
    public class ProductionDiaryView
    {
        [Display(Name = "日期")]
        [DataType(DataType.DateTime)]
        public string DIARYDATE { get; set; }

        [Required]
        [Display(Name = "項目代碼")]
        [StringLength(4)]
        public string TYPE_NUM { get; set; }

        [Display(Name = "文字記錄")]
        [StringLength(1000)]
        public string DATE_NOTE { get; set; }

        [Display(Name = "圖檔")]
        public string DATE_PIC1 { get; set; }
        public List<W_DAYN_PIC> FilesList { get; set; }
        public List<HttpPostedFileBase> FilesFiles { get; set; }

        [Required]
        [Display(Name = "紀錄人員")]
        [StringLength(20)]
        public string ENTER_USER { get; set; }

        [Display(Name = "客戶")]
        [StringLength(10)]
        public string COD_CUST { get; set; }

        [Display(Name = "型號")]
        [StringLength(20)]
        public string NAM_ITEM { get; set; }

        [Display(Name = "當日次數")]
        [StringLength(4)]
        public string LIN_PSCT { get; set; }

        public List<SelectListItem> TypeOptions { get; set; }
        public List<SelectListItem> CustOptions { get; set; }
        //public List<SelectListItem> TypeOptions
        //{
        //    get
        //    {
        //        List<SelectListItem> result = new List<SelectListItem>();
        //        var values = EnumHelper.GetValues<MaintStatus>();
        //        foreach (var v in values)
        //        {
        //            result.Add(new SelectListItem()
        //            {
        //                Value = ((int)v).ToString(),
        //                Text = EnumHelper.GetDescription(v)
        //            });
        //        }
        //        return result;
        //    }
        //}
    }
}