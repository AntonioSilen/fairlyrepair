﻿using FairlyRepair.Models.Others;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FairlyRepair.Areas.Admin.ViewModels.ProductionDiary
{
    public class ProductionDiaryIndexView : PageQuery
    {
        public ProductionDiaryIndexView()
        {
            this.Sorting = "DIARYDATE";
            this.IsDescending = true;
        }

        public PageResult<ProductionDiaryView> PageResult { get; set; }
        //public List<ProductionDiaryIndexView> QueryData { get; set; }

        [Required]
        [Display(Name = "起始日期")]
        [DataType(DataType.DateTime)]
        public string StartDate { get; set; }

        [Required]
        [Display(Name = "結束日期")]
        [DataType(DataType.DateTime)]
        public string EndDate { get; set; }

        [Display(Name = "日期")]
        [DataType(DataType.DateTime)]
        public string DIARYDATE { get; set; }

        [Display(Name = "項目")]
        public int? Type { get; set; }
        public string TYPE_NUM { get; set; }

        [Display(Name = "文字記錄")]
        [StringLength(200)]
        public string DATE_NOTE { get; set; }

        [Display(Name = "圖檔紀錄")]
        public HttpPostedFileBase PicFile { get; set; }
        public string DATE_PIC1 { get; set; }

        [Display(Name = "紀錄人員")]
        [StringLength(20)]
        public string ENTER_USER { get; set; }

        [Display(Name = "客戶")]
        public int? Cust { get; set; }
        public string COD_CUST { get; set; }

        [Display(Name = "型號")]
        [StringLength(20)]
        public string NAM_ITEM { get; set; }

        [Display(Name = "當日次數")]
        [StringLength(4)]
        public string LIN_PSCT { get; set; }

        public List<SelectListItem> TypeOptions { get; set; }
        public List<SelectListItem> CustOptions { get; set; }
    }
}