﻿using FairlyRepair.Models;
using FairlyRepair.Repositories;
using FairlyRepair.Repositories.T8Repositories;
using FairlyRepair.Utility.Helpers;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FairlyRepair.Areas.Admin.ViewModels.Manufacture
{
    public class ManufactureView
    {
        [Required]
        [Display(Name = "ID")]
        public int ID { get; set; }

        [Required]
        [Display(Name = "名稱")]
        public string Title { get; set; }

        [Required]
        [Display(Name = "廠商類型")]
        public int Type { get; set; }

        [Required]
        [Display(Name = "啟用狀態")]
        public bool IsOnline { get; set; }
   
        public List<SelectListItem> TypeOptions
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                var values = EnumHelper.GetValues<ManufactureType>();
                foreach (var v in values)
                {
                    result.Add(new SelectListItem()
                    {
                        Value = ((int)v).ToString(),
                        Text = EnumHelper.GetDescription(v)
                    });
                }
                return result;
            }
        }
    }
}