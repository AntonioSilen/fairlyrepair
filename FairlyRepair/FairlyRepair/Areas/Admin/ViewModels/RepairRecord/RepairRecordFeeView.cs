﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace FairlyRepair.Areas.Admin.ViewModels.RepairRecord
{
    public class RepairRecordFeeView
    {
        [Display(Name = "編號")]
        public int ID { get; set; }

        [Display(Name = "報修紀錄編號")]
        public int RecordID { get; set; }

        [Display(Name = "項目")]
        public string Title { get; set; }

        [Display(Name = "描述")]
        public string Description { get; set; }

        [Display(Name = "金額")]
        public int Fee { get; set; }

        [Display(Name = "標籤")]
        public string Tags { get; set; }
    }
}