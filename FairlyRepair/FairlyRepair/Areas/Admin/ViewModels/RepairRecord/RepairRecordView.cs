﻿using FairlyRepair.Models;
using FairlyRepair.Repositories;
using FairlyRepair.Repositories.T8Repositories;
using FairlyRepair.Utility.Helpers;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FairlyRepair.Areas.Admin.ViewModels.RepairRecord
{
    public class RepairRecordView
    {
        AdminRepository adminRepository = new AdminRepository(new FairlyRepairDBEntities());
        PersonRepository personRepository = new PersonRepository(new T8ERPEntities());
        GroupPersonRepository groupPersonRepository = new GroupPersonRepository(new T8ERPEntities());

        [Required]
        [Display(Name = "ID")]
        public int ID { get; set; }

        [Required]
        [Display(Name = "報修單編號")]
        public int RepairID { get; set; }

        [Required]
        [Display(Name = "維修狀況")]
        public int MaintStatus { get; set; }

        [Display(Name = "說明")]
        public string Description { get; set; }

        [Display(Name = "費用")]
        public int Fee { get; set; }

        [Required]
        [Display(Name = "狀況是否排除")]
        public bool IsSolved { get; set; }

        [Required]
        [Display(Name = "建立時間")]
        public DateTime CreateDate { get; set; }

        [Required]
        [Display(Name = "建立者")]
        public int Creater { get; set; }
        public string CreaterName
        {
            get
            {
                try
                {
                    if (this.Creater == 0)
                    {
                        this.Creater = AdminInfoHelper.GetAdminInfo().ID;
                    }
                    var jobID = adminRepository.GetById(this.Creater).Account;
                    var nameInfo = groupPersonRepository.FindByJobID(jobID);

                    return nameInfo == null ? "管理員" : nameInfo.PersonName;
                }
                catch (Exception e)
                {
                    return adminRepository.GetById(this.Creater).Name;
                }
            }
        }

        [Display(Name = "收據照片")]
        public string ReceiptPics { get; set; }
        public List<RepairRecordReceipt> ReceiptPicList { get; set; }
        public List<HttpPostedFileBase> ReceiptPicsFiles { get; set; }

        [Display(Name = "維修處理照片")]
        public string Pics { get; set; }
        public List<RepairRecordImage> PicList { get; set; }
        public List<HttpPostedFileBase> PicsFiles { get; set; }

        public List<SelectListItem> MaintStatusOptions
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                var values = EnumHelper.GetValues<MaintStatus>();
                foreach (var v in values)
                {
                    result.Add(new SelectListItem()
                    {
                        Value = ((int)v).ToString(),
                        Text = EnumHelper.GetDescription(v)
                    });
                }
                return result;
            }
        }

        [Display(Name = "確認完工")]
        public bool IsFinish { get; set; }

        [Display(Name = "標籤")]
        public string TagsValue { get; set; }

        public string JsonFee { get; set; }
        public List<RepairRecordFeeView> FeeList { get; set; }
    }
}