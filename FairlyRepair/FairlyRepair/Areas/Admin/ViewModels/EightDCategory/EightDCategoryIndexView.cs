﻿using FairlyRepair.Models;
using FairlyRepair.Models.Others;
using FairlyRepair.Utility.Helpers;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FairlyRepair.Areas.Admin.ViewModels.EightDCategory
{
    public class EightDCategoryIndexView : PageQuery
    {
        public EightDCategoryIndexView()
        {
            this.Sorting = "Type";
            this.IsDescending = false;
        }

        public PageResult<EightDCategoryView> PageResult { get; set; }

        [Display(Name = "ID")]
        public int ID { get; set; }

        [Display(Name = "標題")]
        public string Title { get; set; }

        [Display(Name = "類型")]
        public int Type { get; set; }

        [Display(Name = "啟用狀態")]
        public bool? IsOnline { get; set; }
    }
}