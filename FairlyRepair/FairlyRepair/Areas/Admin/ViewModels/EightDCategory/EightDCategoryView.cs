﻿using FairlyRepair.Models;
using FairlyRepair.Utility.Helpers;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FairlyRepair.Areas.Admin.ViewModels.EightDCategory
{
    public class EightDCategoryView
    {
        [Required]
        [Display(Name = "ID")]
        public int ID { get; set; }

        [Required]
        [Display(Name = "標題")]
        public string Title { get; set; }

        [Required]
        [Display(Name = "類型")]
        public int Type { get; set; }

        [Required]
        [Display(Name = "啟用狀態")]
        public bool IsOnline { get; set; }
    }
}