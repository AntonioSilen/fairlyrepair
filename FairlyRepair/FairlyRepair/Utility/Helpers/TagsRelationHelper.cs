﻿using FairlyRepair.Models;
using FairlyRepair.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FairlyRepair.Utility.Helpers
{
    public class TagsRelationHelper
    {
        private static TagsRelationRepository tagsRelationRepository = new TagsRelationRepository(new FairlyRepairDBEntities());
        private static TagsRepository tagsRepository = new TagsRepository(new FairlyRepairDBEntities());

        public static bool AddTagRelation(int id, string tagsStr, int type)
        {
            try
            {
                if (!string.IsNullOrEmpty(tagsStr))
                {
                    var newTagList = tagsStr.Split(',');
                    var oldTagList = tagsRelationRepository.Query(id, type).ToList();
                    foreach (var item in oldTagList)
                    {
                        tagsRelationRepository.Delete(item.ID);
                    }
                    if (newTagList.Count() > 0)
                    {
                        for (int i = 1; i <= newTagList.Count(); i++)
                        {
                            var thisTag = newTagList[i - 1];
                            //新標籤
                            if (tagsRepository.GetAll().Where(t => t.TagName == thisTag).Count() <= 0)
                            {
                                Tags tag = new Tags()
                                {
                                    TagName = thisTag,
                                    UpdateDate = DateTime.Now,
                                    Updater = AdminInfoHelper.GetAdminInfo().ID,
                                    CreateDate = DateTime.Now,
                                    Creater = AdminInfoHelper.GetAdminInfo().ID,
                                };
                                tagsRepository.Insert(tag);
                            }

                            TagsRelation newTag = new TagsRelation()
                            {
                                TargetType = type,
                                TargetID = id,
                                TagID = tagsRepository.GetAll().Where(t => t.TagName == thisTag).FirstOrDefault().ID,
                                TagName = thisTag,
                                UpdateDate = DateTime.Now,
                                Updater = AdminInfoHelper.GetAdminInfo().ID,
                                CreateDate = DateTime.Now,
                                Creater = AdminInfoHelper.GetAdminInfo().ID
                            };

                            tagsRelationRepository.Insert(newTag);

                            //if (oldTagList.Count() >= i)
                            //{
                            //    TagsRelation oldTag = tagsRelationRepository.GetById(oldTagList[i - 1].ID);
                            //    oldTag.TagID = tagsRepository.GetAll().Where(t => t.TagName == thisTag).FirstOrDefault().ID;
                            //    oldTag.TagName = thisTag;
                            //    oldTag.UpdateDate = DateTime.Now;
                            //    oldTag.Updater = AdminInfoHelper.GetAdminInfo().ID;
                            //    tagsRelationRepository.Update(oldTag);
                            //}
                            //else
                            //{
                            //    TagsRelation newTag = new TagsRelation()
                            //    {
                            //        TargetType = type,
                            //        TargetID = id,
                            //        TagID = tagsRepository.GetAll().Where(t => t.TagName == thisTag).FirstOrDefault().ID,
                            //        TagName = thisTag,
                            //        UpdateDate = DateTime.Now,
                            //        Updater = AdminInfoHelper.GetAdminInfo().ID,
                            //        CreateDate = DateTime.Now,
                            //        Creater = AdminInfoHelper.GetAdminInfo().ID
                            //    };

                            //    tagsRelationRepository.Insert(newTag);
                            //}
                        }
                        //if (oldTagList.Count() > newTagList.Count())
                        //{
                        //    for (int r = newTagList.Count(); r < (oldTagList.Count() - newTagList.Count()); r++)
                        //    {
                        //        tagsRelationRepository.Delete(r);
                        //    }
                        //}
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public static string GetTagRelationStr(int id, int type)
        {
            string tagStr = "";
            var tagList = tagsRelationRepository.Query(id, type);
            if (tagList.Count() > 0)
            {
                foreach (var item in tagList)
                {
                    tagStr += $"{item.TagName},";
                }
            }
            return tagStr;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="tagvalue"></param>
        /// <param name="type">1.新聞資訊  2.新聞專欄  3.活動情報  4.進修課程  5.影音  6.票券</param>
        /// <returns></returns>
        public static List<TagsRelation> GetArticleByTag(string tagvalue, int type = 0)
        {
            var articleList = tagsRelationRepository.GetAll().Where(t => t.TagName == tagvalue).ToList();

            return articleList;
        }

        public static bool DeleteTagRelation(int id, int type)
        {
            try
            {
                var tagRelationList = tagsRelationRepository.Query(id, type);
                foreach (var item in tagRelationList)
                {
                    tagsRelationRepository.Delete(item.ID);
                }
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}