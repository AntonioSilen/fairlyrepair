﻿using FairlyRepair.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FairlyRepair.Repositories
{
    public class TagsRelationRepository : GenericRepository<TagsRelation>
    {
        public TagsRelationRepository(FairlyRepairDBEntities context) : base(context) { }

        public IQueryable<TagsRelation> Query(int targetId, int targetType)
        {
            var query = GetAll();
            if (targetId != 0)
            {
                query = query.Where(q => q.TargetID == targetId && q.TargetType == targetType);
            }
            return query;
        }
    }
}