﻿using FairlyRepair.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FairlyRepair.Repositories
{
    public class RepairRepository : GenericRepository<Repair>
    {
        public RepairRepository(FairlyRepairDBEntities context) : base(context) { }
        private FairlyRepairDBEntities db = new FairlyRepairDBEntities();

        public IQueryable<Repair> Query(bool? isPres = null, bool? isVice = null, 
            int? presStatus = null, int? viceStatus = null, int deviceType = 0, 
            int deviceId = 0, int factory = 0, int floor = 0, int area = 0, string deptId = "", bool? isRevoke = null)
        {
            var query = GetAll();
            if (isPres.HasValue)
            {
                query = query.Where(q => q.IsPresApproval == isPres);
            }
            if (isVice.HasValue)
            {
                query = query.Where(q => q.IsViceApproval == isVice);
            }
            if (presStatus.HasValue)
            {
                query = query.Where(q => q.PresAppStatus == presStatus);
            }
            if (viceStatus.HasValue)
            {
                query = query.Where(q => q.ViceAppStatus == viceStatus);
            }
            if (isRevoke.HasValue)
            {
                query = query.Where(q => q.IsRevoke == isRevoke);
            }
            if (deviceType != 0)
            {
                query = query.Where(q => q.DeviceType == deviceType);
            }
            if (deviceId != 0)
            {
                query = query.Where(q => q.DeviceID == deviceId);
            }
            if (factory != 0)
            {
                query = query.Where(q => q.FactoryID == factory);
            }
            if (floor != 0)
            {
                query = query.Where(q => q.FloorID == floor);;
            }
            if (area != 0)
            {
                query = query.Where(q => q.AreaID == area);
            }
            if (!string.IsNullOrEmpty(deptId))
            {
                query = query.Where(q => q.DeptID == deptId);
            }
       
            return query;
        }

        public Repair FindBy(int id)
        {
            return db.Repair.Find(id);
        }

        public void DeleteFile(int id)
        {
            Repair data = FindBy(id);

            db.SaveChanges();
        }

        public IQueryable<Repair> GetNumAndPercentageQuery(DateTime start, DateTime end, int factoryId = 0, int floorId = 0, int areaId = 0, int deviceId = 0, string type = "", bool? isFinished = null)
        {
            var query = GetAll().Where(q => q.IsRevoke == false && q.DeviceType == (int)DeviceType.All);
            var ckList = query.ToList();
            if (factoryId != 0)
            {
                query = query.Where(q => q.FactoryID == factoryId);
            }
            if (floorId != 0)
            {
                query = query.Where(q => q.FloorID == floorId);
            }
            if (areaId != 0)
            {
                query = query.Where(q => q.AreaID == areaId);
            }
            if (deviceId != 0)
            {
                query = query.Where(q => q.DeviceItem == deviceId);
            }
            ckList = query.ToList();
            if (start >= Convert.ToDateTime("2000/01/01") && end >= Convert.ToDateTime("2000/01/01"))
            {
                query = query.Where(q => q.CreateDate >= start && q.CreateDate <= end);
            }
            if (isFinished.HasValue)
            {
                var recordList = db.RepairRecord.Where(q => q.IsSolved).Select(q => q.RepairID).Distinct().ToList();
                if (isFinished == true)
                {
                    query = query.Where(q => recordList.Contains(q.ID));
                }
                else
                {
                    query = query.Where(q => !recordList.Contains(q.ID));
                }
            }
            return query;
        }

        public List<NumAndPercentageModel> GetNumAndPercentage(DateTime start, DateTime end, int factoryId = 0, int floorId = 0, int areaId = 0, int deviceId = 0, string type = "", bool? isFinished = null)
        {
            var query = GetNumAndPercentageQuery(start, end, factoryId, floorId, areaId, deviceId, type, isFinished);
            List<NumAndPercentageModel> data = new List<NumAndPercentageModel>();
            switch (type)
            {
                case "factory":
                    return query.GroupBy(d => d.FactoryID)
                          .SelectMany(grp => grp
                          .Select(row => new NumAndPercentageModel()
                          {
                              Title = row.FactoryID.ToString(),
                              Num = grp.Count(),
                              CateCount = grp.Count(),
                              AllCount = query.Count()
                          })
                      ).Distinct().ToList();
                case "floor":
                    return query.GroupBy(d => d.FloorID)
                        .SelectMany(grp => grp
                        .Select(row => new NumAndPercentageModel()
                        {
                            Title = row.FloorID.ToString(),
                            Num = grp.Count(),
                            CateCount = grp.Count(),
                            AllCount = query.Count()
                        })
                    ).Distinct().ToList();
                case "area":
                    return query.GroupBy(d => d.AreaID)
                       .SelectMany(grp => grp
                       .Select(row => new NumAndPercentageModel()
                       {
                           Title = row.AreaID.ToString(),
                           Num = grp.Count(),
                           CateCount = grp.Count(),
                           AllCount = query.Count()
                       })
                   ).Distinct().ToList();
                case "device":
                    return query.GroupBy(d => d.DeviceID)
                        .SelectMany(grp => grp
                        .Select(row => new NumAndPercentageModel()
                        {
                            Title = row.DeviceID.ToString(),
                            Num = grp.Count(),
                            CateCount = grp.Count(),
                            AllCount = query.Count()
                        })
                    ).Distinct().ToList();
            }

            return data;
        }

        public class NumAndPercentageModel
        {
            public string Title { get; set; }
            public double DoubleTitle
            {
                get
                {
                    try
                    {
                        return Convert.ToDouble(this.Title);
                    }
                    catch (Exception)
                    {
                        return 0;
                    }
                }
            }
            public int Num { get; set; }

            public int AllCount { get; set; }
            public int CateCount { get; set; }

            public string Percentage
            {
                get
                {
                    return (100.0 * this.CateCount / this.AllCount).ToString("0.00");
                }
            }
        }
    }
}