﻿using FairlyRepair.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FairlyRepair.Repositories
{
    public class EmissionsT3C1Repository : GenericRepository<EmissionsT3C1>
    {
        public EmissionsT3C1Repository(FairlyRepairDBEntities context) : base(context) { }
        private FairlyRepairDBEntities db = new FairlyRepairDBEntities();

        public IQueryable<EmissionsT3C1> Query(int emissionsId = 0)
        {
            var query = GetAll();
            if (emissionsId != 0)
            {
                query = query.Where(q => q.EmissionID == emissionsId);
            }

            return query;
        }

        public EmissionsT3C1 GetByEmissionsId(int id)
        {
            return Query(id).FirstOrDefault();
        }

        public EmissionsT3C1 FindBy(int id)
        {
            return db.EmissionsT3C1.Find(id);
        }

        public void DeleteFile(int id)
        {
            EmissionsT3C1 data = FindBy(id);

            db.SaveChanges();
        }
    }
}