﻿using FairlyRepair.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FairlyRepair.Repositories
{
    public class RepairImageRepository : GenericRepository<RepairImage>
    {
        public RepairImageRepository(FairlyRepairDBEntities context) : base(context) { }
        private FairlyRepairDBEntities db = new FairlyRepairDBEntities();

        public IQueryable<RepairImage> Query(int rid = 0)
        {
            var query = GetAll();
            if (rid != 0)
            {
                query = query.Where(q => q.RepairID == rid);
            }
       
            return query;
        }

        public RepairImage FindBy(int id)
        {
            return db.RepairImage.Find(id);
        }

        public void DeleteFile(int id)
        {
            RepairImage data = FindBy(id);

            db.SaveChanges();
        }
    }
}