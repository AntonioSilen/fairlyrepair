﻿using FairlyRepair.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FairlyRepair.Repositories
{
    public class EightDFileRepository : GenericRepository<EightDFile>
    {
        public EightDFileRepository(FairlyRepairDBEntities context) : base(context) { }
        private FairlyRepairDBEntities db = new FairlyRepairDBEntities();

        public IQueryable<EightDFile> Query(int eid = 0)
        {
            var query = GetAll();
            if (eid != 0)
            {
                query = query.Where(q => q.EightDID == eid);
            }
       
            return query;
        }

        public EightDFile FindBy(int id)
        {
            return db.EightDFile.Find(id);
        }

        public void DeleteFile(int id)
        {
            EightDFile data = FindBy(id);

            db.SaveChanges();
        }
    }
}