﻿using FairlyRepair.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FairlyRepair.Repositories
{
    public class TagsRepository : GenericRepository<Tags>
    {
        public TagsRepository(FairlyRepairDBEntities context) : base(context) { }

        public IQueryable<Tags> Query()
        {
            var query = GetAll();
            return query;
        }

        public Tags FindByName(string name)
        {
            return GetAll().Where(q => q.TagName == name).FirstOrDefault();
        }
    }
}