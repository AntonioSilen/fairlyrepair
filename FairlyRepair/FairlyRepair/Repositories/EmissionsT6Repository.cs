﻿using FairlyRepair.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FairlyRepair.Repositories
{
    public class EmissionsT6Repository : GenericRepository<EmissionsT6>
    {
        public EmissionsT6Repository(FairlyRepairDBEntities context) : base(context) { }
        private FairlyRepairDBEntities db = new FairlyRepairDBEntities();

        public IQueryable<EmissionsT6> Query(int emissionsId = 0)
        {
            var query = GetAll();
            if (emissionsId != 0)
            {
                query = query.Where(q => q.EmissionID == emissionsId);
            }

            return query;
        }

        public EmissionsT6 GetByEmissionsId(int id)
        {
            return Query(id).FirstOrDefault();
        }

        public EmissionsT6 FindBy(int id)
        {
            return db.EmissionsT6.Find(id);
        }

        public void DeleteFile(int id)
        {
            EmissionsT6 data = FindBy(id);

            db.SaveChanges();
        }
    }
}