﻿using FairlyRepair.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FairlyRepair.Repositories
{
    public class EmissionsRepository : GenericRepository<Emissions>
    {
        public EmissionsRepository(FairlyRepairDBEntities context) : base(context) { }
        private FairlyRepairDBEntities db = new FairlyRepairDBEntities();

        public IQueryable<Emissions> Query(DateTime? startDate = null, DateTime? endDate = null )
        {
            var query = GetAll();

            if (startDate.HasValue)
            {
                query = query.Where(q => q.CreateDate >= startDate);
            }
            if (endDate.HasValue)
            {
                query = query.Where(q => q.CreateDate <= endDate);
            }
       
            return query;
        }

        public Emissions FindBy(int id)
        {
            return db.Emissions.Find(id);
        }

        public void DeleteFile(int id)
        {
            Emissions data = FindBy(id);

            db.SaveChanges();
        }
    }
}