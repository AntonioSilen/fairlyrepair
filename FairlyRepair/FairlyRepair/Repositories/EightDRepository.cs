﻿using FairlyRepair.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FairlyRepair.Repositories
{
    public class EightDRepository : GenericRepository<EightD>
    {
        public EightDRepository(FairlyRepairDBEntities context) : base(context) { }
        private readonly FairlyRepairDBEntities db = new FairlyRepairDBEntities();

        public IQueryable<EightD> Query(bool? status, string title, int category = 0, string customer = "", string depts = "", bool? isRevoke = null)
        {
            var query = GetAll();
            if (status.HasValue)
            {
                query = query.Where(q => q.IsFinished == status);
            }       
            if (!string.IsNullOrEmpty(title))
            {
                query = query.Where(q => q.Title.Contains(title));
            }
            if (category != 0)
            {
                query = query.Where(q => q.Category == category);
            }
            if (!string.IsNullOrEmpty(customer))
            {
                query = query.Where(q => q.Customer == customer);
            }
            if (!string.IsNullOrEmpty(depts))
            {
                var deptList = depts.Split(',').ToList();
                //query = query.Where(q => q.Depts == depts);
                query = query.Where(q => deptList.Any(s => q.Depts.Contains(s)));
            }
            if (isRevoke.HasValue)
            {
                query = query.Where(q => q.IsRevoke == isRevoke);
            }
          
            return query;
        }
    }
}