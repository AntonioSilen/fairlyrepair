﻿using FairlyRepair.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FairlyRepair.Repositories
{
    public class RepairRecordReceiptRepository : GenericRepository<RepairRecordReceipt>
    {
        public RepairRecordReceiptRepository(FairlyRepairDBEntities context) : base(context) { }
        private FairlyRepairDBEntities db = new FairlyRepairDBEntities();

        public IQueryable<RepairRecordReceipt> Query(int rid = 0)
        {
            var query = GetAll();
            if (rid != 0)
            {
                query = query.Where(q => q.RecordID == rid);
            }
       
            return query;
        }

        public RepairRecordReceipt FindBy(int id)
        {
            return db.RepairRecordReceipt.Find(id);
        }

        public void DeleteFile(int id)
        {
            RepairRecordReceipt data = FindBy(id);

            db.SaveChanges();
        }
    }
}