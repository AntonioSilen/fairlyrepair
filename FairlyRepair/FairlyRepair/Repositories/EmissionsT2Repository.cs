﻿using FairlyRepair.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FairlyRepair.Repositories
{
    public class EmissionsT2Repository : GenericRepository<EmissionsT2>
    {
        public EmissionsT2Repository(FairlyRepairDBEntities context) : base(context) { }
        private FairlyRepairDBEntities db = new FairlyRepairDBEntities();

        public IQueryable<EmissionsT2> Query(int emissionsId = 0)
        {
            var query = GetAll();
            var a = query.ToList();
            if (emissionsId != 0)
            {
                query = query.Where(q => q.EmissionID == emissionsId);
            }
            a = query.ToList();

            return query;
        }

        public EmissionsT2 FindBy(int id)
        {
            return db.EmissionsT2.Find(id);
        }

        public EmissionsT2 GetByEmissionsId(int id)
        {
            return Query(id).FirstOrDefault();
        }

        public void DeleteFile(int id)
        {
            EmissionsT2 data = FindBy(id);

            db.SaveChanges();
        }
    }
}