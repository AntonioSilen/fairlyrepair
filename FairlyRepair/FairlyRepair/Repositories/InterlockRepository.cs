﻿using FairlyRepair.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FairlyRepair.Repositories
{
    public class InterlockRepository : GenericRepository<Interlock>
    {
        public InterlockRepository(FairlyRepairDBEntities context) : base(context) { }

        public IQueryable<Interlock> Query()
        {
            var query = GetAll();

            return query;
        }
    }
}