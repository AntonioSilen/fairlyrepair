﻿using FairlyRepair.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FairlyRepair.Repositories
{
    public class ManufactureRepository : GenericRepository<Manufacture>
    {
        public ManufactureRepository(FairlyRepairDBEntities context) : base(context) { }
        private FairlyRepairDBEntities db = new FairlyRepairDBEntities();

        public IQueryable<Manufacture> Query(bool? isOnline = null)
        {
            var query = GetAll();
            if (isOnline.HasValue)
            {
                query = query.Where(q => q.IsOnline == isOnline);
            }
       
            return query;
        }

        public Manufacture FindBy(int id)
        {
            return db.Manufacture.Find(id);
        }

        public void DeleteFile(int id)
        {
            Manufacture data = FindBy(id);

            db.SaveChanges();
        }
    }
}