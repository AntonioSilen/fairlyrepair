﻿using FairlyRepair.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FairlyRepair.Repositories
{
    public class RepairRecordRepository : GenericRepository<RepairRecord>
    {
        public RepairRecordRepository(FairlyRepairDBEntities context) : base(context) { }
        private FairlyRepairDBEntities db = new FairlyRepairDBEntities();

        public IQueryable<RepairRecord> Query(bool? solved = null, int repairId = 0, List<int> haveFeeIdList = null)
        {
            var query = GetAll();
            if (solved.HasValue)
            {
                query = query.Where(q => q.IsSolved == solved);
            }
            if (repairId != 0)
            {
                query = query.Where(q => q.RepairID == repairId);
            }
            if (haveFeeIdList != null && haveFeeIdList.Count() > 0)
            {
                query = query.Where(q => haveFeeIdList.Contains(q.ID));
            }
       
            return query;
        }

        public RepairRecord FindBy(int id)
        {
            return db.RepairRecord.Find(id);
        }

        public void DeleteFile(int id)
        {
            RepairRecord data = FindBy(id);

            db.SaveChanges();
        }
    }
}