﻿using FairlyRepair.Areas.Admin.ViewModels.RepairRecord;
using FairlyRepair.Models;
using FairlyRepair.Repositories;
using FairlyRepair.Repositories.T8Repositories;
using FairlyRepair.Utility.Helpers;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FairlyRepair.ViewModels.Repair
{
    public class RepairRecordIndexView
    {
        AdminRepository adminRepository = new AdminRepository(new FairlyRepairDBEntities());
        PersonRepository personRepository = new PersonRepository(new T8ERPEntities());
        GroupPersonRepository groupPersonRepository = new GroupPersonRepository(new T8ERPEntities());

        public RepairRecordIndexView()
        {

        }

        public Areas.Admin.ViewModels.Repair.RepairView RepairInfo { get; set; }

        public List<RepairRecordView> RecordList { get; set; }

        [Display(Name = "維護人員")]
        public string SearchMaintStaff { get; set; }

        [Required]
        [Display(Name = "報修單編號")]
        public int RepairID { get; set; }

        //[Required]
        //[Display(Name = "維護人員(編號)")]
        //public int MaintStaff { get; set; }

        [Required]
        [Display(Name = "維修狀況")]
        public int MaintStatus { get; set; }

        [Required]
        [Display(Name = "狀況是否排除")]
        public bool? IsSolved { get; set; }

        [Required]
        [Display(Name = "建立時間")]
        public DateTime CreateDate { get; set; }

        [Required]
        [Display(Name = "建立者")]
        public int Creater { get; set; }
        public string CreaterName
        {
            get
            {
                try
                {
                    if (this.Creater == 0)
                    {
                        this.Creater = MemberInfoHelper.GetMemberInfo().ID;
                    }
                    var jobID = adminRepository.GetById(this.Creater).Account;
                    var nameInfo = groupPersonRepository.FindByJobID(jobID);

                    return nameInfo.PersonName;
                }
                catch (Exception e)
                {
                    return adminRepository.GetById(this.Creater).Name;
                }
            }
        }

        public List<SelectListItem> MaintStatusOptions
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                var values = EnumHelper.GetValues<MaintStatus>();
                foreach (var v in values)
                {
                    result.Add(new SelectListItem()
                    {
                        Value = ((int)v).ToString(),
                        Text = EnumHelper.GetDescription(v)
                    });
                }
                return result;
            }
        }
        public List<SelectListItem> StaffOptions
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                var values = EnumHelper.GetValues<MaintStatus>();
                foreach (var v in values)
                {
                    result.Add(new SelectListItem()
                    {
                        Value = ((int)v).ToString(),
                        Text = EnumHelper.GetDescription(v)
                    });
                }
                return result;
            }
        }
    }
}