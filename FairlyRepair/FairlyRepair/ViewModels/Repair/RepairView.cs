﻿using FairlyRepair.Models;
using FairlyRepair.Repositories;
using FairlyRepair.Repositories.T8Repositories;
using FairlyRepair.Utility.Helpers;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FairlyRepair.ViewModels.Repair
{
    public class RepairView
    {
        AdminRepository adminRepository = new AdminRepository(new FairlyRepairDBEntities());
        PersonRepository personRepository = new PersonRepository(new T8ERPEntities());
        GroupPersonRepository groupPersonRepository = new GroupPersonRepository(new T8ERPEntities());
        DeviceRepository deviceRepository = new DeviceRepository(new FairlyRepairDBEntities());
        EquipmentRepository equipmentRepository = new EquipmentRepository(new FairlyRepairDBEntities());

        public List<FairlyRepair.Models.RepairRecord> RecordList { get; set; }

        [Required]
        [Display(Name = "ID")]
        public int ID { get; set; }

        [Required]
        [Display(Name = "課別")]
        public string DeptID { get; set; }
        public string DepartmentTitle
        {
            get
            {
                return personRepository.FindDepartments(this.DeptID).DeptName;
            }
        }

        [Required]
        [Display(Name = "廠區")]
        public int FactoryID { get; set; }
        public string FactoryTitle
        {
            get
            {
                var data = equipmentRepository.GetById(this.FactoryID);
                return data == null ? "" : data.Title;
                //return EnumHelper.GetDescription((Factory)this.FactoryID);
            }
        }


        [Required]
        [Display(Name = "樓層")]
        public int FloorID { get; set; }
        public string FloorTitle
        {
            get
            {
                var data = equipmentRepository.GetById(this.FloorID);
                return data == null ? "" : data.Title;
            }
        }

        [Required]
        [Display(Name = "區域")]
        public int AreaID { get; set; }
        public string AreaTitle
        {
            get
            {
                var data = equipmentRepository.GetById(this.AreaID);
                return data == null ? "" : data.Title;
            }
        }

        [Required]
        [Display(Name = "設備類型")]
        public int DeviceType { get; set; }
        public string DeviceTypeName
        {
            get
            {
                return deviceRepository.FindBy(this.DeviceType).Title;
            }
        }


        [Required]
        [Display(Name = "設備")]
        public int DeviceID { get; set; }
        public string DeviceName
        {
            get
            {
                var data = equipmentRepository.GetById(this.DeviceID);
                return data == null ? "" : data.Title;
                //return deviceRepository.FindBy(this.DeviceID).Title;
            }
        }

        [Display(Name = "設備項目")]
        public int DeviceItem { get; set; }
        public string DeviceItemName
        {
            get
            {
                var ItemInfo = deviceRepository.FindBy(this.DeviceItem);
                return ItemInfo == null ? "" : ItemInfo.Title;
            }
        }

        [Required]
        [Display(Name = "問題敘述")]
        public string Description { get; set; }

        [Display(Name = "事故照片")]
        public string Pics { get; set; }
        public List<RepairImage> PicList { get; set; }
        public List<HttpPostedFileBase> PicsFiles { get; set; }

        [Required]
        [Display(Name = "故障現況")]
        public int FaultState { get; set; }

        [Required]
        [Display(Name = "調度員")]
        public int DispatcherID { get; set; }

        [Display(Name = "維修人員")]
        public string MaintID { get; set; }

        [Display(Name = "送審原因")]
        public string SubmitReason { get; set; }

        [Required]
        [Display(Name = "總經理審核狀態")]
        public int PresAppStatus { get; set; }

        [Required]
        [Display(Name = "需總經理審核")]
        public bool IsPresApproval { get; set; }

        [Display(Name = "總經理審核日期")]
        public DateTime? PresAppDate { get; set; }

        [Display(Name = "總經理審核結果說明")]
        public string PresResult { get; set; }

        [Required]
        [Display(Name = "副總審核狀態")]
        public int ViceAppStatus { get; set; }

        [Required]
        [Display(Name = "需副總審核")]
        public bool IsViceApproval { get; set; }

        [Display(Name = "副總審核日期")]
        public DateTime? ViceAppDate { get; set; }

        [Display(Name = "副總審核結果說明")]
        public string ViceResult { get; set; }

        [Required]
        [Display(Name = "撤銷狀態")]
        public bool IsRevoke { get; set; }

        [Required]
        [Display(Name = "更新時間")]
        public DateTime UpdateDate { get; set; }

        [Required]
        [Display(Name = "更新者")]
        public int Updater { get; set; }

        [Required]
        [Display(Name = "建立時間")]
        public DateTime CreateDate { get; set; }

        [Required]
        [Display(Name = "建立者")]
        public int Creater { get; set; }

        public string CreaterName
        {
            get
            {
                try
                {
                    if (this.Creater == 0)
                    {
                        this.Creater = MemberInfoHelper.GetMemberInfo().ID;
                    }
                    var jobID = adminRepository.GetById(this.Creater).Account;
                    var nameInfo = groupPersonRepository.FindByJobID(jobID);
                    if (nameInfo == null)
                    {
                        return adminRepository.GetById(this.Creater).Name;
                    }

                    return nameInfo.PersonName;
                }
                catch (Exception e)
                {
                    return "";
                    //return adminRepository.GetById(this.Creater).Name;
                }
            }
        }

        public List<SelectListItem> FactoryOptions
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                var values = equipmentRepository.Query(true, 0, 1);
                foreach (var v in values)
                {
                    result.Add(new SelectListItem()
                    {
                        Value = v.ID.ToString(),
                        Text = v.Title
                    });
                }
                return result;
            }
        }

        public List<SelectListItem> FloorOptions
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem()
                {
                    Value = "0",
                    Text = "---選擇樓層---"
                });

                return result;
            }
        }
        public List<SelectListItem> AreaOptions
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem()
                {
                    Value = "0",
                    Text = "---選擇區域---"
                });

                return result;
            }
        }

        public List<SelectListItem> FaultStateOptions
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                var values = EnumHelper.GetValues<FaultState>();
                foreach (var v in values)
                {
                    result.Add(new SelectListItem()
                    {
                        Value = ((int)v).ToString(),
                        Text = EnumHelper.GetDescription(v)
                    });
                }
                return result;
            }
        }

        public List<SelectListItem> DepartmentOptions
        {
            get
            {
                var adminInfo = FairlyRepair.Utility.Helpers.MemberInfoHelper.GetMemberInfo();
                List<SelectListItem> result = new List<SelectListItem>();

                if (adminInfo.Type == (int)MeetAdminType.Manager)
                {
                    var value = personRepository.GetPersonQuery("", adminInfo.Account).FirstOrDefault();
                    result.Add(new SelectListItem()
                    {
                        Value = value.DeptID,
                        Text = value.DepartmentStr
                    });
                }
                else
                {
                    var values = personRepository.GetDepartments();
                    foreach (var v in values)
                    {
                        result.Add(new SelectListItem()
                        {
                            Value = v.DeptId,
                            Text = v.DeptName
                        });
                    }
                }
                return result;
            }
        }

        public List<SelectListItem> DeviceOptions
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem()
                {
                    Value = "0",
                    Text = "---選擇設備---"
                });

                return result;
            }
        }

        public List<SelectListItem> DeviceItemOptions
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem()
                {
                    Value = "0",
                    Text = ""
                });
                if (this.DeviceID != 0)
                {
                    var values = deviceRepository.Query((int)DeviceClass.Third, this.DeviceID);
                    foreach (var v in values)
                    {
                        result.Add(new SelectListItem()
                        {
                            Value = v.ID.ToString(),
                            Text = v.Title
                        });
                    }
                }
                return result;
            }
        }

        public List<SelectListItem> MaintStaffOptions
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                var values = adminRepository.Query(true, (int)AdminType.Mainter);
                foreach (var v in values)
                {
                    result.Add(new SelectListItem()
                    {
                        Value = v.ID.ToString(),
                        Text = v.Name
                    });
                }
                return result;
            }
        }
    }

}