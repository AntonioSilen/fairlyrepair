﻿using FairlyRepair.Utility.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FairlyRepair.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            var acc = MemberInfoHelper.GetMemberInfo();
            if (acc == null)
            {
                return RedirectToAction("LogIn", "Auth");
            }
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}