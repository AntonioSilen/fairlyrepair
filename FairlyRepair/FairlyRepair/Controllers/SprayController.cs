﻿using AutoMapper;
using FairlyRepair.ActionFilters;
using FairlyRepair.Areas.Admin.ViewModels.Repair;
using FairlyRepair.Models;
using FairlyRepair.Models.Others;
using FairlyRepair.Repositories;
using FairlyRepair.Repositories.T8Repositories;
using FairlyRepair.Utility;
using FairlyRepair.Utility.Helpers;
using FairlyRepair.ViewModels.Repair;
using Ical.Net.DataTypes;
using Ical.Net.Serialization;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace FairlyRepair.Controllers
{
    [ErrorHandleActionFilter]
    public class SprayController : BaseController
    {
        private AdminRepository adminRepository = new AdminRepository(new FairlyRepairDBEntities());
        private PersonRepository personRepository = new PersonRepository(new T8ERPEntities());
        private GroupPersonRepository groupPersonRepository = new GroupPersonRepository(new T8ERPEntities());
        private RepairRepository repairRepository = new RepairRepository(new FairlyRepairDBEntities());
        private RepairImageRepository repairImageRepository = new RepairImageRepository(new FairlyRepairDBEntities());
        private RepairRecordRepository repairRecordRepository = new RepairRecordRepository(new FairlyRepairDBEntities());
        private RepairRecordImageRepository repairRecordImageRepository = new RepairRecordImageRepository(new FairlyRepairDBEntities());
        private DeviceRepository deviceRepository = new DeviceRepository(new FairlyRepairDBEntities());

        // GET: Repair
        public ActionResult Index(RepairIndexView model)
        {
            var acc = MemberInfoHelper.GetMemberInfo();
            if (acc == null)
            {
                return RedirectToAction("LogIn", "Auth");
            }
            //model.PageSize = 1;
            var query = repairRepository.Query(null, null,
                null, null,
                3, 0, 0, 0, 0, "").Where(q => q.Creater == acc.ID);
            var pageResult = query.ToPageResult<Repair>(model);
            model.PageResult = Mapper.Map<PageResult<Areas.Admin.ViewModels.Repair.RepairView>>(pageResult);
            model.DeviceType = 3;

            return View(model);
        }
        public ActionResult Detail(int id = 0)
        {
            var acc = MemberInfoHelper.GetMemberInfo();
            if (acc == null)
            {
                return RedirectToAction("LogIn", "Auth");
            }

            var model = new ViewModels.Repair.RepairView();

            if (id != 0)
            {
                var query = repairRepository.FindBy(id);
                model = Mapper.Map<ViewModels.Repair.RepairView>(query);
                model.PicList = repairImageRepository.Query(id).ToList();
            }
            else
            {
                model.DeviceType = 3;
                //預設無須審核
                model.PresAppStatus = 0;
                model.ViceAppStatus = 0;
                model.IsPresApproval = false;
                model.IsViceApproval = false;
            }

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public ActionResult Detail(ViewModels.Repair.RepairView model)
        {
            var acc = MemberInfoHelper.GetMemberInfo();
            if (acc == null)
            {
                return RedirectToAction("LogIn", "Auth");
            }
            if (ModelState.IsValid)
            {
                Repair data = Mapper.Map<Repair>(model);
                int adminId = acc.ID;
                data.Updater = adminId;
                model.DispatcherID = GetDispatcherID(model);

                if (model.ID == 0)
                {
                    data.Creater = adminId;
                    data.Updater = adminId;
                    model.ID = repairRepository.Insert(data);
                    SendNotify(model.ID);
                    ShowMessage(true, "報修單已送出");
                }
                else
                {
                    repairRepository.Update(data);
                    ShowMessage(true, "報修單已更新");
                }

                #region 檔案處理
                string PhotoFolder = Path.Combine(Server.MapPath("~/FileUploads"), "RepairPhoto");
                if (model.PicsFiles != null)
                {
                    foreach (HttpPostedFileBase pic in model.PicsFiles)
                    {
                        bool hasFile = ImageHelper.CheckFileExists(pic);
                        if (hasFile)
                        {
                            RepairImage repairData = new RepairImage();
                            //ImageHelper.DeleteFile(PhotoFolder, model.FileOne);
                            repairData.RepairID = model.ID;
                            repairData.Pics = ImageHelper.SaveFile(pic, PhotoFolder);
                            repairImageRepository.Insert(repairData);
                        }
                    }
                }
                #endregion

                return RedirectToAction("Detail", new { id = model.ID });
            }

            return View(model);
        }

        public ActionResult Record(RepairRecordIndexView model)
        {
            var acc = MemberInfoHelper.GetMemberInfo();
            if (acc == null)
            {
                return RedirectToAction("LogIn", "Auth");
            }
            var repairQuery = repairRepository.FindBy(model.RepairID);
            if (repairQuery == null)
            {
                return RedirectToAction("Index", "Home");
            }
            var query = repairRecordRepository.Query(model.IsSolved, model.RepairID);
            model.RecordList = Mapper.Map<List<Areas.Admin.ViewModels.RepairRecord.RepairRecordView>>(query).OrderByDescending(q => q.CreateDate).ToList();
            foreach (var item in model.RecordList)
            {
                item.PicList = repairRecordImageRepository.Query(item.ID).ToList();
            }

            model.RepairInfo = Mapper.Map<Areas.Admin.ViewModels.Repair.RepairView>(repairQuery);
            model.RepairInfo.PicList = repairImageRepository.Query(model.RepairID).ToList();
            return View(model);
        }

        public int GetDispatcherID(ViewModels.Repair.RepairView model)
        {
            var memberInfo = MemberInfoHelper.GetMemberInfo();
            if (!string.IsNullOrEmpty(model.MaintID))
            {
                return 0;
            }
            if (memberInfo.Type == (int)AdminType.Dispatcher)
            {
                return memberInfo.ID;
            }
            return 0;
        }

        public string SendNotify(int repairId)
        {
            try
            {
                var repairInfo = Mapper.Map<ViewModels.Repair.RepairView>(repairRepository.FindBy(repairId));
                if (repairInfo == null)
                {
                    return "發送失敗，查無報修申請";
                }
                //var dispactcherJID = adminRepository.Query(true, (int)MeetAdminType.Dispatcher).Select(q => q.Account);
                var dispactcherJID = adminRepository.Query(true, 0, "2416").Select(q => q.Account).ToList();
                var reciverList = groupPersonRepository.Query().Where(q => dispactcherJID.Contains(q.PersonId)).ToList();
                foreach (var item in reciverList)
                {
                    //var thisSignIn = notifyList.Where(q => q.JobID == item.Account).FirstOrDefault();

                    #region 生成Qrcode圖片
                    //string host = $"{Request.Url.Scheme}://{Request.Url.Authority}";
                    //string fileName = $"/{repairInfo.ID}_{item.JobID}.png";
                    //string qrcodeUrl = thisSignIn.SignCode;
                    //string savePath = Server.MapPath("/FileUploads/Qrcode") + $"/{repairInfo.ID}/{fileName}";
                    //QRCodeHelper.GeneratorQrCodeImage(qrcodeUrl, savePath);
                    #endregion

                    #region 寄送Email                    
                    //string to = "seelen12@fairlybike.com";
                    string to = item.EMail;
                    string subject = "";
                    string mailBody = "";
                    string DeviceItemName = !string.IsNullOrEmpty(repairInfo.DeviceItemName) ? " (" + repairInfo.DeviceItemName + ") " : "";
                    mailBody = $"<p>申請人 : {repairInfo.CreaterName}</p>" +
                                        $"<p>廠區 : {repairInfo.FactoryTitle}</p>" +
                                        $"<p>課別 : {repairInfo.DepartmentTitle}</p>" +
                                        $"<p>設備 : {repairInfo.DeviceName} {DeviceItemName}</p>";

                    mailBody += $"<p>預定開始時間 : {repairInfo.CreateDate.ToString("yyyy/MM/dd HH:mm")}</p>" +
                                                $"<p>{repairInfo.Description}</p>";

                    subject = $"報修申請 - 系統信件";

                    var calendar = new Ical.Net.Calendar();

                    DateTime today = DateTime.Now;
                    calendar.Events.Add(new Ical.Net.CalendarComponents.CalendarEvent
                    {
                        Class = "PUBLIC",
                        Summary = subject,
                        Created = new CalDateTime(today),
                        Description = $"【{repairInfo.CreaterName}】{repairInfo.DeviceName} 報修申請",
                        Start = new CalDateTime(repairInfo.CreateDate),
                        End = new CalDateTime(repairInfo.CreateDate),
                        Sequence = 0,
                        Uid = Guid.NewGuid().ToString(),
                        Location = repairInfo.FactoryTitle,
                    });

                    var serializer = new CalendarSerializer(new SerializationContext());
                    var serializedCalendar = serializer.SerializeToString(calendar);
                    var bytesCalendar = Encoding.UTF8.GetBytes(serializedCalendar);
                    MemoryStream ms = new MemoryStream(bytesCalendar);

                    MailHelper.POP3Mail(to, subject, mailBody, ms);
                    #endregion

                    //新增報修紀錄

                }

                return "通知發送成功";
            }
            catch (Exception ex)
            {
                return "發送失敗，錯誤訊息 : " + ex.Message;
            }
            //return RedirectToAction("Detail", "Repair", new { id = repairId });
        }
    }
}